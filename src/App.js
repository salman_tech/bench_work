import React, {Component} from "react";
import ReactDOM from 'react-dom';

import { BrowserRouter as Router, Route } from 'react-router-dom';

import Home  from "./components/home/index"
// import CaseOrders from "./components/home/CaseOrder/caseOrders";
import CauseList from "./components/home/CauseList/causeList";
import AdminCauseList from "./components/home/CauseList/adminCauseList";
import CaseStatus from './components/home/CaseStatus/caseStatus';
import caseStatusSubmit from "./components/home/CaseStatus/caseStatusSubmit";
import caseOrderLogin from "./components/home/caseOrder/CaseOrderLogin";
// import caseOrderSubmit from "./components/home/CaseOrder/caseOrderSubmit";
import causeListSubmit from "./components/home/CauseList/CauseListSubmit";
// import caseOrderSubmitPage from "./components/home/CaseOrder/caseOrdersSubmitPage";
import AllCaseOrder from './components/home/caseOrder/AllCaseOrder'
import Login from "./components/auth/login";
import MasterLogin from "./components/auth/MasterLogin";
import PeshkarLogin from "./components/auth/PeshkaarLogin";
import MasterLoginSetting from "./components/common/MasterLoginSetting";
import caseStatusSubmitPage from "./components/home/CaseStatus/caseStatusSubmitPage";
import newCaseRegistration from "./components/home/CaseRegistration/NewCaseRegistration";
import viewAllCase from "./components/home/CaseRegistration/ViewAllCase";
import VeiwPageCaseRegistration from "./components/home/CaseRegistration/ViewCasePage";
import ViewCaseRegistration from "./components/home/CaseRegistration/ViewCaseRegistration";
import disputedLandInfo from "./components/home/disputedLandInfo/disputedLandInfo";
import disputedLandLogin from "./components/home/disputedLandInfo/disputedLogin";
import chart from "./components/common/chart";
import ChangePassword from './components/common/changePassword'
import DM_Login from "./components/auth/DM_Login";
import ViewDailyOrder from './components/home/peetaseen_adhikari/viewDailyCase';
import CourtOrder from './components/home/peetaseen_adhikari/courtOrderr';
import addUsers from "./components/home/masterLogin/addUsers";
import addCourt from "./components/home/masterLogin/addCourt";
import addAct from "./components/home/masterLogin/addAct";
import Footer from "./components/common/footer";


class App extends Component {
    render() {
        return (
            <div>
                   <Router>
                       <Route exact path="/" component={Home} />
                       <Route exact path="/case-status" component={CaseStatus} />
                       {/* <Route exact path="/case-orders" component={CaseOrders} /> */}
                       <Route exact path="/cause-list" component={CauseList} />
                       <Route exact path="/case-status-submit" component={caseStatusSubmit} />
                       {/* <Route exact path="/case-order-submit" component={caseOrderSubmit} /> */}
                       <Route exact path="/cause-list-submit" component={causeListSubmit} />
                       <Route exact path="/admin-cause-list" component={AdminCauseList}/>
                       {/* <Route exact path="/case-order-submit-page" component={caseOrderSubmitPage} /> */}
                       <Route exact path="/all-case-order" component={AllCaseOrder}/>
                       <Route exact path="/login" component={Login} />
                       <Route exact path="/master-login" component={MasterLogin} />
                       <Route exact path="/peshkaar-login" component={PeshkarLogin} />
                       <Route exact path="/master-setting" component={MasterLoginSetting} />
                       <Route exact path="/Case-Status-submit-page" component={caseStatusSubmitPage} />
                       <Route exact path="/new-case-registration" component={newCaseRegistration} />
                       <Route exact path="/view-all-case" component={viewAllCase} />
                       <Route exact path="/view-case" component={VeiwPageCaseRegistration} />
                       <Route exact path="/view-case-status" component={ViewCaseRegistration} />
                       <Route exact path="/disputed-land-information" component={disputedLandInfo} />
                       <Route exact path="/case-order-info" component={caseOrderLogin} />
                       <Route exact path="/disputed-land" component={disputedLandLogin} />
                       <Route exact path="/DM-login" component={DM_Login} />
                       <Route exact path="/daily-case" component={ViewDailyOrder} />
                       <Route exact path="/court-order" component={CourtOrder} />
                       <Route exact path="/chart" component={chart} />
                       <Route exact path="/change-password" component={ChangePassword} />
                       <Route exact path="/Add-Users" component={addUsers} />
                       <Route exact path="/Add-Court" component={addCourt} />
                       <Route exact path="/Add-Act" component={addAct} />
                   </Router>
                <Footer/>

            </div>
        );
    }
}

export default App;

