import React, {Component} from 'react';
import logo from "../../assets/logo.jpg";
import {Link} from "react-router-dom";
import Dashboard from "../../assets/Dashboard.png";
import CaseRegistration from "../../assets/CaseRegistration.png";
import DailyCauseList from "../../assets/DailyCauseList.png";
import CourtOrder from "../../assets/CourtOrder.png";
import DisputedLandInfo from "../../assets/DisputedLandInfo.png";
import Setting from "../../assets/setting.png";
import Footer from "../../common/footer"
import MasterLoginSetting from "../../common/MasterLoginSetting";
import DownArrow from "../../assets/DownArrow.png";
import Service from '../../service'
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";
import {ToastsContainer, ToastsStore} from "react-toasts";
import Moment from 'react-moment';
import SimpleReactValidator from 'simple-react-validator';
import { sync } from 'glob';




class ViewCaseRegistration extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data:[],
            witness:[''],
            arguments:[''],
            witnessPet:[''],
            witnessDef:[''],
            argumentsPet:[''],
            argumentsDef:[''],
            dist:[],
            nextDate:false,
            files:null,
            images:[{}],
            type:'default'
        };
        // this.validator = new SimpleReactValidator({ autoForceUpdate: this });0.2
    }

    componentDidMount(){
        this.getDistict()
    }
    getDistict=async()=>{
        let res = await Service.location.getDistict()
        if(typeof(res)=="object"){
            if(res && res.status && res.status == true){
                this.setState({dist:res.data})
            }
            else{
                alert("Status Faild")
            }
        }
        else{
            alert("Api failed")
        }
    }
    saveImages = async(e) => {
        let image;
        var t = e.target.files;
        image = [];
        for (let i = 0; i < t.length; i++) {
            image.push(t[i])
        }
       await this.setState({[e.target.name]:image})
       console.log("imgggg",this.state.images)
    }
        
    handleChangefile = (e) => {
        let image
            var t = e.target.files;
            image = [];
            for (let i = 0; i < t.length; i++) {
                image.push(t[i])
            }
        this.setState({files: image})
        console.log("imgggggggg",this.state.files)
    }

    handleChange=async(e)=>{
        if (e.keyCode === 13) {
            e.preventDefault();
            this.caseDirection()
        }
        else{
            this.setState({[e.target.name]:e.target.value})
        }
    }
    caseDirection= async()=>{
        if(this.state.caseId && this.state.caseId !=''){

            let res = await Service.location.getCaseHearing(this.state.caseId)
                if(typeof(res)=="object"){
    
                    if(res && res.status == true && res.status !==undefined){
                        this.setState({data:res.data})
                    }
                    else{
                        alert(res.message)
                    }
                }
        }
        else{
            alert("Search Field is Empty")
        }
    }
    
        
    handleChangeMap=(e,i,value)=>{
        debugger
        if(value=="1"){
        const data = [...this.state.witness]
        data[i]=e.target.value
        this.setState({witness:data})
        }
        else if(value =="2"){
            const data = [...this.state.evidence]
        data[i][e.target.name]=e.target.value
        this.setState({evidence:data})
        }
        else if(value == "3"){
            const data = [...this.state.witnessDef]
            data[i]=e.target.value
            this.setState({arguments:data})
        }
        else if(value == "4"){
            const data = [...this.state.argumentsPet]
            data[i]=e.target.value
            this.setState({argumentsPet:data})
        }
        else if(value == "5"){
            const data = [...this.state.argumentsDef]
            data[i]=e.target.value
            this.setState({argumentsDef:data})
        }
        else if(value === "evdFile"){
            console.log("ediice filessssss",e.target.files)
            this.setState({[e.target.name]:e.target.files})
        }
        else if(e.target.value === "ORDER"){
            this.props.history.push("/all-case-order")
        }
        else{
            this.setState({[e.target.name]:e.target.value})
        }

    }
    addMore=(value)=>{
        if(value=="1"){
            this.setState((prevState, nextState) => ({
                witness: [...prevState.witness,'']
            })) 
        }
        else if(value=="2"){
            this.setState((prevState, nextState) => ({
                evidence: [...prevState.evidence,{}]
            }))
        }
        else if(value=="3"){
            this.setState((prevState, nextState) => ({
                witnessPet: [...prevState.witnessPet,'']
            }))
        }
        else if(value=="4"){
            this.setState((prevState, nextState) => ({
                witnessDef: [...prevState.witnessDef,'']
            }))
        }
        else if(value=="5"){
            this.setState((prevState, nextState) => ({
                argumentsPet: [...prevState.argumentsPet,'']
            }))
        }
        else if(value=="6"){
            this.setState((prevState, nextState) => ({
                argumentsDef: [...prevState.argumentsDef,'']
            }))
        }

    }
    remove=(i,value)=>{
        if(value=="1"){
            const witnessPet = [...this.state.witnessPet]
            witnessPet.splice(i,1)
            this.setState({witnessPet})
        }
        else if(value =="2"){
            const evidence = [...this.state.evidence]
            evidence.splice(i,1)
            this.setState({evidence})
        }
        else if(value =="3"){
            const witnessDefClone = [...this.state.witnessDef]
            witnessDefClone.splice(i,1)
            this.setState({witnessDef:witnessDefClone})
        }
        else if(value =="4"){
            const argumentssPetRev = [...this.state.argumentsPet]
            argumentssPetRev.splice(i,1)
            this.setState({argumentsPet:argumentssPetRev})
        }
        else if(value =="5"){
            const argumentssDefRev = [...this.state.argumentsDef]
            argumentssDefRev.splice(i,1)
            this.setState({argumentsDef:argumentssDefRev})
        }
    }
    nextDate=()=>{
      if(this.state.nextDate)this.setState({nextDate:false})
      else this.setState({nextDate:true})
    }
    onSubmit=async()=>{
        const body={
                "hearing" :{ 
                         "witness":this.state.witness,
                         "evidences": this.state.files, 
                         "time":this.state.time,
                         "arguments": this.state.arguments,
                         "nextHearingDate":this.state.nextDate ? this.state.nextHearingDate:"null"
                     }
                }
        let res = await Service.location.addCaseHearing(this.state.caseId,body)
        if(typeof(res)=="object"){
            if(res && res.status && res.status == true){
                alert(res.message)
            }
            else{
                alert(res.message)
            }
        }
        else{
            alert("API Faild")
        }
               

    }

        render() {
            return (

                    <div>

            <div style={{}}>
                    <div className="logo--heading">
                        <img src={logo} style={{width:80,height:80}}/>
                        <h2>REVENUE COURT OF DEHRADUN</h2>
                    </div>

                    <div className={"setting_menu_main " + (this.state.settingMenuVisible ? "open" : "")}>
                        <span className="close_menu" onClick={()=>{
                            this.setState({
                                settingMenuVisible:false
                            })
                        }}>
                            <i className="mdi mdi-close"></i>
                        </span>
                        <MasterLoginSetting/>
                    </div>

                    <div className="PeshkarLoginTabs" >
                        <div className="container">
                            <ul>
                                <li><Link to='/peshkaar-login' ><button className="active"><img src={Dashboard} style={{width:60,height:60,marginTop:"-15px"}}/><br/> Dashboard </button> </Link> </li>

                                <li> <UncontrolledDropdown>
                                    <DropdownToggle  style={{background:"white",color: "black",border:"none"}}>
                                        <img src={CaseRegistration} style={{width:60,height:60}}/><br/> Case Registration <br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem> <Link to='/new-case-registration'>New Case Registration</Link></DropdownItem>
                                        <DropdownItem divider />
                                        <DropdownItem><Link to to='/view-all-case'>View Case</Link></DropdownItem>
                                        <DropdownItem divider />
                                        {/* <DropdownItem>Delete Case</DropdownItem> */}
                                        <DropdownItem divider />
                                        <DropdownItem ><Link to='/view-case-status'>Update Case status</Link>
                                        <DropdownItem divider />
                                        </ DropdownItem>
                                        <DropdownItem ><Link >Upload Order Sheet</Link>
                                        <DropdownItem divider />
                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown> </li>

                                <li>
                                    <UncontrolledDropdown>
                                        <DropdownToggle style={{background:"white",color: "black",border:"none"}} >
                                            <img src={DailyCauseList} style={{width:60,height:60}}/><br/>  Daily Cause List <br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem><Link to="/admin-cause-list"> View Daily Cause List </Link></DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Change Prioritiy</DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Next Hearing Date</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                </li>



                                <li>
                                    <UncontrolledDropdown>
                                        <DropdownToggle style={{background:"white",color: "black",border:"none"}} >
                                            <Link to="/all-case-order"><img src={CourtOrder} style={{width:60,height:60}}/><br/>  Court Orders</Link> <br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                         {/* <DropdownMenu>
                                            <DropdownItem>  Type Order </DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Upload Order</DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Upload Order: Disposed </DropdownItem>
                                           <DropdownItem divider />
                                           <DropdownItem>Application Letter: Disposed</DropdownItem> 
                                     </DropdownMenu> */}
                                    </UncontrolledDropdown>
                                </li>


                                <li>
                                    <UncontrolledDropdown>
                                        <DropdownToggle  style={{background:"white",color: "black",border:"none"}}>
                                            <img src={DisputedLandInfo} style={{width:60,height:60}}/><br/>Disputed Land Info<br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem> <Link to={{pathname:'/disputed-land-information',state:{type:"GOLDEN_FOREST_LAND"}}}>Golden Forest Land</Link></DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem> <Link to={{pathname:'/disputed-land-information',state:{type:"GRAM_SABHA_LAND"}}}>Gram Sabha Land</Link></DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem> <Link to={{pathname:'/disputed-land-information',state:{type:"LAND_PROHIBITED_FROM_SALE"}}}>Land Prohibited From Sale</Link></DropdownItem>

                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                </li>
                                <li><button className="active" onClick={()=>{this.setState({settingMenuVisible:true})}}><img src={Setting} style={{width:60,height:60,marginTop:"-15px"}}/><br/> Setting</button></li>
                            </ul>
                        </div>
                    </div>
                </div>

                        <div className="CaseRegistrationBox" style={{paddingTop:"0px",height:"auto"}}>
                        <div className="container" style={{background:"#F1F6FE"}}>
                            <div className="col-md-12">
                                    <span style={{textAlign:"center",display:"block",fontWeight:"bold"}}>Update Case Status</span>
                                    </div>
                                </div>
                            <div className="container" style={{background: "#FFF", border: "2px solid lightGray"}}>
                                
                                        <div className="row" >
                                        <div className="container">
                                            <div className="row">
                                            {/* <div className="col-md-12" style={{paddingTop: "20px", paddingBottom: "20px"}}> */}
                                            <div className="col-md-3" style={{paddingTop: "20px", paddingBottom: "20px"}}>
                                            <label style={{marginLeft:"20px"}}>Case No.</label>
                                            <input style={{padding:"0px",border:"1px solid #666666",marginLeft:"20px",width:"50%"}} value={this.state.data.caseId} onKeyUp={(e)=>{this.handleChange(e)}} onChange={(e)=>{this.handleChange(e)}} placeholder="Case Id" name="caseId"/>
                                            <span style={{background:"#007bff",width: "35px",height:"25px",top:"21px",right:"41px",position:"absolute"}}>
                                                <img onClick={()=>this.caseDirection()} style={{height:"22px",width:"22px",marginLeft:"8px"}}  className="img-thumbnails" src={require('../../assets/search.png')} /></span>
                                            </div>
                                            <div className="col-md-3" style={{paddingTop: "20px", paddingBottom: "20px"}}>
                                            <label>Registration Date</label>
                                            <span style={{padding:"7px",border:"1px solid #666666",marginLeft:"20px"}}>{this.state.data &&this.state.data[0] && this.state.data[0].registrationDate !==undefined ? <Moment format="DD/MM/YYYY">{this.state.data[0].registrationDate}</Moment>:"null"}</span>
                                            </div>


                                            <div className="col-md-3" style={{paddingTop: "20px", paddingBottom: "20px"}}>
                                            
                                            <label>Court</label>
                                            <span style={{padding:"7px",border:"1px solid #666666",marginLeft:"20px"}}>{this.state.data &&this.state.data[0] && this.state.data[0].courtId &&  this.state.data[0].courtId.length > 5 ?this.state.data[0].courtId.slice(0,5):"null" }</span>
                                            </div>
                                            <div className="col-md-3" style={{paddingTop: "20px", paddingBottom: "20px"}}>
                                            <label>District</label>
                                            <select value={this.state.data && this.state.data[0] &&  this.state.data[0].complainant && this.state.data[0].complainant[0].districtId !==undefined ? this.state.data[0].complainant[0].districtId:"null" } >
                                            {this.state.dist && this.state.dist.map(itm=>{
                                                return(
                                                    <option value={itm.id} >{itm.name}</option>
                                                )
                                            })}
                                            </select>
                                            {/* <span style={{padding:"7px",border:"1px solid #666666",marginLeft:"20px"}}>Dehradun</span> */}
                                            </div>
                                            </div>
                                        </div>
                                        <div className="container" style={{marginTop:"-10px"}}>
                                            <div className="row">
                                            {/* <div className="col-md-12" style={{paddingTop: "20px", paddingBottom: "20px"}}> */}
                                            <div className="col-md-2" style={{paddingTop: "20px", paddingBottom: "20px"}}>
                                            <label>Act Name</label>
                                            <span style={{padding:"7px",border:"1px solid #666666",marginLeft:"20px"}}>{this.state.data && this.state.data[0] && this.state.data[0].dharaId.length > 5 ? this.state.data[0].dharaId.slice(0,5):"null" }</span>
                                            </div>
                                            <div className="col-md-2" style={{paddingTop: "20px", paddingBottom: "20px"}}>
                                            <label style={{marginLeft:"20px"}}>Section</label>
                                            <span style={{padding:"7px",border:"1px solid #666666",marginLeft:"20px"}}>{this.state.data && this.state.data[0] && this.state.data[0].caseDirection && this.state.data[0].caseDirection.length > 5 ? this.state.data[0].caseDirection.slice(0,5):"null" }</span>
                                            </div>
                                            <div className="col-md-2" style={{paddingTop: "20px", paddingBottom: "20px"}}>
                                            <label>Petitioner</label>
                                            <span style={{padding:"7px",fontSize:"12px",border:"1px solid #666666",marginLeft:"20px"}}>{this.state.data && this.state.data[0] &&  this.state.data[0].complainant && this.state.data[0].complainant[0] && this.state.data[0].complainant[0].name && this.state.data[0].complainant[0].name.length > 2  ? this.state.data[0].complainant[0].name.slice(0,12):"null" }</span>
                                            </div>
                                            <div className="col-md-2" style={{paddingTop: "20px", paddingBottom: "20px"}}>
                                            <label>Adv. Name</label>
                                            <span style={{padding:"7px",fontSize:"12px",border:"1px solid #666666",marginLeft:"20px"}}>{this.state.data && this.state.data[0] &&  this.state.data[0].complainant && this.state.data[0].complainant[0] && this.state.data[0].complainant[0].advocateName && this.state.data[0].complainant[0].advocateName.length > 2  ? this.state.data[0].complainant[0].advocateName.slice(0,10):"null" }</span>
                                            </div>
                                            <div className="col-md-2" style={{paddingTop: "20px", paddingBottom: "20px"}}>
                                            <label>Respondent</label>
                                            <span style={{padding:"7px",fontSize:"12px",border:"1px solid #666666",marginLeft:"20px"}}>{this.state.data && this.state.data[0] && this.state.data[0].defendant && this.state.data[0].defendant[0] && this.state.data[0].defendant[0].name && this.state.data[0].defendant[0].name.length > 2 ? this.state.data[0].defendant[0].name.slice(0,12):"null" }</span>
                                            </div>
                                            <div className="col-md-2" style={{paddingTop: "20px", paddingBottom: "20px"}}>
                                            <label>Adv. Name</label>
                                            <span style={{padding:"7px",fontSize:"12px",border:"1px solid #666666",marginLeft:"20px"}}>{this.state.data && this.state.data[0] && this.state.data[0].defendant && this.state.data[0].defendant[0] && this.state.data[0].defendant[0].advocateName && this.state.data[0].defendant[0].advocateName.length > 2 ? this.state.data[0].defendant[0].advocateName.slice(0,12):"null" }</span>
                                            </div>
                                            
                                            </div>
                                        </div>
                                        </div>
                            </div>
                            
                            <div className="container" style={{background: "#FFF", border: "2px solid lightGray",marginTop:"20px"}}>
                                
                                        <div className="row" style={{padding:"20px"}}>
                                        <div className="container">
                                            <div className="row">
                                            <div className="col-md-2">
                                    <label style={{display:"block",textAlign:"center"}}>Date</label>
                                        </div>
                                        <div className="col-md-3" style={{position:"relative"}}>
                                        <span>
                                         {this.state.data &&this.state.data[0] && this.state.data[0].registrationDate !==undefined ? <Moment format="DD/MM/YYYY">{this.state.data[0].registrationDate}</Moment>:"null"}
                                        </span>
                                        </div>
                                        <div className="col-md-2">
                                                <label style={{display:"block",textAlign:"center"}}>Fixed Proceeding</label>
                                        </div>
                                                <div className="col-md-3" style={{position:"relative"}}>
                                                <select name="fixedProceeding" onChange={(e)=>this.handleChangeMap(e,"","4")} value='ADMISSIBLE' disabled style={{width:"100%",height:"100%"}}>
                                                <option value="DEFAULT">Select the Proceeding</option>
                                                            <option value="ADMISSIBLE">Admissible</option>
                                                            <option value="NOTICE">Notice</option>
                                                            <option value="WAITING_FOR_RESPONDENT">Waiting For Respondent</option>
                                                            <option value="EXPERTY/WS">Experty/WS</option>
                                                            <option value="RE-JOINER">Re-joiner</option>
                                                            <option value="EVIDENCE">Evidence</option>
                                                            <option value="ARGUMENT">Argument</option>
                                                            <option value="ORDER">Order</option>
                                                            {/* <option value="JUDGEMENT">Judgement</option>
                                                            <option value="REGISTERED">Registered</option> */}
                                                {/* <option value="option">option2</option>
                                                <option value="option">option3</option> */}
                                                </select>
                                                </div>
                                            </div>
                                            
                                        
                                        </div>
                                        
                                        </div>
                            </div>
                            <div className="container" style={{background: "#FFF", border: "2px solid lightGray",marginTop:"20px"}}>
                                
                                <div className="row" style={{padding:"20px"}}>
                                <div className="container">
                                    <div className="row">
                                    <div className="col-md-2">
                            <label style={{display:"block",textAlign:"center"}}>Date</label>
                                </div>
                                <div className="col-md-3" style={{position:"relative"}}>
                                <input 
                                type="date"
                                onChange={(e)=>this.handleChangeMap(e,"","4")}
                                name="noticeDate"
                                />
                                </div>
                                <div className="col-md-2">
                                        <label style={{display:"block",textAlign:"center"}}>Fixed Proceeding</label>
                                </div>
                                        <div className="col-md-3" style={{position:"relative"}}>
                                        <select name="fixedProceeding" onChange={(e)=>this.handleChangeMap(e,"","")}  style={{width:"100%",height:"100%"}}>
                                        <option value="DEFAULT">Select the Proceeding</option>
                                                    <option value="ADMISSIBLE">Admissible</option>
                                                    <option value="NOTICE">Notice</option>
                                                    <option value="WAITING_FOR_RESPONDENT">Waiting For Respondent</option>
                                                    <option value="EXPERTY/WS">Experty/WS</option>
                                                    <option value="RE-JOINER">Re-joiner</option>
                                                    <option value="WITNESS">Witness</option>
                                                    <option value="EVIDENCE">Evidence</option>
                                                    <option value="ARGUMENT">Argument</option>
                                                    <option value="ORDER">Order</option>
                                                    {/* <option value="JUDGEMENT">Judgement</option>
                                                    <option value="REGISTERED">Registered</option> */}
                                        {/* <option value="option">option2</option>
                                        <option value="option">option3</option> */}
                                        </select>
                                        </div>
                                    </div>
                                    
                                
                                </div>
                                
                                </div>
                    </div>
                            
                            
                            {this.state.nextDate && 
                            <div className="container" style={{background: "#FFF", border: "2px solid lightGray",marginTop:"20px"}}>
                            
                                                 <div className="row" style={{marginTop:"10px",marginBottom:"10px"}}>
                                                <div className="col-md-12">
                                                    <div className="row">
                                                        <div className="col-md-2">

                                                <label style={{display:"block",textAlign:"center"}}>Next Date</label>
                                                        </div>
                                            
                                                <div className="col-md-3" style={{position:"relative"}}>
                                                <input name="nextHearingDate" type="date" placeholder="Enter the Name" onChange={(e)=>this.handleChangeMap(e,"","4")}/>
                                                </div>
                                                
                                                <div className="col-md-2" style={{marginTop:"10px"}}>
                                                <label style={{display:"block",textAlign:"center"}}>Fixed Proceeding</label></div>
                                                <div className="col-md-3" style={{position:"relative",marginTop:"10px"}}>
                                                <select name="fixedProceeding" onChange={(e)=>this.handleChangeMap(e,"","4")} style={{width:"100%",height:"100%"}}>
                                                <option value="option">Select Fixed Proceeding</option>
                                                <option value="ADMISSIBLE">Admissible</option>
                                                    <option value="NOTICE">Notice</option>
                                                    <option value="WAITING_FOR_RESPONDENT">Waiting For Respondent</option>
                                                    <option value="EXPERTY/WS">Experty/WS</option>
                                                    <option value="RE-JOINER">Re-joiner</option>
                                                    <option value="EVIDENCE">Evidence</option>
                                                    <option value="ARGUMENT">Argument</option>
                                                    <option value="ORDER">Order</option>
                                                {/* <option value="option">option2</option>
                                                <option value="option">option3</option> */}
                                                </select>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                            </div>
                            }
                            {this.state.fixedProceeding =="WITNESS" &&
                            <div className="container" style={{background: "#FFF", border: "2px solid lightGray",marginTop:"20px"}}>
                                
                                        <div className="row" >
                                        <div className="container">
                                            <div className="row">
                                                <div className="col-md-12" style={{textAlign:"center"}}><h4>Witness
                                                    </h4></div>
                                            <div className="col-md-5">
                                                {this.state.witnessPet.map((itm,idx)=>{
                                                    return(

                                                <div className="row" style={{marginTop:"10px",marginBottom:"10px"}}>
                                                <div className="col-md-4">
                                                <label style={{display:"block",textAlign:"center"}}>Petioner Name
                                                {/* {this.state.witness.length > 1 ? idx+1:""} */}
                                                </label>
                                                </div>
                                                <div className="col-md-8" style={{position:"relative"}}><input placeholder="Enter the Name" onChange={(e)=>this.handleChangeMap(e,idx,"1")}/>
                                                <span onClick={()=>this.addMore("3")} style={{width:"34px",height:"29px",position:"absolute",background:"#c9d7ef",left:"155px",textAlign:"center",fontSize:"20px",color:"#fff",cursor:"pointer"}}>+</span>
                                                {this.state.witnessPet.length > 1 &&  <span onClick={()=>this.remove(idx,"1")} style={{width:"34px",height:"29px",position:"absolute",background:"#c9d7ef",right:"90px",textAlign:"center",fontSize:"20px",color:"black",cursor:"pointer"}}>-</span>
                                               }
                                                </div>
                                                </div>
                                                    )
                                                })}
                                                
                                                {/* <div className="row" style={{marginTop:"10px",marginBottom:"10px"}}>
                                                <div className="col-md-6"><label style={{display:"block",textAlign:"center"}}>Evidence (s)</label></div>
                                                <div className="col-md-6" style={{position:"relative"}}>
                                                <input type="file"  id="customFile" name="images" onChange={(e) => this.saveImages(e)} multiple />
                                                <span  style={{width:"34px",height:"29px",position:"absolute",background:"#c9d7ef",left:"155px",textAlign:"center",fontSize:"20px",color:"#fff",cursor:"pointer"}}>+</span>
                                               
                                                </div>
                                                </div> */}
                                                   
                                                {/* <div className="row" style={{marginTop:"10px",marginBottom:"10px"}}>
                                                <div className="col-md-6"><label style={{display:"block",textAlign:"center"}}>Time</label>
                                                </div>
                                                <div className="col-md-6" style={{position:"relative"}}><input type="date" placeholder="Enter the Date" name="time" onChange={(e)=>this.handleChangeMap(e,"","4")}/>
                                                <span style={{width:"34px",height:"29px",position:"absolute",background:"#c9d7ef",left:"155px",textAlign:"center",fontSize:"20px",color:"#fff"}}>
                                                    <img className="img-thumbnail" style={{background:"rgb(201, 215, 239)",marginTop:"-5px",border:"none",height:"27px",cursor:"pointer"}} src={require("../../assets/time.png")}/>
                                                </span>
                                                </div>
                                                </div> */}
                                                {/* {this.state.nextDate &&
                                                 <div className="row" style={{marginTop:"10px",marginBottom:"10px"}}>
                                                <div className="col-md-6">
                                                    <div className="row">
                                                        <div className="col-md-6">

                                                <label style={{display:"block",textAlign:"center"}}>Next Date</label>
                                                </div>
                                                <div className="col-md-6">
                                                </div>
                                                </div>
                                                            
                                                </div>
                                                <div className="col-md-6" style={{position:"relative"}}>
                                                <input name="nextHearingDate" type="date" placeholder="Enter the Name" onChange={(e)=>this.handleChangeMap(e,"","4")}/>
                                                </div>
                                                <div className="col-md-6" style={{marginTop:"10px"}}>
                                                <label style={{display:"block",textAlign:"center"}}>Fixed Proceeding</label></div>
                                                <div className="col-md-6" style={{position:"relative",marginTop:"10px"}}>
                                                <select name="fixedProceeding" onChange={(e)=>this.handleChangeMap(e,"","4")} style={{width:"80%"}}>
                                                <option value="option">Select Fixed Proceeding</option>
                                                <option value="option">Admissibility</option>
                                                <option value="option">option2</option>
                                                <option value="option">option3</option>
                                                </select>
                                                </div>
                                                </div>} */}
                                            </div>
                                            {/* <div className="col-md-7">
                                                {this.state.arguments.map((itm,idx)=>{
                                                    return(

                                            <div className="row" style={{marginTop:"10px",marginBottom:"10px"}}>
                                                <div className="col-md-2">
                                                    <label style={{display:"block",textAlign:"left"}}>Argments</label>
                                                    </div>
                                                <div className="col-md-9" style={{position:"relative"}}><input style={{width:"100%",height:"76px"}} onChange={(e)=>this.handleChangeMap(e,idx,"3")} placeholder="Enter the Arguments Here"/>
                                                <span onClick={()=>this.addMore("3")} style={{width:"50px",height:"75px",position:"absolute",right:"16px",background:"#c9d7ef",textAlign:"center",fontSize:"46px",color:"#fff",cursor:"pointer"}}>+</span>
                                                {this.state.arguments.length > 1 &&  <span onClick={()=>this.remove(idx,"3")} style={{width:"50px",height:"75px",position:"absolute",right:"-38px",background:"#c9d7ef",textAlign:"center",fontSize:"46px",color:"#fff",cursor:"pointer"}}>-</span>
                                               }
                                                </div>
                                            </div>
                                                    )
                                                })}
                                            </div> */}
                                            {/* <div className="col-md-7">
                                                {this.state.arguments.map((itm,idx)=>{
                                                    return(

                                            <div className="row" style={{marginTop:"10px",marginBottom:"10px"}}>
                                                <div className="col-md-2">
                                                    <label style={{display:"block",textAlign:"left"}}>Defendant Name</label>
                                                    </div>
                                                <div className="col-md-9" style={{position:"relative"}}><input style={{width:"100%",height:"76px"}} onChange={(e)=>this.handleChangeMap(e,idx,"3")} placeholder="Enter the Arguments Here"/>
                                                <span onClick={()=>this.addMore("3")} style={{width:"50px",height:"75px",position:"absolute",right:"16px",background:"#c9d7ef",textAlign:"center",fontSize:"46px",color:"#fff",cursor:"pointer"}}>+</span>
                                                {this.state.arguments.length > 1 &&  <span onClick={()=>this.remove(idx,"3")} style={{width:"50px",height:"75px",position:"absolute",right:"-38px",background:"#c9d7ef",textAlign:"center",fontSize:"46px",color:"#fff",cursor:"pointer"}}>-</span>
                                               }
                                                </div>
                                            </div>
                                                    )
                                                })}
                                            </div> */}
                                            <div className="col-md-7">
                                                {this.state.witnessDef.map((itm,idx)=>{
                                                    return(

                                            <div className="row" style={{marginTop:"10px",marginBottom:"10px"}}>
                                                <div className="col-md-4">
                                                    <label style={{display:"block",textAlign:"left"}}>Defendant Name</label>
                                                    </div>
                                                <div className="col-md-8" style={{position:"relative"}}><input  onChange={(e)=>this.handleChangeMap(e,idx,"3")} placeholder="Enter the Name"/>
                                                <span onClick={()=>this.addMore("4")} style={{width:"34px",height:"29px",position:"absolute",background:"#c9d7ef",left:"155px",textAlign:"center",fontSize:"20px",color:"#fff",cursor:"pointer"}}>+</span>
                                                {this.state.witnessDef.length > 1 &&  <span onClick={()=>this.remove(idx,"3")} style={{width:"34px",height:"29px",position:"absolute",background:"#c9d7ef",right:"215px",textAlign:"center",fontSize:"20px",color:"black",cursor:"pointer"}}>-</span>
                                               }
                                                </div>
                                            </div>
                                                    )
                                                })}
                                            </div>
                                            </div>
                                        </div>
                                        
                                        </div>
                            </div>
                            }
                            {this.state.fixedProceeding === "ARGUMENT" && 
                        <div className="container" style={{background: "#FFF", border: "2px solid lightGray",marginTop:"20px"}}>
                                
                        <div className="row" >
                        <div className="container">
                            <div className="row">
                                <div className="col-md-12" style={{textAlign:"center"}}><h4>ARGUMENT
                                    </h4></div>
                            <div className="col-md-5">
                                {this.state.argumentsPet.map((itm,idx)=>{
                                    return(

                                <div className="row" style={{marginTop:"10px",marginBottom:"10px"}}>
                                <div className="col-md-4">
                                <label style={{display:"block",textAlign:"center"}}>Petioner Name
                                {/* {this.state.witness.length > 1 ? idx+1:""} */}
                                </label>
                                </div>
                                <div className="col-md-8" style={{position:"relative"}}><input placeholder="Enter the Name" onChange={(e)=>this.handleChangeMap(e,idx,"4")}/>
                                <span onClick={()=>this.addMore("5")} style={{width:"34px",height:"29px",position:"absolute",background:"#c9d7ef",left:"155px",textAlign:"center",fontSize:"20px",color:"#fff",cursor:"pointer"}}>+</span>
                                {this.state.argumentsPet.length > 1 &&  <span onClick={()=>this.remove(idx,"4")} style={{width:"34px",height:"29px",position:"absolute",background:"#c9d7ef",right:"90px",textAlign:"center",fontSize:"20px",color:"black",cursor:"pointer"}}>-</span>
                               }
                                </div>
                                </div>
                                    )
                                })}
                                
                                {/* <div className="row" style={{marginTop:"10px",marginBottom:"10px"}}>
                                <div className="col-md-6"><label style={{display:"block",textAlign:"center"}}>Evidence (s)</label></div>
                                <div className="col-md-6" style={{position:"relative"}}>
                                <input type="file"  id="customFile" name="images" onChange={(e) => this.saveImages(e)} multiple />
                                <span  style={{width:"34px",height:"29px",position:"absolute",background:"#c9d7ef",left:"155px",textAlign:"center",fontSize:"20px",color:"#fff",cursor:"pointer"}}>+</span>
                               
                                </div>
                                </div> */}
                                   
                                {/* <div className="row" style={{marginTop:"10px",marginBottom:"10px"}}>
                                <div className="col-md-6"><label style={{display:"block",textAlign:"center"}}>Time</label>
                                </div>
                                <div className="col-md-6" style={{position:"relative"}}><input type="date" placeholder="Enter the Date" name="time" onChange={(e)=>this.handleChangeMap(e,"","4")}/>
                                <span style={{width:"34px",height:"29px",position:"absolute",background:"#c9d7ef",left:"155px",textAlign:"center",fontSize:"20px",color:"#fff"}}>
                                    <img className="img-thumbnail" style={{background:"rgb(201, 215, 239)",marginTop:"-5px",border:"none",height:"27px",cursor:"pointer"}} src={require("../../assets/time.png")}/>
                                </span>
                                </div>
                                </div> */}
                                {/* {this.state.nextDate &&
                                 <div className="row" style={{marginTop:"10px",marginBottom:"10px"}}>
                                <div className="col-md-6">
                                    <div className="row">
                                        <div className="col-md-6">

                                <label style={{display:"block",textAlign:"center"}}>Next Date</label>
                                </div>
                                <div className="col-md-6">
                                </div>
                                </div>
                                            
                                </div>
                                <div className="col-md-6" style={{position:"relative"}}>
                                <input name="nextHearingDate" type="date" placeholder="Enter the Name" onChange={(e)=>this.handleChangeMap(e,"","4")}/>
                                </div>
                                <div className="col-md-6" style={{marginTop:"10px"}}>
                                <label style={{display:"block",textAlign:"center"}}>Fixed Proceeding</label></div>
                                <div className="col-md-6" style={{position:"relative",marginTop:"10px"}}>
                                <select name="fixedProceeding" onChange={(e)=>this.handleChangeMap(e,"","4")} style={{width:"80%"}}>
                                <option value="option">Select Fixed Proceeding</option>
                                <option value="option">Admissibility</option>
                                <option value="option">option2</option>
                                <option value="option">option3</option>
                                </select>
                                </div>
                                </div>} */}
                            </div>
                            {/* <div className="col-md-7">
                                {this.state.arguments.map((itm,idx)=>{
                                    return(

                            <div className="row" style={{marginTop:"10px",marginBottom:"10px"}}>
                                <div className="col-md-2">
                                    <label style={{display:"block",textAlign:"left"}}>Argments</label>
                                    </div>
                                <div className="col-md-9" style={{position:"relative"}}><input style={{width:"100%",height:"76px"}} onChange={(e)=>this.handleChangeMap(e,idx,"3")} placeholder="Enter the Arguments Here"/>
                                <span onClick={()=>this.addMore("3")} style={{width:"50px",height:"75px",position:"absolute",right:"16px",background:"#c9d7ef",textAlign:"center",fontSize:"46px",color:"#fff",cursor:"pointer"}}>+</span>
                                {this.state.arguments.length > 1 &&  <span onClick={()=>this.remove(idx,"3")} style={{width:"50px",height:"75px",position:"absolute",right:"-38px",background:"#c9d7ef",textAlign:"center",fontSize:"46px",color:"#fff",cursor:"pointer"}}>-</span>
                               }
                                </div>
                            </div>
                                    )
                                })}
                            </div> */}
                            {/* <div className="col-md-7">
                                {this.state.arguments.map((itm,idx)=>{
                                    return(

                            <div className="row" style={{marginTop:"10px",marginBottom:"10px"}}>
                                <div className="col-md-2">
                                    <label style={{display:"block",textAlign:"left"}}>Defendant Name</label>
                                    </div>
                                <div className="col-md-9" style={{position:"relative"}}><input style={{width:"100%",height:"76px"}} onChange={(e)=>this.handleChangeMap(e,idx,"3")} placeholder="Enter the Arguments Here"/>
                                <span onClick={()=>this.addMore("3")} style={{width:"50px",height:"75px",position:"absolute",right:"16px",background:"#c9d7ef",textAlign:"center",fontSize:"46px",color:"#fff",cursor:"pointer"}}>+</span>
                                {this.state.arguments.length > 1 &&  <span onClick={()=>this.remove(idx,"3")} style={{width:"50px",height:"75px",position:"absolute",right:"-38px",background:"#c9d7ef",textAlign:"center",fontSize:"46px",color:"#fff",cursor:"pointer"}}>-</span>
                               }
                                </div>
                            </div>
                                    )
                                })}
                            </div> */}
                            <div className="col-md-7">
                                {this.state.argumentsDef.map((itm,idx)=>{
                                    return(

                            <div className="row" style={{marginTop:"10px",marginBottom:"10px"}}>
                                <div className="col-md-4">
                                    <label style={{display:"block",textAlign:"left"}}>Defendant Name</label>
                                    </div>
                                <div className="col-md-8" style={{position:"relative"}}><input  onChange={(e)=>this.handleChangeMap(e,idx,"5")} placeholder="Enter the Name"/>
                                <span onClick={()=>this.addMore("6")} style={{width:"34px",height:"29px",position:"absolute",background:"#c9d7ef",left:"155px",textAlign:"center",fontSize:"20px",color:"#fff",cursor:"pointer"}}>+</span>
                                {this.state.argumentsDef.length > 1 &&  <span onClick={()=>this.remove(idx,"5")} style={{width:"34px",height:"29px",position:"absolute",background:"#c9d7ef",right:"215px",textAlign:"center",fontSize:"20px",color:"black",cursor:"pointer"}}>-</span>
                               }
                                </div>
                            </div>
                                    )
                                })}
                            </div>
                            </div>
                        </div>
                        
                        </div>
            </div>}
            {this.state.fixedProceeding == "RE-JOINER" && 
                            <div className="container" style={{background: "#FFF", border: "2px solid lightGray",marginTop:"20px"}}>
                            
                                                 <div className="row" style={{marginTop:"10px",marginBottom:"10px"}}>
                                                <div className="col-md-12">
                                                    <div className="row">
                                                        <div className="col-md-2">

                                                <label style={{display:"block",textAlign:"center"}}>Text</label>
                                                        </div>
                                            
                                                <div className="col-md-3" style={{position:"relative"}}>
                                                <input name="nextHearingDate" type="text" placeholder="Enter the Name" onChange={(e)=>this.handleChangeMap(e,"","")}/>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                            </div>
                            }
                            {this.state.fixedProceeding == "EVIDENCE" && 
                            <div className="container" style={{background: "#FFF", border: "2px solid lightGray",marginTop:"20px"}}>
                            
                                                 <div className="row" style={{marginTop:"10px",marginBottom:"10px"}}>
                                                <div className="col-md-12">
                                                    <div className="row">
                                                        <div className="col-md-2">

                                                <label style={{display:"block",textAlign:"center"}}>Evidence</label>
                                                        </div>
                                            
                                                <div className="col-md-3" style={{position:"relative"}}>
                                                <input name="nextHearingDate" type="file" name="evdFile" onChange={(e)=>this.handleChangeMap(e,"","evdFile")}/>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                            </div>
                            }
                            
                            <div className="row" style={{marginTop:"10px",marginBottom:"55px",paddingBottom:"15px"}}>
                                <div className="container">
                                    <div className="row">
                                        <div className="container">
                                        <div className="row">
                                    <div className="col-md-4" style={{textAlign:"center"}}>
                                    <button className="btn btn-primary btn-lg" style={{width:"inherit",background:"#fff",border:"2px solid blue",color:"blue"}}onClick={()=>this.nextDate()}>Set Next Hearing Date</button>
                                    </div>
                                    <div className="col-md-4" style={{textAlign:"center"}}>
                                    <button className="btn btn-primary btn-lg" style={{width:"inherit"}} onClick={()=>this.onSubmit()}>submit</button>
                                    </div>
                                    <div className="col-md-4" style={{textAlign:"center"}}>
                                    <button className="btn btn-primary btn-lg" style={{width:"inherit",background:"green",color:"#fff"}}>End Case</button>
                                    </div>
                                    </div>
                                    </div>
                                </div>
                                 </div>
                                </div>
                            {/* <div className="container" style={{ paddingLeft:"340px",paddingTop:"30px",paddingBottom:"30px"}}>
                                <div className="col-md-12">
                                    <div className="col-md-4">
                                <div className="caseStatusButton" style={{paddingBottom:"50px",paddingLeft:"50px"}}>
                                    <div className="container">
                                        <button type="button" className="btn btn-secondary btn-lg" style={{background:"transparent",borderColor:"blue",borderWidth:"2px",color:"blue", width:"230px"}}>Reset</button>
                                  <button onClick={this.submitAddCase} type="submit" className="btn btn-primary btn-lg" style={{width:"230px"}}>Submit</button>
                                    </div>
                                </div>
                                </div>
                                </div>
                            </div> */}
                        </div>
                        <ToastsContainer store={ToastsStore}/>
                        <Footer/>
                    </div>
            );
        }
}
export default ViewCaseRegistration;

