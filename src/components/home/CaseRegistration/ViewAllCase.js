import React, {Component} from 'react';
import logo from "../../assets/logo.jpg";
import {Link} from "react-router-dom";
import Dashboard from "../../assets/Dashboard.png";
import CaseRegistration from "../../assets/CaseRegistration.png";
import DailyCauseList from "../../assets/DailyCauseList.png";
import CourtOrder from "../../assets/CourtOrder.png";
import DisputedLandInfo from "../../assets/DisputedLandInfo.png";
import Setting from "../../assets/setting.png";
import DownArrow from '../../assets/DownArrow.png'
import MasterLoginSetting from "../../common/MasterLoginSetting";
import Footer from "../../common/footer";
import Service from '../../service'
import Moment from 'react-moment';
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';







class ViewAllCase extends Component {


    constructor(props) {

        super(props);
        this.state = {
            tabContent: 0,
            menuOpenClass: false,
            data:[]
        };
    }

    componentDidMount(){
        this.getAllCase()
    }
    getAllCase=async()=>{
        let res = await Service.location.getAllCase()
        if(typeof(res) =="object"){
            if(res.status ==true && res.status !== undefined){
                this.setState({data:res.data})
            }
            else{
                this.setState({data:[]})
            }
        }
        else{
            this.setState({data:[]})
        }
    }
 deleletCase=async(id)=>{
     var id = id;
    var person = prompt("Please Enter Password");
//     if (window.confirm("Are you sure!")) {
//     let res =await Service.location.deleletCase(id)
//     if(typeof(res) == "object"){
//         if(res.status == true || res.status !== undefined){
//             alert("Deleted successfully")
//             this.getAllCase()
//         }
//         else{
//             alert(res.message)
//         }
//     }
//     else{
//         alert("Backend error")
//     }
//   } 
    }

    moveToPage=(id)=>{
        const value = id
        localStorage.setItem('disputed_id',value)
        this.props.history.push('/Case-Status-submit-page')
    }
    editPage=(id)=>{
        const value = id
        localStorage.setItem('page_id',value)
        this.props.history.push('/view-case')
    }
    
    handleChnage=(e)=>{
    this.setState({[e.target.name]:e.target.value})
    }
    onSearch=async()=>{
        if(this.state.searchItem !==undefined && this.state.searchItem !==''){

            const res = await Service.location.caseSearch(this.state.searchItem)
            if(typeof(res)=='object'){
                if(res.status && res.status == true){
                    this.setState({data:res.data})
                }
                else{
                    alert("Search Failed")
                }
            }
            else{
                alert("API Failed")
            }
        }
        else{
            alert("Enter the Search Item")
        }
    }
    render() {
        return (
            <div>
                <div style={{}}>
                    <div className="logo--heading">
                        <img src={logo} style={{width:80,height:80}}/>
                        <h2>REVENUE COURT OF DEHRADUN</h2>
                    </div>

                    <div className={"setting_menu_main " + (this.state.settingMenuVisible ? "open" : "")}>
                        <span className="close_menu" onClick={()=>{
                            this.setState({
                                settingMenuVisible:false
                            })
                        }}>
                            <i className="mdi mdi-close"></i>
                        </span>
                        <MasterLoginSetting/>
                    </div>

                    <div className="PeshkarLoginTabs" >
                        <div className="container">
                            <ul>
                                <li><Link to='/peshkaar-login' ><button className="active"><img src={Dashboard} style={{width:60,height:60,marginTop:"-15px"}}/><br/> Dashboard </button> </Link> </li>

                                <li> <UncontrolledDropdown>
                                    <DropdownToggle  style={{background:"white",color: "black",border:"none"}}>
                                        <img src={CaseRegistration} style={{width:60,height:60}}/><br/> Case Registration <br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem> <Link to='/new-case-registration'>New Case Registration</Link></DropdownItem>
                                        <DropdownItem divider />
                                        <DropdownItem><Link to to='/view-all-case'>View Case</Link></DropdownItem>
                                        <DropdownItem divider />
                                        {/* <DropdownItem>Delete Case</DropdownItem> */}
                                        <DropdownItem divider />
                                        <DropdownItem ><Link to='/view-case-status'>Update Case status</Link>
                                        <DropdownItem divider />
                                        </ DropdownItem>
                                        <DropdownItem ><Link >Upload Order Sheet</Link>
                                        <DropdownItem divider />
                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown> </li>

                                <li>
                                    <UncontrolledDropdown>
                                        <DropdownToggle style={{background:"white",color: "black",border:"none"}} >
                                            <img src={DailyCauseList} style={{width:60,height:60}}/><br/>  Daily Cause List <br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem><Link to="/admin-cause-list"> View Daily Cause List </Link></DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Change Prioritiy</DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Next Hearing Date</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                </li>



                                <li>
                                    <UncontrolledDropdown>
                                        <DropdownToggle style={{background:"white",color: "black",border:"none"}} >
                                            <Link to="/all-case-order"><img src={CourtOrder} style={{width:60,height:60}}/><br/>  Court Orders</Link> <br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                         {/* <DropdownMenu>
                                            <DropdownItem>  Type Order </DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Upload Order</DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Upload Order: Disposed </DropdownItem>
                                           <DropdownItem divider />
                                           <DropdownItem>Application Letter: Disposed</DropdownItem> 
                                     </DropdownMenu> */}
                                    </UncontrolledDropdown>
                                </li>


                                <li>
                                    <UncontrolledDropdown>
                                        <DropdownToggle  style={{background:"white",color: "black",border:"none"}}>
                                            <img src={DisputedLandInfo} style={{width:60,height:60}}/><br/>Disputed Land Info<br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem> <Link to={{pathname:'/disputed-land-information',state:{type:"GOLDEN_FOREST_LAND"}}}>Golden Forest Land</Link></DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem> <Link to={{pathname:'/disputed-land-information',state:{type:"GRAM_SABHA_LAND"}}}>Gram Sabha Land</Link></DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem> <Link to={{pathname:'/disputed-land-information',state:{type:"LAND_PROHIBITED_FROM_SALE"}}}>Land Prohibited From Sale</Link></DropdownItem>

                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                </li>
                                <li><button className="active" onClick={()=>{this.setState({settingMenuVisible:true})}}><img src={Setting} style={{width:60,height:60,marginTop:"-15px"}}/><br/> Setting</button></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div className="PeshkarLoginBlueBox">
                        
                    <div className="container">
                    <div style={{paddingTop:"30px",paddingBottom:"100px"}}>
                    <div className="row">
                        <div className="col-md-8"><h4>View All Case</h4>
                        </div>
                        <div className="col-md-4" style={{textAlign:"right"}}>
                        <div className="row">
                        <div className="col-md-9" style={{textAlign:"right",paddingRight:"0px"}} >
                        <input placeholder="Enter the Case No" name="searchItem" onChange={(e)=>this.handleChnage(e)} style={{height:"29px",width:"100%"}}/>
                        </div>
                        <div className="col-md-3" style={{textAlign:"center",padding:"0px"}}>
                        <button className="btn btn-primary" type="button" onClick={()=>this.onSearch()} style={{height:"30px",lineHeight:"0px"}}>Search</button>
                        </div>
                        </div>
                        </div>
                        
                        </div>
                        <div className='PeshkarTable'>
                        <table>
                            <tr>
                                <th>Sr No</th>
                                <th>Case No</th>
                                {/* <th>Revison</th>
                                <th>Review</th>
                                <th>Transfer</th> */}
                                <th>Registration By</th>
                                <th>Complainant Vs Defendent</th>
                                <th>Court</th>
                                <th>Action</th>
                            </tr>
                            {this.state.data && this.state.data.length > 0 ? this.state.data.map((itm,key)=>{
                                return(
                            <tr>
                                <td>{key+1}</td>
                                <td>{itm.caseId}</td>
                                {/* <td>25</td>
                                <td>12</td>
                                <td>0</td> */}
                                <td>Backend No key</td>
                                <td>{itm.complainant.length > 0 ? itm.complainant.length+"complainant":itm.complainant.name}{" "} Vs {" "}{itm.defendant.length > 0 ? itm.defendant.length+"Defendanant":itm.defendant.name }</td>
                                <td>{itm.typeOfComplainant}</td>
                                <td>
                                    <img className="img-thumbnail" onClick={()=>this.moveToPage(itm._id)} style={{width:"30px",border:"none"}} src={require('../../assets/eye.png')}/>
                                    <img className="img-thumbnail" onClick={()=>this.editPage(itm._id)} style={{width:"30px",border:"none"}} src={require('../../assets/update.png')}/>
                                    {/* <img className="img-thumbnail" onClick={()=>this.deleletCase(itm.id)} style={{width:"30px",border:"none"}} src={require('../../assets/delete.png')}/> */}
                                </td>
                            </tr>

                                )
                            }):
                            <tr>
                                <td colspan="6"><div style={{color:"red"}}>No result found.</div></td>
                                </tr>}
                        </table>
                        </div>
                    </div>
                    </div>

                </div>
                <Footer/>

            </div>
        );
    }
}
export default ViewAllCase;