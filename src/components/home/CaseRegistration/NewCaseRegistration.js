import React , { Component } from 'react';
import logo from "../../assets/logo.jpg";
import { Link } from "react-router-dom";
import Dashboard from "../../assets/Dashboard.png";
import CaseRegistration from "../../assets/CaseRegistration.png";
import DailyCauseList from "../../assets/DailyCauseList.png";
import CourtOrder from "../../assets/CourtOrder.png";
import DisputedLandInfo from "../../assets/DisputedLandInfo.png";
import Setting from "../../assets/setting.png";
import Footer from "../../common/footer"
import MasterLoginSetting from "../../common/MasterLoginSetting";
import DownArrow from "../../assets/DownArrow.png";
import axios from "axios";
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";
import {ToastsContainer, ToastsStore} from "react-toasts";
import SimpleReactValidator from 'simple-react-validator';
import Service from '../../service'




class newCaseRegistration extends Component {

    constructor(props) {

        super(props);
        // this.submitAddCase = this.submitAddCase.bind(this);

        this.state = {
            vaadType : '',
            adhiniumId : '5e23f392e6b2647e6c908319',
            dharaId: '5e23f3fbe6b2647e6c90831a',
            registrationDate: Date(),
            vaadKaNaam: 'MM',
            vadiPitaPatiKaNaam: '',


            Act:'',
            RegistrationDate:'',
            Clause:'',
            complainant:[{"name":'',"mobile":'',"fatherHusbandName":'',"address":'',"districtId":'',"advocateName":'',"advocateMobile":'',"complainantEmail":'',"advocateEmail":''}],
            defendant:[{"name":'',"mobile":'',"fatherHusbandName":'',"address":'',"districtId":'',"advocateName":'',"advocateMobile":'',"defendantEmail":'',"advocateEmail":''}],
            FixedDate:'',
            landType:["ZA","NZA","Survey"],
            FixedProceeding:'',
            LandDistrict:'',
            type:[],
            getAct:[],
            filterMap:[],
            filterMapSection:[],
            courtName:[],
            getDist:[],
            getTehsil:[],
            getPargana:[],
            getVillage:[]
        };
        this.handleChange = this.handleChange.bind(this);
        this.validator = new SimpleReactValidator({ autoForceUpdate: this });

    }
    componentDidMount(){
        this.getCase()
        this.getAct()
        this.getCourt()
        this.getDist()
        this.getTehsil()
        this.getPargana()
        this.getVillage()
    }
    getDist=async()=>{
        const res = await Service.location.getDistict()
        if(typeof(res)== 'object'){
            if(res.status && res.status == true){
                this.setState({getDist:res.data})
            }
            else{
                alert("Api Status false")
                this.setState({getDist:[]})
            }
        }
        else{
            alert("API Failed")
            this.setState({getDist:[]})
        }
    }
    getTehsil=async()=>{
        const res = await Service.location.getTehsil()
        if(typeof(res)== 'object'){
            if(res.status && res.status == true){
                this.setState({getTehsil:res.data})
            }
            else{
                alert("Api Status false")
                this.setState({getTehsil:[]})
            }
        }
        else{
            alert("API Failed")
            this.setState({getTehsil:[]})
        }
    }
    getPargana=async()=>{
        const res = await Service.location.getPargana()
        if(typeof(res)== 'object'){
            if(res.status && res.status == true){
                this.setState({getPargana:res.data})
            }
            else{
                alert("Api Status false")
                this.setState({getPargana:[]})
            }
        }
        else{
            alert("API Failed")
            this.setState({getPargana:[]})
        }
    }
    getVillage=async()=>{
        const res = await Service.location.getVillage()
        if(typeof(res)== 'object'){
            if(res.status && res.status == true){
                this.setState({getVillage:res.data})
            }
            else{
                alert("Api Status false")
                this.setState({getVillage:[]})
            }
        }
        else{
            alert("API Failed")
            this.setState({getVillage:[]})
        }
    }

    getCourt=async()=>{
        let res  = await Service.location.getCourt()
        if(typeof(res) == "object"){
            if(res && res.status && res.status == true){
                this.setState({courtName:res.data})
            }
            else{
                alert(res.message)
            }
        }
        else{
            this.setState({courtName:[]})

            // alert("API")
        }

    }
    getAct=async()=>{
        const res = await Service.location.getActAdhinium()
        console.log("actttttttttttttttttttt",res)
        if(typeof(res) == "object"){
            if(res && res.status == true){
                this.setState({getAct:res.data})
            }
            else{
                alert(res.message)
            }
        }
        else{
            this.setState({getAct:[]})
        }
    }

    getCase=async()=>{
        const res = await Service.location.getCase()
        console.log("caseeeeeee",res)
        if(typeof(res) == "object"){
            if(res.status === true){
                this.setState({type:res.data})
            }
            else{
                this.setState({type:[]})
            }
        }
        else{
            this.setState({type:[]})
        }
    }

    handleChange(e) {
        this.setState({
            [e.target.name] : e.target.value
        });
        console.log(this.state,"Updated")
    }
    handleChangeAct= async(e)=>{
        const name = e.target.value
        debugger
        // alert(name)
        const filtervalue = this.state.getAct.filter(itm=>{
            if(itm._id == name){
                return true
            }
        })
            
        debugger
        await this.setState({[e.target.name]:e.target.value,
        filterMap:filtervalue[0] && filtervalue[0].forms ? filtervalue[0].forms:[] ,filterMapSection:filtervalue[0] && filtervalue[0].section && filtervalue[0].section ?filtervalue[0].section:[]})
       
        console.log("filter value",this.state)
     }
        
        
    handleChangeMap=(e,i,value)=>{
        if(value=="1"){
        const data = [...this.state.complainant]
        data[i][e.target.name]=e.target.value
        this.setState({complainant:data})
        }
        else{
            const data = [...this.state.defendant]
        data[i][e.target.name]=e.target.value
        this.setState({defendant:data})
        }
    }
    handleChangeLandType=(e)=>{
        this.setState({[e.target.name]:e.target.value})
    }
    addMore=(value)=>{
        if(value=="1"){
            this.setState((prevState, nextState) => ({
                complainant: [...prevState.complainant,{}]
            })) 
        }
        else{
            this.setState((prevState, nextState) => ({
                defendant: [...prevState.defendant,{}]
            }))
        }

    }
    remove=(i,value)=>{
        if(value=="1"){
            const complainant = [...this.state.complainant]
            complainant.splice(i,1)
            this.setState({complainant})
        }
        else{
            const defendant = [...this.state.defendant]
            defendant.splice(i,1)
            this.setState({defendant})
        }
    }
    submitAddCase =async()=>{
        if (this.validator.allValid()) {
        console.log("you submitted this form");

        const headers = {
            headers: {
                "Authorization": "JWT "+localStorage.getItem('token')
            }
        };

        console.log(this.state);
        const body={
            "vaadType": this.state.vaadType,
            "caseId": this.state.caseId,
            "adhiniumId": this.state.Act,
            "targetDate":this.state.targetDate,
            "dharaId": this.state.section,
            "dharaId": "5e459e700501794bfe31e951",
            "registrationDate": this.state.RegistrationDate,
            "fixedDate": this.state.fixedDate,
            "fixedProceeding": this.state.fixedProceeding,
            "courtFeeValutionProperty":this.state.valuationProperty,
            "courtFeeValuationLandRevenue":this.state.valuationLandRevenue,
            "courtFee":this.state.courtFee,
            "typeOfComplainant":this.state.companyType,
            "complainant":this.state.complainant,
            "defendant":this.state.defendant,
            "arms":{ 
                "armsType":this.state.arms,
                "licenseNumber":this.state.armsLicenseNumber,
                "weaponNumber":this.state.weaponNumber,
                "thanaId":"5e459e700501794bfe31e951",
                "districtId":this.state.armsdistrict,
                // "thanaId":this.state.thana,
                // "districtId":this.state.district,
               
             },
            "land":{
                "typeLandData":{ 
                    "gata":this.state.landTypeSelect == ("NZA"||"Survey") ? this.state.gataNumber:"null",
                    "rakba":this.state.landTypeSelect == ("NZA"||"Survey") ? this.state.rakba:'null',
                    "noOflandSections":this.state.landTypeSelect == "ZA" ? this.state.landSection:"0"
                 },
                "districtId":this.state.landDistrict,
                //  "tehsilId":this.state.landTehsil,
                "tehsilId":this.state.landTehsil,
                "parganaId":this.state.landPargana,
                "villageId":this.state.landVillage,
                "typeLand":this.state.landTypeSelect,
                "typeDisputedLand":this.state.landType == "ZA"? null : this.state.disputedLand
            },
            "stampDuty":{
                "paid":this.state.paid,
                "reference":this.state.reference,
                "reduction":this.state.reduction
            },
            "description": {
                "thanaId": "5e459e700501794bfe31e951",
                // "thanaId": this.state.thanaDescription,
                // "districtId": "5e072a064c545a0b7ca2c1dc",
                "districtId":this.state.districtDescription,
                // "tehsilId": "5e4b716eb3575558ce6307cc",
                "tehsilId":this.state.tehsilDescription,
                "dateChalaniReport":this.state.dateOfChalaniReport,
                "oppositionFigureChalaniReport":this.state.chalaniReport
            },
        }
        debugger
        // const r = await Service.location.addNewCase(body)
        // if(typeof(r)== "object"){
        //     if(r && r.status && r.status == true){
        //         alert(r.message)
        //     }
        //     else{
        //         alert(r.message)
        //     }
        // }
        // else{
        //  alert("Api Failed")   
        // } 
        axios.post("http://139.59.47.2/api/v1/case",body,headers).then(resp => {
            console.log(['data',resp.data]);
            // resp.data me apna object aayega { status: true|false, msg: "asf", data }
            if(resp && resp.data.data && resp.data.data.status) {
                console.log("all true");

            } 
            else {
                console.log("this is validation err msg");
                console.log(resp.data.message);
            }
            if(resp.data.status) {
                ToastsStore.success('Case has been registered');
                this.props.history.push('/view-all-case')
                console.log(this.state.loginType)

            }
            else{
                ToastsStore.error('User Failed');

            }

        },err => {
            console.log(['err',err]);
        });
    }
    else {
        this.validator.showMessages();
        this.forceUpdate();
        alert("you miss the required filed")
     }
}

        




        render() {
            return (

                    <div>
<div style={{}}>
                    <div className="logo--heading">
                        <img src={logo} style={{width:80,height:80}}/>
                        <h2>REVENUE COURT OF DEHRADUN</h2>
                    </div>

                    <div className={"setting_menu_main " + (this.state.settingMenuVisible ? "open" : "")}>
                        <span className="close_menu" onClick={()=>{
                            this.setState({
                                settingMenuVisible:false
                            })
                        }}>
                            <i className="mdi mdi-close"></i>
                        </span>
                        <MasterLoginSetting/>
                    </div>

                    <div className="PeshkarLoginTabs" >
                        <div className="container">
                            <ul>
                                <li><Link to='/peshkaar-login' ><button className="active"><img src={Dashboard} style={{width:60,height:60,marginTop:"-15px"}}/><br/> Dashboard </button> </Link> </li>

                                <li> <UncontrolledDropdown>
                                    <DropdownToggle  style={{background:"white",color: "black",border:"none"}}>
                                        <img src={CaseRegistration} style={{width:60,height:60}}/><br/> Case Registration <br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem> <Link to='/new-case-registration'>New Case Registration</Link></DropdownItem>
                                        <DropdownItem divider />
                                        <DropdownItem><Link to to='/view-all-case'>View Case</Link></DropdownItem>
                                        <DropdownItem divider />
                                        {/* <DropdownItem>Delete Case</DropdownItem> */}
                                        <DropdownItem ><Link to='/view-case-status'>Update Case status</Link>
                                        <DropdownItem divider />
                                        </ DropdownItem>
                                        <DropdownItem ><Link >Upload Order Sheet</Link>
                                        <DropdownItem divider />
                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown> </li>

                                <li>
                                    <UncontrolledDropdown>
                                        <DropdownToggle style={{background:"white",color: "black",border:"none"}} >
                                            <img src={DailyCauseList} style={{width:60,height:60}}/><br/>  Daily Cause List <br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem><Link to="/admin-cause-list"> View Daily Cause List </Link></DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Change Prioritiy</DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Next Hearing Date</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                </li>



                                <li>
                                    <UncontrolledDropdown>
                                        <DropdownToggle style={{background:"white",color: "black",border:"none"}} >
                                            <Link to="/all-case-order"><img src={CourtOrder} style={{width:60,height:60}}/><br/>  Court Orders</Link> <br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                         {/* <DropdownMenu>
                                            <DropdownItem>  Type Order </DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Upload Order</DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Upload Order: Disposed </DropdownItem>
                                           <DropdownItem divider />
                                           <DropdownItem>Application Letter: Disposed</DropdownItem> 
                                     </DropdownMenu> */}
                                    </UncontrolledDropdown>
                                </li>


                                <li>
                                    <UncontrolledDropdown>
                                        <DropdownToggle  style={{background:"white",color: "black",border:"none"}}>
                                            <img src={DisputedLandInfo} style={{width:60,height:60}}/><br/>Disputed Land Info<br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem> <Link to={{pathname:'/disputed-land-information',state:{type:"GOLDEN_FOREST_LAND"}}}>Golden Forest Land</Link></DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem> <Link to={{pathname:'/disputed-land-information',state:{type:"GRAM_SABHA_LAND"}}}>Gram Sabha Land</Link></DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem> <Link to={{pathname:'/disputed-land-information',state:{type:"LAND_PROHIBITED_FROM_SALE"}}}>Land Prohibited From Sale</Link></DropdownItem>

                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                </li>
                                <li><button className="active" onClick={()=>{this.setState({settingMenuVisible:true})}}><img src={Setting} style={{width:60,height:60,marginTop:"-15px"}}/><br/> Setting</button></li>
                            </ul>
                        </div>
                    </div>
                </div>

                        <div className="CaseRegistrationBox" style={{height:"100%"}}>
                            <div className="container" style={{background: "#FFF", border: "2px solid lightGray"}}>
                                <div >
                                    <div style={{
                                        background: "#B7DCFD",
                                        border: "1px solid black",
                                        marginTop: "-15px",
                                        width: "135px",
                                        textAlign: "center"
                                    }}><b>Case Details</b></div>
                                    <div style={{paddingTop: "20px", paddingBottom: "20px"}}>
                                        <div className="row">
                                            <div className="col-sm-2"><p>Case Type</p></div>
                                            <div className='col-sm-4'>
                                                <select name={'vaadType'} className="custom-select" value={this.state.vaadType} onChange={this.handleChange} >
                                                <option value='default'>Select the Case type</option>
                                                {
                                                    this.state.type.map( item => {
                                                        return <option value={item}> {item} </option>
                                                    })
                                                }
                                            </select>
                                            <span className='text-danger'>
                                                {this.validator.message('vaadType', this.state.vaadType, 'required' )}
                                            </span>
                                            </div>
                                            <div className="col-sm-2"><p>Act</p></div>
                                            <div className='col-sm-4'>
                                                <select className="custom-select" name={'Act'} value={this.state.Act} onChange={e=>this.handleChangeAct(e)}>
                                                    <option selected>Choose Act</option>
                                                    {this.state.getAct.map((itm,key)=>{
                                                        return(
                                                            
                                                            <option value={itm._id}>{itm.name}</option>
                                                        )
                                                    })}
                                                    {/* <option value="2">Two</option>
                                                    <option value="3">Three</option> */}
                                                </select>
                                                <span className='text-danger'>
                                                            {this.validator.message('Act', this.state.Act, 'required' )}
                                                </span>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-sm-2"><p>Registration Date</p></div>
                                            <div className='col-sm-4'>
                                                <div>
                                                    <input type="date" id="date-picker-example" name={'RegistrationDate'} value={this.state.RegistrationDate} className="form-control datepicker" onChange={this.handleChange}/>
                                                    <span className='text-danger'>
                                                            {this.validator.message('vaadType', this.state.RegistrationDate, 'required' )}
                                                    </span>
                                                </div>
                                            </div>
                                            <div className="col-sm-2"><p>Section</p></div>
                                            <div className='col-sm-4'>
                                                <select className="custom-select" name={'section'} value={this.state.section} onChange={this.handleChange}>
                                                <option selected>Choose Section</option>
                                                {this.state.filterMapSection && this.state.filterMapSection.map(itm=>{
                                                    return(
                                                        <option value={itm._id}>{itm.name}</option>
                                                    )
                                                })}
                                                {/* <option value="One">One</option>
                                                <option value="Two">Two</option>
                                                <option value="Three">Three</option> */}
                                            </select>
                                            <span className='text-danger'>
                                                            {this.validator.message('section', this.state.section, 'required' )}
                                                    </span>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-sm-2"><p>Target Date</p></div>
                                            <div className='col-sm-4'>
                                                <div>
                                                    <input type="date" id="date-picker-example" name={'targetDate'} value={this.state.targetDate} className="form-control datepicker" onChange={this.handleChange}/>
                                                    <span className='text-danger'>
                                                            {this.validator.message('targetDate', this.state.targetDate, 'required' )}
                                                    </span>
                                                </div>
                                            </div>
                                            <div className="col-sm-2"><p>Case Direction</p></div>
                                            <div className='col-sm-4'>
                                                <select className="custom-select" name={'direction'} value={this.state.direction} onChange={this.handleChange}>
                                                <option selected>Choose Direction</option>
                                                <option value="HIGH_COURT">High Court</option>
                                                <option value="SUPREME_COURT">Supreme Court</option>
                                                <option value="BOARD_OF_REVENUE">Board of Revenue</option>
                                                <option value="NONE">None</option>
                                            </select>
                                            <span className='text-danger'>
                                                            {this.validator.message('direction', this.state.direction, 'required' )}
                                                    </span>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-sm-2"><p>Case ID</p></div>
                                            <div className='col-sm-4'>
                                                <div>
                                                    <input type="text" name={'caseId'} value={this.state.caseId} placeholder="Enter Unique Case Id" className="form-control datepicker" onChange={this.handleChange}/>
                                                    <span className='text-danger'>
                                                            {this.validator.message('caseId', this.state.caseId, 'required' )}
                                                    </span>
                                                </div>
                                            </div>
                                            <div className="col-sm-2"><p>Court</p></div>
                                            <div className='col-sm-4'>
                                                <div>
                                                    <select  name={'courtSelect'} value={this.state.courtSelect} placeholder="Enter Unique Case Id" className="form-control datepicker" onChange={this.handleChange}>
                                                    <option  value="difault">Select your Court type</option>
                                                        {/* <option  value="Revenue Court">Revenue Court</option> */}
                                                        {this.state.courtName && this.state.courtName.map(itm=>{
                                                            return(
                                                                
                                                                <option  value={itm.name}>{itm.name}</option>
                                                            )
                                                        })}
                                                        {/* <option  value="DM_Court">DM Court</option> */}
                                                        {/* <option  value="ADM(F/R)">ADM (E)</option>
                                                        <option  value="ADM(F/R)">ADM (F/R)</option>
                                                        <option  value="ADM(F/R)">ADM (F/R)</option>
                                                        <option  value="ADM(F/R)">ADM (F/R)</option>
                                                        <option  value="SDM">SDM</option>
                                                        <option  value="Tesildar">Tesildar</option>
                                                        <option  value="NayabTesildar">Nayab Tesildar</option> */}
                                                        </select>
                                                    <span className='text-danger'>
                                                            {this.validator.message('courtSelect', this.state.courtSelect, 'required' )}
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style={{paddingTop: "30px"}}>
                                <div className="container" style={{background: "#FFF", border: "2px solid lightGray"}}>
                                    <div>
                                        <div style={{
                                            background: "#B7DCFD",
                                            border: "1px solid black",
                                            marginTop: "-15px",
                                            width: "135px",
                                            textAlign: "center"
                                        }}><b>Petitioner Details</b></div>
                                        <div style={{paddingTop: "20px", paddingBottom: "20px"}}></div>

                                        <div className="row">
                                            {this.state.complainant.map((itm,idx)=>{
                                                return(
                                                    <>
                                            <div className='col-sm-6'>
                                                <table className="newCaseRegistrationTable" style={{border: "none"}}>
                                                    <tr style={{border: "none"}}>
                                                        <td style={{border: "none"}}><p>Name of Petitioner</p></td>
                                                        <td style={{border: "none"}}><input name={'name'} value={this.state.complainant[idx].name} type={'text'} onChange={(e)=>this.handleChangeMap(e,idx,"1")}/>
                                                        <span className='text-danger'>
                                                            {this.validator.message('name', this.state.complainant[idx].name, 'required' )}
                                                    </span>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style={{border: "none"}}><p>Father/ Husband Name</p></td>
                                                        <td style={{border: "none"}}>
                                                            <input name={'fatherHusbandName'} value={this.state.complainant[idx].fatherHusbandName} type={'text'} onChange={(e)=>this.handleChangeMap(e,idx,"1")}/>
                                                            <span className='text-danger'>
                                                            {this.validator.message('fatherHusbandName', this.state.complainant[idx].fatherHusbandName, 'required' )}
                                                    </span>
                                                            </td>
                                                    </tr>
                                                    <tr>
                                                        <td style={{border: "none"}}><p>Contact No</p></td>
                                                        <td style={{border: "none"}}><input name={'mobile'} value={this.state.complainant[idx].mobile} type={'text'} onChange={(e)=>this.handleChangeMap(e,idx,"1")}/>
                                                        <span className='text-danger'>
                                                            {this.validator.message('mobile', this.state.complainant[idx].mobile, 'required' )}
                                                    </span></td>
                                                    </tr>
                                                    <tr>
                                                        <td style={{border: "none"}}><p>District</p></td>
                                                        <td style={{border: "none"}}>
                                                        <select name={'districtId'} style={{width:"60%"}} value={this.state.complainant[idx].districtId}  onChange={(e)=>this.handleChangeMap(e,idx,"1")}>
                                                            <option value="default">Select the District</option>
                                                        {this.state.getDist && this.state.getDist.map(itm=>{
                                                            return(
                                                                <>
                                                                <option value={itm._id}>{itm.name}</option>
                                                            </>
                                                            )
                                                        })}
                                                        </select>
                                                        <span className='text-danger'>
                                                            {this.validator.message('districtId', this.state.complainant[idx].districtId, 'required' )}
                                                    </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style={{border: "none"}}><p>Petitioner Email</p></td>
                                                        <td style={{border: "none"}}><input name={'complainantEmail'} value={this.state.complainant[idx].complainantEmail} type={'text'} onChange={(e)=>this.handleChangeMap(e,idx,"1")}/>
                                                        <span className='text-danger'>
                                                            {this.validator.message('complainantEmail', this.state.complainant[idx].complainantEmail, 'required' )}
                                                    </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div className='col-sm-6'>
                                                <div className="newCaseRegistrationTable">
                                                    <table style={{border: "none"}}>
                                                        <tr style={{border: "none"}}>
                                                            <td style={{border: "none"}}><p>Address</p></td>
                                                            <td style={{border: "none"}}><textarea rows="4" cols="50"  name={'address'} value={this.state.complainant[idx].address} form="usrform" onChange={(e)=>this.handleChangeMap(e,idx,"1")}>
                                                                 </textarea>
                                                                 <span className='text-danger'>
                                                            {this.validator.message('address', this.state.complainant[idx].address, 'required' )}
                                                    </span>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td style={{border: "none"}}><p>Advocate Name</p></td>
                                                            <td style={{border: "none"}}><input name={'advocateName'} value={this.state.complainant[idx].advocateName} v type={'text'} onChange={(e)=>this.handleChangeMap(e,idx,"1")}/>
                                                            <span className='text-danger'>
                                                            {this.validator.message('advocateName', this.state.complainant[idx].advocateName, 'required' )}
                                                    </span>
                                                            </td>
                                                        </tr>


                                                        <tr>
                                                            <td style={{border: "none"}}><p>Advocate Contact No</p></td>
                                                            <td style={{border: "none"}}><input name={'advocateMobile'} value={this.state.complainant[idx].advocateMobile} type={'text'} onChange={(e)=>this.handleChangeMap(e,idx,"1")}/>
                                                            <span className='text-danger'>
                                                            {this.validator.message('advocateMobile', this.state.complainant[idx].advocateMobile, 'required' )}
                                                    </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style={{border: "none"}}><p>Advocate Email</p></td>
                                                            <td style={{border: "none"}}><input name={'advocateEmail'} value={this.state.complainant[idx].advocateEmail} type={'text'} onChange={(e)=>this.handleChangeMap(e,idx,"1")}/>
                                                            <span className='text-danger'>
                                                            {this.validator.message('advocateEmail', this.state.complainant[idx].advocateEmail, 'required' )}
                                                    </span>
                                                            </td>
                                                        </tr>
                                                        <tr style={{}}>
                                                        <td style={{border:"none",position:"absolute",left:"-55px"}}>
                                                        <span style={{border:"none",cursor:"pointer",borderRadius:"50%",padding:"10px"}} onClick={()=>{this.addMore("1")}}>+</span>
                                                        <span>/</span>
                                                        <span style={{border:"none",cursor:"pointer",borderRadius:"50%",padding:"10px"}} onClick={()=>{this.remove(idx,"1")}}>-</span>
                                                        </td>
                                                        </tr>

                                                        
                                                    </table>
                                                </div>

                                            </div>
                                            
                                                    </>
                                                )
                                            })}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="container"
                                 style={{background: "#C9E5FD", marginTop: "30px", textAlign: "center"}}><p
                                style={{background: "#C9E5FD"}}><b>Versus</b></p></div>

                            <div style={{paddingTop: "30px"}}>
                                <div className="container" style={{background: "#FFF", border: "2px solid lightGray"}}>
                                    <div>
                                        <div style={{
                                            background: "#B7DCFD",
                                            border: "1px solid black",
                                            marginTop: "-15px",
                                            width: "135px",
                                            textAlign: "center"
                                        }}><b>Respondent Details</b></div>
                                        <div style={{paddingTop: "20px", paddingBottom: "20px"}}></div>
                                        <div className="row">
                                        {this.state.defendant.map((itm,idx)=>{
                                            return(
                                                <>
                                            <div className='col-sm-6'>
                                                <table style={{border: "none"}}>
                                                    <tr style={{border: "none"}}>
                                                        <td style={{border: "none"}}><p>Name of Respondent</p></td>
                                                        <td style={{border: "none"}}><input name={'name'} type={'text'} value={this.state.defendant[idx].name} onChange={(e)=>this.handleChangeMap(e,idx,"2")}/>
                                                        <span className='text-danger'>
                                                            {this.validator.message('name', this.state.defendant[idx].name, 'required' )}
                                                    </span>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style={{border: "none"}}><p>Father/ Husband Name</p></td>
                                                        <td style={{border: "none"}}><input type={'text'} name={'fatherHusbandName'} value={this.state.defendant[idx].fatherHusbandName} onChange={(e)=>this.handleChangeMap(e,idx,"2")}/>
                                                        <span className='text-danger'>
                                                            {this.validator.message('fatherHusbandName', this.state.defendant[idx].fatherHusbandName, 'required' )}
                                                    </span>
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td style={{border: "none"}}><p>Contact No</p></td>
                                                        <td style={{border: "none"}}><input name={'mobile'} type={'text'} value={this.state.defendant[idx].mobile} onChange={(e)=>this.handleChangeMap(e,idx,"2")} />
                                                        <span className='text-danger'>
                                                            {this.validator.message('mobile', this.state.defendant[idx].mobile, 'required' )}
                                                    </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        < td style={{border: "none"}}><p>District</p></td>
                                                        <td style={{border: "none"}}>
                                                        <select name={'districtId'} style={{width:"60%"}}  value={this.state.defendant[idx].districtId} onChange={(e)=>this.handleChangeMap(e,idx,"2")} >
                                                        {this.state.getDist && this.state.getDist.map(itm=>{
                                                            return(
                                                                <>
                                                                
                                                                <option value={itm._id}>{itm.name}</option>
                                                            </>
                                                            )
                                                        })}
                                                        </select>    
                                                        <span className='text-danger'>
                                                            {this.validator.message('districtId', this.state.defendant[idx].districtId, 'required' )}
                                                    </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        < td style={{border: "none"}}><p>Respondent Email</p></td>
                                                        <td style={{border: "none"}}><input name={'defendantEmail'} type={'text'} value={this.state.defendant[idx].defendantEmail} onChange={(e)=>this.handleChangeMap(e,idx,"2")} />
                                                        <span className='text-danger'>
                                                            {this.validator.message('defendantEmail', this.state.defendant[idx].defendantEmail, 'required' )}
                                                    </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div className='col-sm-6'>
                                                <div>
                                                    <table style={{border: "none"}}>
                                                        <tr>
                                                            <td style={{border: "none"}}><p>Address</p></td>
                                                            <td style={{border: "none"}}><textarea name={'address'} value={this.state.defendant[idx].address} rows="4" cols="50" form="usrform" onChange={(e)=>this.handleChangeMap(e,idx,"2")}> </textarea>
                                                            <span className='text-danger'>
                                                            {this.validator.message('address', this.state.defendant[idx].address, 'required' )}
                                                    </span>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td style={{border: "none"}}><p>Advocate Name</p></td>
                                                            <td style={{border: "none"}}><input type={'text'} name={'advocateName'} value={this.state.defendant[idx].advocateName} onChange={(e)=>this.handleChangeMap(e,idx,"2")} />
                                                            <span className='text-danger'>
                                                            {this.validator.message('advocateName', this.state.defendant[idx].advocateName, 'required' )}
                                                    </span>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td style={{border: "none"}}><p>Advocate Contact No</p></td>
                                                            <td style={{border: "none"}}><input type={'text'} name={'advocateMobile'} value={this.state.defendant[idx].advocateMobile} onChange={(e)=>this.handleChangeMap(e,idx,"2")} />
                                                            <span className='text-danger'>
                                                            {this.validator.message('advocateMobile', this.state.defendant[idx].advocateMobile, 'required' )}
                                                    </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style={{border: "none"}}><p>Advocate Email</p></td>
                                                            <td style={{border: "none"}}><input type={'text'} name={'advocateEmail'} value={this.state.defendant[idx].advocateEmail} onChange={(e)=>this.handleChangeMap(e,idx,"2")} />
                                                            <span className='text-danger'>
                                                            {this.validator.message('advocateEmail', this.state.defendant[idx].advocateEmail, 'required' )}
                                                    </span>
                                                            </td>
                                                        </tr>
                                                        <tr style={{}}>
                                                        <td style={{border:"none",position:"absolute",left:"-55px"}}>
                                                        <span style={{border:"none",cursor:"pointer",borderRadius:"50%",padding:"10px"}} onClick={()=>{this.addMore("2")}}>+</span>
                                                        <span>/</span>
                                                        <span style={{border:"none",cursor:"pointer",borderRadius:"50%",padding:"10px"}} onClick={()=>{this.remove(idx,"2")}}>-</span>
                                                        </td>
                                                        </tr>
                                                        {/* <tr>
                                                        <td style={{border:"none"}} onClick={()=>{this.addMore("2")}}><span style={{background:"rgb(201, 229, 253)",padding:"10px",borderRadius:"50px"}}>Add</span></td>
                                                        <td style={{border:"none"}} onClick={()=>{this.remove(idx,"2")}}><span style={{background:"red",padding:"10px",borderRadius:"50px"}}>Remove</span></td>
                                                        </tr> */}
                                                    </table>
                                                </div>

                                            </div>
                                                </>
                                            )
                                        })}
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div style={{paddingTop: "30px"}}>
                                <div className="container" style={{background: "#FFF", border: "2px solid lightGray"}}>
                                    <div>
                                        <div style={{
                                            background: "#B7DCFD",
                                            border: "1px solid black",
                                            marginTop: "-15px",
                                            width: "200px",
                                            textAlign: "center"
                                        }}><b>Fixed Proceeding Details</b></div>
                                        <div style={{paddingTop: "20px", paddingBottom: "20px"}}></div>

                                        <div className="row">
                                            <div className='col-sm-6'>
                                                <div className="row">
                                                    <div className="col-sm-4"><p>Fixed Date</p></div>
                                                    <div className='col-sm-6'> 
                                                     <input type="date" id="date-picker-example" name={'fixedDate'} className="form-control datepicker" onChange={this.handleChange}/>
                                                     <span className='text-danger'>
                                                            {this.validator.message('fixedDate', this.state.fixedDate, 'required' )}
                                                    </span>
                                                        <div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            {/* <div className='col-sm-6'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>Fixed Proceeding</p></div>
                                                    <div className='col-sm-6'>
                                                        <select className="custom-select" name={'fixedProceeding'} onChange={this.handleChange}>
                                                            <option value="DEFAULT">Select the Proceeding</option>
                                                            <option value="ADMISSIBLE">Admissible</option>
                                                            <option value="REGISTERED">Registered</option>
                                                            <option value="NOTICE">Notice</option>
                                                            <option value="EVIDENCE">Evidence</option>
                                                            <option value="ARGUMENT">Argument</option>
                                                            <option value="JUDGEMENT">Judgement</option>
                                                            <option value="ORDER">Order</option>
                                                        </select>
                                                        <span className='text-danger'>
                                                            {this.validator.message('fixedProceeding', this.state.fixedProceeding, 'required' )}
                                                    </span>
                                                    </div>
                                                </div>
                                            </div> */}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style={{paddingTop: "30px"}}>
                                <div className="container" style={{background: "#FFF", border: "2px solid lightGray"}}>
                                    <div>
                                        <div style={{
                                            background: "#B7DCFD",
                                            border: "1px solid black",
                                            marginTop: "-15px",
                                            width: "200px",
                                            textAlign: "center"
                                        }}><b>Court Fees</b></div>
                                        <div style={{paddingTop: "20px", paddingBottom: "20px"}}></div>

                                        <div className="row">
                                            <div className='col-sm-6'>
                                                <div className="row">
                                                    <div className="col-sm-4"><p>Valuation of Property</p></div>
                                                    <div className='col-sm-6'>

                                                     <input type="text"  name={'valuationProperty'} className="form-control datepicker" onChange={this.handleChange}/>
                                                     <span className='text-danger'>
                                                            {this.validator.message('valuationProperty', this.state.valuationProperty, 'required' )}
                                                    </span>
                                                        <div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div className='col-sm-6'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>Valuation Land Revenue</p></div>
                                                    <div className='col-sm-6'>
                                                                                                                   
                                                     <input type="text"  name={'valuationLandRevenue'} className="form-control datepicker" onChange={this.handleChange}/>
                                                     <span className='text-danger'>
                                                            {this.validator.message('valuationLandRevenue', this.state.valuationLandRevenue, 'required' )}
                                                    </span>
                                                        {/* <select className="custom-select" name={'FixedProceeding'} onChange={this.handleChange}>
                                                            <option selected>Admissibility</option>
                                                            <option value="1">One</option>
                                                            <option value="2">Two</option>
                                                            <option value="3">Three</option>
                                                        </select> */}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-sm-6'>
                                                <div className="row">
                                                    <div className="col-sm-4"><p>Court Fee</p></div>
                                                    <div className='col-sm-6'>

                                                     <input type="text"  name={'courtFee'} className="form-control" onChange={this.handleChange}/>
                                                     <span className='text-danger'>
                                                            {this.validator.message('courtFee', this.state.courtFee, 'required' )}
                                                    </span>
                                                        <div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {this.state.filterMap && this.state.filterMap.map(itm=>{
                                if(itm === "LAND"){
                                    return(
                                        <>
                                <div style={{paddingTop: "30px"}}>
                                <div className="container" style={{background: "#FFF", border: "2px solid lightGray"}}>
                                    <div>
                                        <div style={{
                                            background: "#B7DCFD",
                                            border: "1px solid black",
                                            marginTop: "-15px",
                                            width: "200px",
                                            textAlign: "center"
                                        }}><b>Land Description</b></div>
                                        <div style={{paddingTop: "20px", paddingBottom: "20px"}}></div>

                                        <div className="row">

                                            <div className='col-sm-6'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>District</p></div>
                                                    <div className='col-sm-6'>
                                                        <select className="custom-select" name={'landDistrict'} onChange={this.handleChange}>
                                                            <option selected>Select District</option>
                                                            {this.state.getDist && this.state.getDist.map(itm=>{
                                                                return(
                                                                    <>
                                                                    <option value={itm._id}>{itm.name}</option>
                                                                    </>
                                                                )
                                                            })}
                                                            {/* <option value="one">District One</option>
                                                            <option value="two">District Two</option>
                                                            <option value="three">District Three</option> */}
                                                        </select>
                                                        {/* <span className='text-danger'>
                                                            {this.validator.message('LandDistrict', this.state.LandDistrict, 'required' )}
                                                    </span> */}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-sm-6'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>Tehsil</p></div>
                                                    <div className='col-sm-6'>
                                                        <select className="custom-select" name={'landTehsil'} onChange={this.handleChange}>
                                                            <option selected> Select Tehsil</option>
                                                            {this.state.getTehsil && this.state.getTehsil.map(itm=>{
                                                                return(
                                                                    <option value={itm._id}>{itm.name}</option>
                                                                )
                                                            })}
                                                            {/* <option value="one">Tehsil One</option>
                                                            <option value="two">Tehsil Two</option>
                                                            <option value="three">Tehsil Three</option> */}
                                                        </select>
                                                        {/* <span className='text-danger'>
                                                            {this.validator.message('Tehsil', this.state.Tehsil, 'required' )}
                                                    </span> */}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-sm-6'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>Pargana</p></div>
                                                    <div className='col-sm-6'>
                                                        <select className="custom-select" name={'landPargana'} onChange={this.handleChange}>
                                                            <option selected>Select Pargana</option>
                                                            {this.state.getPargana && this.state.getPargana.map(itm=>{
                                                                return(
                                                                    <>
                                                                    <option value={itm._id}>{itm.name}</option>
                                                                    </>
                                                                )
                                                            })}
                                                            {/* <option value="one">Pargana One</option>
                                                            <option value="two">Pargana Two</option>
                                                            <option value="three">Pargana Three</option> */}
                                                        </select>
                                                        {/* <span className='text-danger'>
                                                            {this.validator.message('Pargana', this.state.Pargana, 'required' )}
                                                    </span> */}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-sm-6'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>Village</p></div>
                                                    <div className='col-sm-6'>
                                                        <select className="custom-select" name="landVillage" onChange={this.handleChange}>
                                                            <option selected>Select Village</option>
                                                            {this.state.getVillage && this.state.getVillage.map(itm=>{
                                                                return(
                                                                    <option value={itm._id}>{itm.name}</option>
                                                                )
                                                            })}
                                                            {/* <option value="one"> One Village</option>
                                                            <option value="two"> Two Village</option>
                                                            <option value="three">Three Village</option> */}
                                                        </select>
                                                        {/* <span className='text-danger'>
                                                            {this.validator.message('village', this.state.village, 'required' )}
                                                    </span> */}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-sm-6'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>Type of Land</p></div>
                                                    <div className='col-sm-6'>
                                                        <select className="custom-select" name="landTypeSelect" onChange={(e)=>this.handleChangeLandType(e)}>
                                                            <option selected>Select Land Type</option>
                                                            {this.state.landType.map((itm,key)=>{
                                                                return(
                                                            <option value={itm}>{itm}</option>
                                                                    
                                                                )
                                                            })}
                                                            {/* <option value="2">NZA</option>
                                                            <option value="3">Three Land</option> */}
                                                        </select>
                                                        
                                                        {/* <span className='text-danger'>
                                                            {this.validator.message('landTypeSelect', this.state.landTypeSelect, 'required' )}
                                                    </span> */}
                                                    </div>
                                                    {this.state.landTypeSelect ==="ZA" && <>
                                                    <div className='col-md-12' style={{marginTop:"20px"}}>
                                                    <div className="row">
                                                    <div className="col-sm-4"><p>No Land Section</p></div>
                                                    <div className='col-sm-6'>
                                                     <input type="number"  name={'landSection'} className="form-control datepicker" onChange={this.handleChange}  min="1" max="100"/>
                                                     
                                                        <div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                </>
                                            }
                                                {(this.state.landTypeSelect === "NZA" || this.state.landTypeSelect === "Survey") &&
                                            <>
                                           
                                            <div className='col-md-12' style={{marginTop:"20px"}}>
                                                <div className="row">
                                                    <div className="col-sm-4"><p>Gata Number</p></div>
                                                    <div className='col-sm-6'>
                                                     <input type="text"  name={'gataNumber'} className="form-control datepicker" onChange={this.handleChange}/>
                                                       
                                                        <div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div className='col-md-12' style={{marginTop:"20px"}}>
                                                <div className="row">
                                                    <div className="col-sm-4"><p>Rakba</p></div>
                                                    <div className='col-sm-6'>
                                                     <input type="text"  name={'rakba'} className="form-control datepicker" onChange={this.handleChange}/>
                                                        <div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            </>
                                            }
                                                </div>
                                            </div>
                                            <div className='col-sm-6'>
                                                {this.state.landTypeSelect !== "ZA" && 
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>Type of Disputed Land</p></div>
                                                    <div className='col-sm-6'>
                                                        <select className="custom-select" name="disputedLand" onChange={this.handleChange}>
                                                            <option selected>Select Village</option>
                                                            <option value="GOLDEN_FOREST_LAND">Golden forest Land</option>
                                                            <option value="GRAM_SABHA_LAND">Gram Sabha Land</option>
                                                            <option value="LAND_PROHIBITED_FROM_SALE">Land prohibited from Sale.</option>
                                                            <option value="NONE">None</option>
                                                            {/* <option value="none">None</option> */}
                                                        </select>
                                                        {/* <span className='text-danger'>
                                                            {this.validator.message('disputedLand', this.state.disputedLand, 'required' )}
                                                    </span> */}
                                                    </div>
                                                </div>}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                        </>
                                    )
                                }
                                else if(itm === "STAMP"){
                                    return(
                                        <>
                            <div style={{paddingTop: "30px"}}>
                            <div className="container" style={{background: "#FFF", border: "2px solid lightGray"}}>

                                            
                                        


                                    <div>
                                        <div style={{
                                            background: "#B7DCFD",
                                            border: "1px solid black",
                                            marginTop: "-15px",
                                            width: "200px",
                                            textAlign: "center"
                                        }}><b>Stamp Duty Description</b></div>
                                        <div style={{paddingTop: "20px", paddingBottom: "20px"}} ></div>

                                        <div className="row">

                                            <div className='col-sm-6'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>Paid Stamp Duty</p></div>
                                                    <div className='col-sm-6'>
                                                        <input type={'number'} name={'paid'} onChange={this.handleChange}/>
                                                        {/* <span className='text-danger'>
                                                            {this.validator.message('paid', this.state.paid, 'required' )}
                                                    </span> */}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-sm-6'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>Stamp duty indicated in reference</p></div>
                                                    <div className='col-sm-6'>
                                                        <input type={'number'} name={'reference'} onChange={this.handleChange}/>
                                                        {/* <span className='text-danger'>
                                                            {this.validator.message('reference', this.state.reference, 'required' )}
                                                    </span> */}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-sm-6'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>Indicated stamp duty reduction</p></div>
                                                    <div className='col-sm-6'>
                                                        <input type={'number'} name={'reduction'} onChange={this.handleChange} />
                                                        {/* <span className='text-danger'>
                                                            {this.validator.message('reduction', this.state.reduction, 'required' )}
                                                    </span> */}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                        </>
                                    )
                                }
                                else if(itm === "ARMS"){
                                    return(
                                        <>
                            <div style={{paddingTop: "30px"}}>
                                <div className="container" style={{background: "#FFF", border: "2px solid lightGray"}}>
                                    <div>
                                        <div style={{
                                            background: "#B7DCFD",
                                            border: "1px solid black",
                                            marginTop: "-15px",
                                            width: "200px",
                                            textAlign: "center"
                                        }}><b>Arms</b></div>
                                        <div style={{paddingTop: "20px", paddingBottom: "20px"}} ></div>

                                        <div className="row">

                                            <div className='col-sm-6'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>Type of Arms</p></div>
                                                    <div className='col-sm-6'>
                                                        <select className="custom-select" name="arms" onChange={this.handleChange}>
                                                            <option selected>Select Land Type</option>
                                                            <option value="Monolithic">Monolithic</option>
                                                            <option value="Double_Baralled">Double Baralled</option>
                                                            <option value="Rifle">Rifle</option>
                                                            <option value="Revolver">Revolver</option>
                                                            <option value="Pistol">Pistol</option>
                                                            <option value="Carbine">Carbine</option>
                                                        </select>
                                                        {/* <span className='text-danger'>
                                                            {this.validator.message('arms', this.state.arms, 'required' )}
                                                    </span> */}
                                                    </div>
                                                </div>
                                            </div>


                                            <div className='col-sm-6'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>District</p></div>
                                                    <div className='col-sm-6'>
                                                        <select className="custom-select" name="armsdistrict" onChange={this.handleChange}>
                                                            <option selected>Select District</option>
                                                            {this.state.getDist && this.state.getDist.map(itm=>{
                                                                return(
                                                                    <option value={itm._id}>{itm.name}</option>
                                                                )
                                                            })}
                                                            {/* <option value="dehradun">DEHRADUN</option> */}
                                                            {/* <option value="2"> Two District</option>
                                                            <option value="3">Three District</option> */}
                                                        </select>
                                                        {/* <span className='text-danger'>
                                                            {this.validator.message('district', this.state.district, 'required' )}
                                                    </span> */}
                                                    </div>
                                                </div>
                                            </div>



                                            <div className='col-sm-6'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>Arms license number</p></div>
                                                    <div className='col-sm-6'>
                                                        <input type={'text'} name={'armsLicenseNumber'} value={this.state.armsLicenseNumber} onChange={this.handleChange}/>
                                                        {/* <span className='text-danger'>
                                                            {this.validator.message('armsLicenseNumber', this.state.armsLicenseNumber, 'required' )}
                                                    </span> */}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-sm-6'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>Weapon number</p></div>
                                                    <div className='col-sm-6'>
                                                        <input type={'text'} name={'weaponNumber'} onChange={this.handleChange} />
                                                        {/* <span className='text-danger'>
                                                            {this.validator.message('weaponNumber', this.state.weaponNumber, 'required' )}
                                                    </span> */}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-sm-6'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>Thana</p></div>
                                                    <div className='col-sm-6'>
                                                        <input type={'text'} name={'armsthana'} onChange={this.handleChange} />
                                                        {/* <span className='text-danger'>
                                                            {this.validator.message('thana', this.state.thana, 'required' )}
                                                    </span> */}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                        </>
                                    )
                                }
                                else if(itm === "DESCRIPTION"){
                                    return(
                                        <>

                            <div style={{paddingTop: "30px"}}>
                                <div className="container" style={{background: "#FFF", border: "2px solid lightGray"}}>
                                    <div>
                                        <div style={{
                                            background: "#B7DCFD",
                                            border: "1px solid black",
                                            marginTop: "-15px",
                                            width: "200px",
                                            textAlign: "center"
                                        }}><b>Description</b></div>
                                        <div style={{paddingTop: "20px", paddingBottom: "20px"}} ></div>


                                        <div className="row">

                                            <div className='col-sm-6'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>District</p></div>
                                                    <div className='col-sm-6'>
                                                        <select className="custom-select" name="districtDescription" onChange={this.handleChange}>
                                                            <option selected>Select District</option>
                                                            {this.state.getDist && this.state.getDist.map(itm=>{
                                                                return(
                                                                    <option value={itm._id}>{itm.name}</option>
                                                                )
                                                            })}
                                                            {/* <option value="dehradun">DEHRADUN</option> */}
                                                            {/* <option value="2"> Two District</option>
                                                            <option value="3">Three District</option> */}
                                                        </select>
                                                        {/* <span className='text-danger'>
                                                            {this.validator.message('district', this.state.districtDescription, 'required' )}
                                                    </span> */}
                                                    </div>
                                                </div>
                                            </div>

                                            <div className='col-sm-6'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>Tehsil</p></div>
                                                    <div className='col-sm-6'>
                                                        <select className="custom-select" name="tehsilDescription" onChange={this.handleChange}>
                                                            <option selected>Select Tehsil</option>
                                                            {this.state.getTehsil && this.state.getTehsil.map(itm=>{
                                                                return(

                                                                    <option  value={itm._id}>{itm.name}</option>
                                                                )
                                                            })}
                                                        {/* <option  value="VikasNamger">VikasNamger</option>
                                                        <option  value="Sadar">Sadar</option>
                                                        <option  value="Tiyoni">Tiyoni</option>
                                                        <option  value="Rishikash">Rishikash</option>
                                                        <option  value="Kalsa">Kalsa</option>
                                                        <option  value="Dojwals">Dojwals</option> */}
                                                        </select>
                                                        {/* <span className='text-danger'>
                                                            {this.validator.message('tehsil', this.state.tehsilDescription, 'required' )}
                                                    </span> */}
                                                    </div>
                                                </div>
                                            </div>


                                            <div className='col-sm-6'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>Thana</p></div>
                                                    <div className='col-sm-6'>
                                                        <input type={'text'} name={'thanaDescription'} onChange={this.handleChange}/>
                                                        {/* <span className='text-danger'>
                                                            {this.validator.message('Thana', this.state.thanaDescription, 'required' )}
                                                    </span> */}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-sm-6'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>Date of Chalani Report</p></div>
                                                    <div className='col-sm-6'>
                                                        <input type={'date'} name={'dateOfChalaniReport'} onChange={this.handleChange} />
                                                        {/* <span className='text-danger'>
                                                            {this.validator.message('dateOfChalaniReport', this.state.dateOfChalaniReport, 'required' )}
                                                    </span> */}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-sm-12'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-6"><p>Number of opposition figures mentioned in the Chalani report</p></div>
                                                    <div className='col-sm-6'>
                                                        <input type={'text'} name={'chalaniReport'} onChange={this.handleChange} />
                                                        {/* <span className='text-danger'>
                                                            {this.validator.message('chalaniReport', this.state.chalaniReport, 'required' )}
                                                    </span> */}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                        </>
                                    )
                                }
                                else if(itm === "COMPLAINANT"){
                                    return(
                                        <>

                            <div style={{paddingTop: "30px"}}>
                                <div className="container" style={{background: "#FFF", border: "2px solid lightGray"}}>
                                    <div>
                                        <div style={{
                                            background: "#B7DCFD",
                                            border: "1px solid black",
                                            marginTop: "-15px",
                                            width: "200px",
                                            textAlign: "center"
                                        }}><b>complainant</b></div>
                                        <div style={{paddingTop: "20px", paddingBottom: "20px"}} ></div>


                                        <div className="row">

                                            <div className='col-sm-6'>
                                                <div className="row" style={{paddingBottom:"30px"}}>
                                                    <div className="col-sm-4"><p>Type of Complainant</p></div>
                                                    <div className='col-sm-6'>
                                                        <select className="custom-select" name="companyType" onChange={this.handleChange}>
                                                            <option selected>Select complainant</option>
                                                            <option value="Govt">Government</option>
                                                            <option value="other">Other</option>
                                                            {/* <option value="3">Three complainant</option> */}
                                                        </select>
                                                        {/* <span className='text-danger'>
                                                            {this.validator.message('companyType', this.state.companyType, 'required' )}
                                                    </span> */}
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                                        </>
                                    )
                                }
                            })}






                            <div className="container" style={{ paddingLeft:"340px",paddingTop:"30px",paddingBottom:"30px"}}>
                                <div className="caseStatusButton" style={{paddingBottom:"50px",paddingLeft:"50px"}}>
                                    <div className="container">
                                        {/* <button type="button" className="btn btn-secondary btn-lg" style={{background:"transparent",borderColor:"blue",borderWidth:"2px",color:"blue", width:"230px"}}>Reset</button> */}
                                  <button onClick={()=>this.submitAddCase()} type="submit" className="btn btn-primary btn-lg" style={{width:"230px",marginLeft:"30px"}}>Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ToastsContainer store={ToastsStore}/>
                        <Footer/>
                    </div>
            );
        }
}
export default newCaseRegistration;