import React, {Component} from 'react';
import logo from "../../assets/logo.jpg";
// import { Link } from "react-router-dom";
import DownArrow from '../../assets/DownArrow.png';
import Dashboard from "../../assets/Dashboard.png";
import CaseRegistration from "../../assets/CaseRegistration.png";
import DailyCauseList from "../../assets/DailyCauseList.png";
import CourtOrder from "../../assets/CourtOrder.png";
import setting from "../../assets/setting.png";
import Archives from "../../assets/Archives.png"
// import Calendar from "../common/calendar";
// import Navbar from "../../common/navbar"
import {Link} from "react-router-dom";
import MasterLoginSetting from "../../common/MasterLoginSetting";
import Footer from "../../common/footer";
import Service from '../../service'
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';


class ViewDailyOrder extends Component {
    constructor(props) {

        super(props);
        this.state = {
            tabContent: 0,
            menuOpenClass: false,
            data:[],
            isActive:true
        };
    }


onHandleChange=(e)=>{
    this.setState({[e.target.name]:e.target.value})
}
onSubmit=async()=>{
const res = await Service.location.getClauseCase(this.state.clauseDate)
if(typeof(res)== "object"){
    if(res.status == true && res.status !==undefined){
        this.setState({isActive:false,data:res.data})
    }
}

console.log("clauseDateeeeee",res)
}
moveToPage=(id)=>{
    const value = id
    localStorage.setItem('page_id',value)
    this.props.history.push('/view-case')
        }
    render() {
        return (
            <div>
                 <div>
                    <div className="logo--heading">
                        <img src={logo} style={{width:80,height:80}}/>
                        <h2>REVENUE COURT OF DEHRADUN</h2>
                    </div>
                    <div className={"setting_menu_main " + (this.state.settingMenuVisible ? "open" : "")}>
                        <span className="close_menu" onClick={()=>{
                            this.setState({
                                settingMenuVisible:false
                            })
                        }}>
                            <i className="mdi mdi-close"></i>
                        </span>
                        <MasterLoginSetting/>
                    </div>
                    <div className="tabs">
                        <button className="active"><Link to="/DM-login"><img src={Dashboard} style={{width:60,height:60}}/></Link><br/> Dashboard</button>
                        <button className="active"> <Link to='/daily-case'><img src={DailyCauseList} style={{width:60,height:60}}/><br/>Daily Cause List</Link> <br/>
                         {/* <img src={DownArrow} style={{width:20,height:20}}/> */}
                         </button>
                        <button className="active"><Link to='/all-case-order'><img src={CourtOrder} style={{width:60,height:60}}/><br/> Court Orders </Link><br/>
                        {/* <img src={DownArrow} style={{width:20,height:20}}/> */}
                        </button>
                        <button className="active"><img src={Archives} style={{width:60,height:60}}/><br/> Archives </button>
                        <button className="active"  onClick={()=>{this.setState({settingMenuVisible:true})}}><img src={setting} style={{width:60,height:60}}/><br/> Setting</button>
                    </div>
                </div>
                        {this.state.isActive ? 
                        <>
                <div className="blueBox1">

                        <form>
                            <div className="causeListTitle">
                                <div className="container">
                                    <h4>Cause List Report</h4>
                                </div>
                            </div>

                            <div className="box_wrapper">
                                <div className="form-group row">
                                <label htmlFor="inputEmail3" className="col-sm-4 col-form-label">Cause List</label>
                                <div className="col-sm-8">
                                    <div className="md-form">
                                        <input type="date" name="clauseDate" onChange={(e)=>{this.onHandleChange(e)}} id="date-picker-example"
                                               className="form-control datepicker"/>
                                    </div>
                                    <div className="caseStatusButton" style={{margin:"20px",textAlign:"center"}}>
                                        {/* <Link to="/cause-list-submit"> */}
                                            <button type="button" onClick={()=>this.onSubmit()} className="btn btn-primary btn-lg">Submit</button>
                                            {/* </Link> */}
                                    </div>
                                </div>
                            </div>
                            </div>


                        </form>
                </div>
                        </>
                        :
                        <>
                        <div className="blueBox1">
                    <div className="tablecontainer">
                        <center> <div><h4> Cause List Report</h4> </div></center>

                        <div className="causeListTable">
                            <table>
                                <tr>
                                    <th>Serial No.</th>
                                    <th>Bench</th>
                                    <th>Registration Date </th>
                                    <th>View Cause List</th>
                                </tr>
                                {this.state.data.length > 0 ?this.state.data.map((itm,key)=>{
                                    return(

                                <tr>
                                    <td>{key+1}</td>
                                    <td>{itm.complainant.length > 0 ? itm.complainant[0].name:"No Complainant Name" }</td>
                                    <td>{itm.registrationDate}</td>
                                    <td><span onClick={()=>this.moveToPage(itm._id)}>View</span></td>
                                </tr>
                                    )
                                })
                                :
                                <tr><td colspan="5"><div style={{color:"red"}}>No result found.</div></td></tr>}
                            </table>
                        </div>

                    </div>
                </div>
                        </>
                            
                        }
                <Footer/>


            </div>
        );
    }
}

export default ViewDailyOrder;