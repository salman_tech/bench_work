import React, {Component} from 'react';
import Navbar from "../../common/navbar"
import {Link} from "react-router-dom";
import Footer from "../../common/footer";
import Service from '../../service'
import SimpleReactValidator from 'simple-react-validator';

class DisputedLandLogin extends Component {


    constructor(props) {

        super(props);
        this.state = {
            tabContent: 0,
            settingMenuVisible:false,
            courtName:[],
            distName:[],
            tehsilName:[],
            parganaName:[]

        };
        this.validator = new SimpleReactValidator({ autoForceUpdate: this });
    }
    componentDidMount(){
        this.getCourt()
        this.getTehsil()
        this.getDist()
        this.getPargana()
    }
    handleChange=(e)=>{
        this.setState({[e.target.name]:e.target.value})
    }
    getPargana=async()=>{
        const res = await Service.location.getPargana()
        if(typeof(res) == "object"){
            if(res && res.status && res.status == true){
                this.setState({parganaName:res.data})
            }
            else{
                alert(res.message)
            }
        }
        else{
            this.setState({parganaName:[]})
            // alert("API")
        }

    }
    getTehsil=async()=>{
        const res = await Service.location.getTehsil()
        if(typeof(res) == "object"){
            if(res && res.status && res.status == true){
                this.setState({tehsilName:res.data})
            }
            else{
                alert(res.message)
            }
        }
        else{
            this.setState({tehsilName:[]})
            // alert("API")
        }

    }
    getDist=async()=>{
        const res = await Service.location.getDistict()
        if(typeof(res) == "object"){
            if(res && res.status && res.status == true){
                this.setState({distName:res.data})
            }
            else{
                alert(res.message)
            }
        }
        else{
            this.setState({distName:[]})
            // alert("API")
        }

    }
    getCourt=async()=>{
        let res  = await Service.location.getCourt()
        if(typeof(res) == "object"){
            if(res && res.status && res.status == true){
                this.setState({courtName:res.data})
            }
            else{
                alert(res.message)
            }
        }
        else{
            this.setState({courtName:[]})
            // alert("API")
        }

    }
    submit=async()=>{
        if (this.validator.allValid()) {
        const id = this.state.caseId
        debugger
        let res = await Service.location.getCaseHearing(id)
        debugger
        if(typeof(res) == "object"){
            if(res.status == true && res.status !==undefined){

                debugger
                // this.setState({data:res.data})
                const caseMoveTo = res.data[0] && res.data[0]._id && res.data[0]._id ? res.data[0]._id : "null" 
                alert(caseMoveTo)
                this.moveToPage(caseMoveTo)
                // alert(res.message)

            }
            else{
                alert(res.message)
            }
        }
        else{
           alert("API Faild")
        }
    }
    else {
        this.validator.showMessages();
        this.forceUpdate();
        // alert("you miss the required filed")
     }
    }


    render() {
        return (
            <div>
                <Navbar />
                <div className="blueBox1" style={{marginBottom:"100px"}}>
                    <div className="container">
                        <div className="box_wrapper">
                            <form>
                            <div className="form-group row">
                                    <label htmlFor="inputEmail3" className="col-sm-4 col-form-label">Rakba Number </label>
                                    
                                    <div className="col-sm-8">
                                        <input className="" style={{width:"100%"}} name="rakbaNumber" onChange={(e)=>this.handleChange(e)} />
                                            {/* <option selected>Select Court</option>
                                            {this.state.courtName && this.state.courtName.map(itm=>{
                                                            return(
                                                                
                                                                <option  value={itm.name}>{itm.name}</option>
                                                            )
                                                        })}
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select> */}
                                        {/* <span className='text-danger'>
                                                {this.validator.message('courtSelect', this.state.courtSelect, 'required' )}
                                        </span> */}
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label htmlFor="inputEmail3" className="col-sm-4 col-form-label">Gata Number </label>
                                    
                                    <div className="col-sm-8">
                                        <input className="" style={{width:"100%"}} name="gataNumber" onChange={(e)=>this.handleChange(e)} />
                                            {/* <option selected>Select Court</option>
                                            {this.state.courtName && this.state.courtName.map(itm=>{
                                                            return(
                                                                
                                                                <option  value={itm.name}>{itm.name}</option>
                                                            )
                                                        })}
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select> */}
                                        {/* <span className='text-danger'>
                                                {this.validator.message('courtSelect', this.state.courtSelect, 'required' )}
                                        </span> */}
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label htmlFor="inputEmail3" className="col-sm-4 col-form-label">Distict Name</label>
                                    <div className="col-sm-8">
                                    <select className="custom-select" name="distId" onChange={(e)=>this.handleChange(e)}>
                                            <option selected>Select District Name</option>
                                            {this.state.distName && this.state.distName.map(itm=>{
                                                            return(
                                                                
                                                                <option  value={itm.name}>{itm.name}</option>
                                                            )
                                                        })}
                                            {/* <option value="1">One</option> */}
                                            {/* <option value="2">Two</option>
                                            <option value="3">Three</option> */}
                                        </select>
                                        {/* <span className='text-danger'>
                                                {this.validator.message('courtSelect', this.state.courtSelect, 'required' )}
                                        </span> */}
                                    </div>
                                    </div>
                                    <div className="form-group row">
                                    <label htmlFor="inputEmail3" className="col-sm-4 col-form-label">Tehsil Name</label>
                                    <div className="col-sm-8">
                                    <select className="custom-select" name="tehsilId" onChange={(e)=>this.handleChange(e)}>
                                            <option selected>Select Tehsil Name</option>
                                            {this.state.tehsilName && this.state.tehsilName.map(itm=>{
                                                            return(
                                                                
                                                                <option  value={itm.name}>{itm.name}</option>
                                                            )
                                                        })}
                                            {/* <option value="1">One</option> */}
                                            {/* <option value="2">Two</option>
                                            <option value="3">Three</option> */}
                                        </select>
                                        {/* <span className='text-danger'>
                                                {this.validator.message('courtSelect', this.state.courtSelect, 'required' )}
                                        </span> */}
                                    </div>
                                    </div>
                                    <div className="form-group row">
                                    <label htmlFor="inputEmail3" className="col-sm-4 col-form-label">Pargana Number</label>
                                    <div className="col-sm-8">
                                    <select className="custom-select" name="distId" onChange={(e)=>this.handleChange(e)}>
                                            <option selected>Select Pargana Name</option>
                                            {this.state.parganaName && this.state.parganaName.map(itm=>{
                                                            return(
                                                                
                                                                <option  value={itm.name}>{itm.name}</option>
                                                            )
                                                        })}
                                            {/* <option value="1">One</option> */}
                                            {/* <option value="2">Two</option>
                                            <option value="3">Three</option> */}
                                        </select>
                                        {/* <span className='text-danger'>
                                                {this.validator.message('courtSelect', this.state.courtSelect, 'required' )}
                                        </span> */}
                                    </div>
                                    </div>
                                {/* <div className="form-group row">
                                    <label htmlFor="inputEmail3" className="col-sm-4 col-form-label">Year</label>
                                    <div className="col-sm-8">
                                        <input type="email" className="form-control" id="inputEmail3"/>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label htmlFor="inputEmail3" className="col-sm-4 col-form-label">Act Number</label>
                                    <div className="col-sm-8">
                                        <input type="email" className="form-control" id="inputEmail3"/>
                                    </div>
                                </div> */}

                            </form>
                            <div className="form-group row">
                                    <label htmlFor="inputEmail3" className="col-sm-4 col-form-label"></label>
                                    <div className="col-sm-8">
                                        <div className="row">
                                        <div className="col-md-2">

                                {/* <button type="button" className="btn btn-secondary btn-lg" style={{background:"white",borderColor:"blue",borderWidth:   "thick",color:"blue"}}>Reset</button> */}
                                
                                        </div>
                                        <div className="col-md-4">

                                        <button type="button" className="btn btn-primary btn-lg" onClick={()=>this.submit()} style={{width:"250%"}}>Submit</button>
                                
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            {/* <div className="caseStatusButton">
                                <div className="container"></div> 
                                 &#160;
                                <Link to="/case-status-submit"><button type="button" className="btn btn-primary btn-lg">Submit</button></Link>
                            </div> */}
                        </div>
                    </div>
                </div>
        

            </div>
        );
    }
}

export default DisputedLandLogin;