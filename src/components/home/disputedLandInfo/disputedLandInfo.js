import React, {Component} from 'react';
import Navbar from "../../common/navbar"
import logo from "../../assets/logo.jpg";
import Dashboard from "../../assets/Dashboard.png";
import {Link} from "react-router-dom";
import CaseRegistration from "../../assets/CaseRegistration.png";
import DailyCauseList from "../../assets/DailyCauseList.png";
import CourtOrder from "../../assets/CourtOrder.png";
import DisputedLandInfo from "../../assets/DisputedLandInfo.png";
import MasterLoginSetting from "../../common/MasterLoginSetting";
import Setting from "../../assets/setting.png";
import Footer from "../../common/footer";
import Service from '../../service'
import DownArrow from "../../assets/DownArrow.png";
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";

class disputedLandInfo extends Component {


    constructor(props) {

        super(props);
        this.state = {
            tabContent: 0,
            menuOpenClass: false,
            data:[]
        };
    }
    async componentDidMount (){
        console.log(this.props)
        const type =this.props.location.state.type
        let res = await Service.location.DisputedLandInfo(type)
        if(typeof(res) =="object"){
            if(res.status == true && res.status !==undefined){
            this.setState({data:res.data})
            
            }
            else{
                this.setState({data:[]})
                alert(res.message)
            }
        }
        else{
            this.setState({data:[]})
            alert("Backend error")
        }

    }
    moveToPage=(id)=>{
        const value = id
        localStorage.setItem('disputed_id',value)
        this.props.history.push('/Case-Status-submit-page')
    }

    render() {
        // console.log("stateeeeeee",this.props)
        return (
            <div>
<div style={{}}>
                    <div className="logo--heading">
                        <img src={logo} style={{width:80,height:80}}/>
                        <h2>REVENUE COURT OF DEHRADUN</h2>
                    </div>

                    <div className={"setting_menu_main " + (this.state.settingMenuVisible ? "open" : "")}>
                        <span className="close_menu" onClick={()=>{
                            this.setState({
                                settingMenuVisible:false
                            })
                        }}>
                            <i className="mdi mdi-close"></i>
                        </span>
                        <MasterLoginSetting/>
                    </div>

                    <div className="PeshkarLoginTabs" >
                        <div className="container">
                            <ul>
                                <li><Link to='/peshkaar-login' ><button className="active"><img src={Dashboard} style={{width:60,height:60,marginTop:"-15px"}}/><br/> Dashboard </button> </Link> </li>

                                <li> <UncontrolledDropdown>
                                    <DropdownToggle  style={{background:"white",color: "black",border:"none"}}>
                                        <img src={CaseRegistration} style={{width:60,height:60}}/><br/> Case Registration <br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem> <Link to='/new-case-registration'>New Case Registration</Link></DropdownItem>
                                        <DropdownItem divider />
                                        <DropdownItem>
                                            <Link to to='/view-all-case'>View Case</Link>
                                        </DropdownItem>
                                        <DropdownItem divider />
                                        {/* <DropdownItem>Delete Case</DropdownItem> */}
                                        <DropdownItem divider />
                                    
                                        <DropdownItem ><Link to='/view-case-status'>Update Case status</Link>
                                        <DropdownItem divider />
                                        </ DropdownItem>
                                        <DropdownItem ><Link >Upload Order Sheet</Link>
                                        <DropdownItem divider />
                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown> </li>

                                <li>
                                    <UncontrolledDropdown>
                                        <DropdownToggle style={{background:"white",color: "black",border:"none"}} >
                                            <img src={DailyCauseList} style={{width:60,height:60}}/><br/>  Daily Cause List <br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem><Link to="/admin-cause-list"> View Daily Cause List </Link></DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Change Prioritiy</DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Next Hearing Date</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                </li>



                                <li>
                                    <UncontrolledDropdown>
                                        <DropdownToggle style={{background:"white",color: "black",border:"none"}} >
                                            <Link to="/all-case-order"><img src={CourtOrder} style={{width:60,height:60}}/><br/>  Court Orders</Link> <br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                         {/* <DropdownMenu>
                                            <DropdownItem>  Type Order </DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Upload Order</DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Upload Order: Disposed </DropdownItem>
                                           <DropdownItem divider />
                                           <DropdownItem>Application Letter: Disposed</DropdownItem> 
                                     </DropdownMenu> */}
                                    </UncontrolledDropdown>
                                </li>


                                <li>
                                    <UncontrolledDropdown>
                                        <DropdownToggle  style={{background:"white",color: "black",border:"none"}}>
                                            <img src={DisputedLandInfo} style={{width:60,height:60}}/><br/>Disputed Land Info<br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem> <Link to={{pathname:'/disputed-land-information',state:{type:"GOLDEN_FOREST_LAND"}}}>Golden Forest Land</Link></DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem> <Link to={{pathname:'/disputed-land-information',state:{type:"GRAM_SABHA_LAND"}}}>Gram Sabha Land</Link></DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem> <Link to={{pathname:'/disputed-land-information',state:{type:"LAND_PROHIBITED_FROM_SALE"}}}>Land Prohibited From Sale</Link></DropdownItem>

                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                </li>
                                <li><button className="active" onClick={()=>{this.setState({settingMenuVisible:true})}}><img src={Setting} style={{width:60,height:60,marginTop:"-15px"}}/><br/> Setting</button></li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div className="PeshkarLoginBlueBox">
                    <div className="tablecontainer">
                        <center> <div> <h4> Cause List Report</h4> </div></center>

                        <div className="causeListTable">
                            <table>
                                <tr>
                                    <th>S.No</th>
                                    {/* <th>{this.props.location.state.type =="GOLDEN_FOREST_LAND"?"No Of landSections":"Khasra/Geta"}</th>
                                    <th>{this.props.location.state.type =="GOLDEN_FOREST_LAND"?"Type Land":"Rakba"}</th> */}
                                    <th>Khasra/Geta</th>
                                    <th>Rakba</th>
                                    <th>View Cause Status</th>
                                </tr>
                                {this.state.data.length > 0 ? this.state.data.map((itm,key)=>{
                                    
                                    return(

                                <tr>
                                    <td>{key+1}</td>
                                    {/* <td>{itm.land && itm.land.typeDisputedLand ==="GOLDEN_FOREST_LAND" ? itm.land.typeLandData.noOflandSections:itm.land.typeLandData.gata }</td>
                                    <td>{itm.land && itm.land.typeDisputedLand ==="GOLDEN_FOREST_LAND" ? itm.land.typeLand:itm.land.typeLandData.rakba }</td>
                                     */}
                                     <td>{itm.land && itm.land.typeLandData.gata }</td>
                                    <td>{itm.land &&  itm.land.typeLandData.rakba }</td>
                                    <td><span onClick={()=>this.moveToPage(itm._id)} style={{color:"#2780d4",cursor:"pointer"}}>View</span></td>
                                </tr>
                                    )
                                })
                                :
                                <tr>
                                    <td colspan="5"><div style={{color:"red"}}>No result found.</div></td>
                                </tr> }
                            
                            </table>
                        </div>

                    </div>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default disputedLandInfo;