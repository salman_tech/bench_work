import React, {Component} from 'react';
import logo from "../../assets/logo.jpg";
import addUser from "../../assets/addUser.png";
import updateUser from "../../assets/updateUser.png";
import setting from "../../assets/setting.png";
import MasterLoginSetting from "../../common/MasterLoginSetting";
import Footer from "../../common/footer";
import {Link} from "react-router-dom";
import AddAct from "../../assets/AddAct.png";
import AddCourt from "../../assets/AddCourt.png";
import axios from "axios";
import {ToastsContainer, ToastsStore} from 'react-toasts';
import SimpleReactValidator from 'simple-react-validator';
import Service from '../../service'

class addUsers extends Component {


    constructor(props) {
        super(props);
        this.state = {
            username:'',
            name:'',
            oldPassword:'',
            newPassword:'',
            password:'',
            designation:'',
            tehsil:[],
            userID:'',
            loginType:'PEETHASEEN_ADHIKARI',
            userType:'आयुक्त',
            district:"Dehradun",
            email:'',
            loginTypeList:[]



        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.validator = new SimpleReactValidator({ autoForceUpdate: this });

    }

    componentDidMount(){
        this.getLoginType()
        this.getTehsil()
    }
getTehsil=async()=>{
    let res = await Service.location.getTehsil()
    if(typeof(res) == "object"){
        if(res && res.status && res.status == true){
            this.setState({tehsil:res.data})
        }
        else{
            alert("API status fail")
        }
    }
    else{
        alert("API Fails")
    }
}
    getLoginType=async()=>{
        const res = await Service.location.getLoginType()
        if(typeof(res) == "object"){
            if(res.status && res.status == true){
                await this.setState({loginTypeList : res.data.loginAs})
                const filterLoginType = this.state.loginTypeList.filter(itm=>{
                    return itm != "MASTER"
                })
                this.setState({loginTypeList:filterLoginType})
                }
            else{
                alert(res.message)
            }
        }
        else{
            alert("API Fails")
        }
    }
    handleChange(e) {
        this.setState({
            [e.target.name] : e.target.value
        });
        console.log(this.state,"Updated")

    }
    reset=()=>{
        this.setState({

            username:'',
                name:'',
                password:'',
                designation:'',
                tehsil:'',
                userID:'',
                loginType:'PEETHASEEN_ADHIKARI',
                userType:'आयुक्त',
                district:'',
                contactNumber:'',
                emailId:'',
        })
    }



    handleSubmit(event) {
        event.preventDefault();
        if (this.validator.allValid()) {
        const data = localStorage.getItem('token')
        console.log(data)
        const path = this.props
        const body ={
            // "old_password":"123456",
            // "new_password":"123456",
            "password":this.state.password,
            "email":this.state.emailId,
            "name":this.state.name,
            "loginType":this.state.loginType,
            "loginAs":this.state.userType,
            "username":this.state.name ,
            "districtId": [this.state.district ],
            "tehsilId": [this.state.tehsilId] ,
            "tehsilId": null ,
            "designation":this.state.designation,
            "registrationNo":this.state.userID   ,

        }
        axios.post('http://139.59.47.2/api/v1/admin/user', body)
            .then(response =>
                {
                  console.log(response)
                  debugger
                    if(response.data.status==true) {
                        alert(response.data.message)
                        // ToastsStore.success('User Created');
                        console.log(this.state.loginType)

                    }
                    else{
                        alert(response.data.message)
                        // ToastsStore.error('User Failed');

                    }

                }
            )
            }
            else {
                this.validator.showMessages();
                this.forceUpdate();
             }
    }



    render() {
        return (
            <div>
                <div className="TopIconShadow">
                    <div className="logo--heading">
                        <img src={logo} style={{width:80,height:80}}/>
                        <h2>REVENUE COURT OF DEHRADUN</h2>
                    </div>
                    <div className={"setting_menu_main " + (this.state.settingMenuVisible ? "open" : "")}>
                        <span className="close_menu" onClick={()=>{
                            this.setState({
                                settingMenuVisible:false
                            })
                        }}>
                            <i className="mdi mdi-close"></i>
                        </span>
                        <MasterLoginSetting/>
                    </div>
                    <div className="Master-Login-tabs">
                        <Link to="/Add-Users"> <button className="active"><img src={addUser} style={{width:60,height:60}}/><br/> Add User</button></Link>
                        <Link to="/Add-Act"> <button className="active"><img src={AddAct} style={{width:60,height:60}}/><br/> Add Act</button></Link>
                        <Link to="/Add-Court"><button className="active"><img src={AddCourt} style={{width:60,height:60}}/><br/>  Add Court</button></Link>
                        <Link to="/master-login"><button className="active"><img src={updateUser} style={{width:60,height:60}}/><br/> Modify User</button></Link>
                        <button className="active"  onClick={()=>{this.setState({settingMenuVisible:true})}}><img src={setting} style={{width:60,height:60}}/><br/> Setting</button>
                    </div>
                </div>

                <div className="CaseRegistrationBox" style={{height:"450px"}} >

                    <div style={{paddingTop: "30px"}}>
                        <div className="container" style={{background: "#FFF", border: "2px solid lightGray"}}>
                            <div>
                                <div style={{
                                    background: "#B7DCFD",
                                    border: "1px solid black",
                                    marginTop: "-15px",
                                    width: "135px",
                                    textAlign: "center"
                                }}><b>User Details </b></div>
                                <div style={{paddingTop: "20px", paddingBottom: "20px"}}></div>

                                <div className="row">
                                    <div className='col-sm-6'>
                                        <table className="newCaseRegistrationTable" style={{border: "none"}}>
                                            <tr style={{border: "none"}}>
                                                <td style={{border: "none"}}><p>Name</p></td>
                                                <td style={{border: "none"}}>
                                                    <input type={'text'} name={'name'} value={this.state.name} onChange={this.handleChange}/>
                                                    <span className='text-danger'>
                                                            {this.validator.message('name', this.state.name, 'required' )}
                                                    </span>
                                                    </td>
                                           
                                            </tr>
                                            {/* <tr style={{border: "none"}}>
                                                <td style={{border: "none"}}><p>userType</p></td>
                                                <td style={{border: "none"}}><input type={'text'} value={this.state.username} name={'username'} onChange={this.handleChange}/>
                                                <span className='text-danger'>
                                                            {this.validator.message('username', this.state.username, 'required' )}
                                                    </span>
                                                </td>
                                            </tr> */}

                                            <tr>
                                                <td style={{border: "none"}}><p>Contact Number</p></td>
                                                <td style={{border: "none"}}><input name={'contactNumber'} value={this.state.contactNumber} type={'text'} onChange={this.handleChange}/>
                                                <span className='text-danger'>
                                                            {this.validator.message('contactNumber', this.state.contactNumber, 'required|alpha_num' )}
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style={{border: "none"}}><p>EmailID</p></td>
                                                <td style={{border: "none"}}><input name={'emailId'}  value={this.state.emailId} type={'text'} onChange={this.handleChange}/>
                                                <span className='text-danger'>
                                                            {this.validator.message('emailId', this.state.emailId, 'required|email' )}
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style={{border: "none"}}><p>Password</p></td>
                                                <td style={{border: "none"}}><input  name={'password'} value={this.state.password} type={'Password'} onChange={this.handleChange}/>
                                                <span className='text-danger'>
                                                            {this.validator.message('password', this.state.password, 'required' )}
                                                    </span>
                                                </td>
                                            </tr>
                                    
                                            <tr>
                                                <td style={{border: "none"}}><p>Tehsil</p></td>
                                                <td style={{border: "none"}}>
                                                <select name={'tehsilId'} className="" value={this.state.tehsil} style={{width:"180px"}}  onChange={this.handleChange} >
                                                    <option value="default">Select the Tehsil</option>
                                                    {this.state.tehsil && this.state.tehsil.map(itm=>{
                                                        return(

                                                            <option value={itm.id}>{itm.name}</option>
                                                        )
                                                    })}
                                                    </select>
                                                {/* <input  name={'tehsil'} value={this.state.tehsil} type={'text'} onChange={this.handleChange}/> */}
                                                
                                                <span className='text-danger'>
                                                            {this.validator.message('tehsil', this.state.tehsil, 'required' )}
                                                    </span>
                                                </td>
                                            </tr>
                                            {/* <tr>
                                                <td style={{border: "none"}}><p>loginType</p></td>
                                                <td style={{border: "none"}}>
                                                <select name={'loginType'} className="" value={this.state.loginType} style={{width:"180px"}}  onChange={this.handleChange} >
                                                    <option value="default">Select the Type</option>
                                                    {this.state.tehsil && this.state.loginTypeList.map(itm=>{
                                                        return(

                                                            <option value={itm}>{itm}</option>
                                                        )
                                                    })}
                                                    </select>
                                                
                                                <span className='text-danger'>
                                                            {this.validator.message('tehsil', this.state.tehsil, 'required' )}
                                                    </span>
                                                </td>
                                            </tr> */}
                                            
                                        </table>
                                    </div>
                                    <div className='col-sm-6'>
                                        <div className="newCaseRegistrationTable">
                                            <table style={{border: "none"}}>
                                                <tr>
                                                <td style={{border: "none"}}><p> Designation</p></td>
                                                <td style={{border: "none"}}>
                                                <select name={'designation'} className="" value={this.state.designation} style={{width:"180px"}}  onChange={this.handleChange} >
                                                    <option value="default">Select the Type</option>
                                                    {this.state.loginTypeList && this.state.loginTypeList.map(itm=>{
                                                        return(

                                                            <option value={itm}>{itm}</option>
                                                        )
                                                    })}
                                                    </select>
                                                
                                                {/* <input   name={'designation'} value={this.state.designation}  type={'text'} onChange={this.handleChange}/> */}
                                                
                                                <span className='text-danger'>
                                                        {this.validator.message('designation', this.state.designation, 'required' )}
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style={{border: "none"}}><p>Headquarters</p></td>
                                                <td style={{border: "none"}}><input   name={'headquarters'} value={this.state.headquarters}  type={'text'} onChange={this.handleChange}/>
                                                <span className='text-danger'>
                                                            {this.validator.message('headquarters', this.state.headquarters, 'required' )}
                                                    </span>
                                                </td>
                                            </tr>

                                                <tr style={{border: "none"}}>
                                                    <td style={{border: "none"}}><p>User ID</p></td>
                                                    <td style={{border: "none"}}> <input  name={'userID'} value={this.state.userID} type={'text'} onChange={this.handleChange}/>
                                                    <span className='text-danger'>
                                                        {this.validator.message('userID', this.state.userID, 'required' )}
                                                    </span>
                                                     </td>
                                                </tr>

                                                <tr>
                                                    <td style={{border: "none"}}><p>User Type</p></td>
                                                    <td style={{border: "none"}}><select name={'loginType'} style={{width:"180px"}} value={this.state.loginType} onChange={this.handleChange} className="">
                                                        <option >Select your User type</option>
                                                        {this.state.loginTypeList && this.state.loginTypeList.map(itm=>{
                                                            return(
                                                                <>
                                                                <option value={itm} >{itm}</option>
                                                                </>
                                                            )
                                                        })}
                                                        {/* <option >Pashkeer</option>
                                                        <option >PEETHASEEN_ADHIKARI</option> */}
                                                    </select>
                                                    <span className='text-danger'>
                                                            {this.validator.message('loginType', this.state.loginType, 'required' )}
                                                    </span>
                                                    </td>
                                                </tr>
                                                {/* <tr>
                                                    <td style={{border: "none"}}><p>User As</p></td>
                                                    <td style={{border: "none"}}><select name={'userAs'} onChange={this.handleChange}className="DropDownForm" style={{border:"1px solid black]"}}>
                                                        <option >Select your User As</option>
                                                        <option >आयुक्त</option>
                                                    </select>
                                                    </td>
                                                </tr> */}

                                                <tr>
                                                    <td style={{border: "none"}}><p>District</p></td>
                                                    <td style={{border: "none"}}><input  name={'district'} value="Dehradun" disabled onChange={this.handleChange} type={'text'}/>
                                                    <span className='text-danger'>
                                                            {this.validator.message('district', this.state.district, 'required' )}
                                                    </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="container" style={{ paddingLeft:"340px",paddingTop:"30px",paddingBottom:"30px"}}>
                        <div className="caseStatusButton" style={{paddingBottom:"50px"}}>
                            <div className="container"><button type="button" className="btn btn-secondary btn-lg" style={{background:"transparent",borderColor:"blue",borderWidth:"2px",color:"blue", width:"230px"}} onClick={()=>{this.reset()}}>Reset</button>
                                <button type="button" className="btn btn-primary btn-lg" style={{width:"230px",marginLeft:"30px"}} onClick={this.handleSubmit} >Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                <ToastsContainer store={ToastsStore}/>

                <Footer/>
            </div>
        );
    }
}

export default addUsers;