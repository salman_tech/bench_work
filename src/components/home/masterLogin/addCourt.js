import React, {Component} from 'react';
import logo from "../../assets/logo.jpg";
import addUser from "../../assets/addUser.png";
import updateUser from "../../assets/updateUser.png";
import setting from "../../assets/setting.png";
import MasterLoginSetting from "../../common/MasterLoginSetting";
import Footer from "../../common/footer";
import {Link} from "react-router-dom";
import AddAct from "../../assets/AddAct.png";
import AddCourt from "../../assets/AddCourt.png";
import Service from '../../service';

class addCourt extends Component {


    constructor(props) {

        super(props);
        this.state = {
            tabContent: 0,
            settingMenuVisible:false,
            courtType:'Revenue Court'
    };
    }
    componentDidMount(){
        // this.getTehsil()
    }
    handleChange=(e)=>{
        this.setState({[e.target.name]:e.target.value})
        console.log(this.state)
    }
    reset=()=>{
        this.setState({
            court:'',
            courtType:'',
            tehsil:''
        })
    }
    onSubmit=async ()=>{
        const body={
            "name":this.state.court ?this.state.court:"null" ,
            "distrcit": "Dehradun",
            "courtType":this.state.courtType ? this.state.courtType:'',
            // "tehsil":this.state.tehsil ? this.state.tehsil:''
            "headquarters":this.state.headquarters
        }
        const res = await Service.location.addCourt(body)
        if(typeof(res) == 'object'){
            if(res.status & res.status == true){
                alert(res.message)
            }
            else{
                alert(res.message)
            }
        }
        else{
            alert("API Failed")
        }
        // console.log(r)

    }

    render() {
        return (
            <div>
                <div className="TopIconShadow">
                    <div className="logo--heading">
                        <img src={logo} style={{width:80,height:80}}/>
                        <h2>REVENUE COURT OF DEHRADUN</h2>
                    </div>
                    <div className={"setting_menu_main " + (this.state.settingMenuVisible ? "open" : "")}>
                        <span className="close_menu" onClick={()=>{
                            this.setState({
                                settingMenuVisible:false
                            })
                        }}>
                            <i className="mdi mdi-close"></i>
                        </span>
                        <MasterLoginSetting/>
                    </div>
                    <div className="Master-Login-tabs">
                        <Link to="/Add-Users"> <button className="active"><img src={addUser} style={{width:60,height:60}}/><br/> Add User</button></Link>
                        <Link to="/Add-Act"> <button className="active"><img src={AddAct} style={{width:60,height:60}}/><br/> Add Act</button></Link>
                        <Link to="/Add-Court"><button className="active"><img src={AddCourt} style={{width:60,height:60}}/><br/>  Add Court</button></Link>
                        <Link to="/master-login"><button className="active"><img src={updateUser} style={{width:60,height:60}}/><br/> Modify User</button></Link>
                        <button className="active"  onClick={()=>{this.setState({settingMenuVisible:true})}}><img src={setting} style={{width:60,height:60}}/><br/> Setting</button>
                    </div>
                </div>

                <div className="CaseRegistrationBox" >

                    <div style={{paddingTop: "30px"}}>
                        <div className="container" style={{background: "#FFF", border: "2px solid lightGray"}}>
                            <div>
                                <div style={{
                                    background: "#B7DCFD",
                                    border: "1px solid black",
                                    marginTop: "-15px",
                                    width: "135px",
                                    textAlign: "center"
                                }}><b> Court Details </b></div>
                                <div style={{paddingTop: "20px", paddingBottom: "20px"}}></div>

                                <div className="row">
                                    <div className='col-sm-6'>
                                        <table className="newCaseRegistrationTable" style={{border: "none"}}>
                                            <tr style={{border: "none"}}>
                                                <td style={{border: "none"}}><p>Court</p></td>
                                                <td style={{border: "none"}}><input type='text' value={this.state.court} name="court" onChange={(e)=>{this.handleChange(e)}}/></td>
                                            </tr>

                                            <tr>
                                                <td style={{border: "none"}}><p>District</p></td>
                                                <td style={{border: "none"}}><input type='Text' name="district" value="Dehradun" disbaled onChange={(e)=>{this.handleChange(e)}}/></td>
                                            </tr>
                                            

                                        </table>
                                    </div>
                                    <div className='col-sm-6'>
                                        <div className="newCaseRegistrationTable">
                                            <table style={{border: "none"}}>
                                            <tr>
                                                <td style={{border: "none"}}><p>Headquarters</p></td>
                                                <td style={{border: "none"}}><input type='Text' name="headquarters"  onChange={(e)=>{this.handleChange(e)}}/></td>
                                            </tr>
                                                <tr>
                                                    <td style={{border: "none"}}><p>Court Type</p></td>
                                                    <td style={{border: "none"}}>
                                                        <select className="DropDownForm" name="courtType" value={this.state.courtType} style={{border:"1px solid black]"}} onChange={(e)=>{this.handleChange(e)}}>
                                                        <option  value="difault">Select your Court type</option>
                                                        <option  value="Revenue Court">Revenue Court</option>
                                                        <option  value="Civil_Court">Civil court</option>
                                                        <option  value="Criminal_Court">Criminal Court</option>
                                                        {/* <option  value="ADM(F/R)">ADM (F/R)</option>
                                                        <option  value="ADM(F/R)">ADM (F/R)</option>
                                                        <option  value="ADM(F/R)">ADM (F/R)</option>
                                                        <option  value="ADM(F/R)">ADM (F/R)</option>
                                                        <option  value="SDM">SDM</option>
                                                        <option  value="Tesildar">Tesildar</option>
                                                        <option  value="NayabTesildar">Nayab Tesildar</option> */}
                                                    </select>
                                                    </td>
                                                </tr>
                                        
                                                {/* <tr>
                                                    <td style={{border: "none"}}><p>Tehsil</p></td>
                                                    <td style={{border: "none"}}>
                                                    <select className="DropDownForm" name="tehsil" value={this.state.tehsil} style={{border:"1px solid black]"}} onChange={(e)=>{this.handleChange(e)}}>
                                                        <option  value="default">Select Tehsil type</option>
                                                        <option  value="Chakrata">Chakrata</option>
                                                        <option  value="VikasNamger">VikasNamger</option>
                                                        <option  value="Sadar">Sadar</option>
                                                        <option  value="Tiyoni">Tiyoni</option>
                                                        <option  value="Rishikash">Rishikash</option>
                                                        <option  value="Kalsa">Kalsa</option>
                                                        <option  value="Dojwals">Dojwals</option>
                                                        </select>
                                                    </td>
                                                    <td style={{border: "none"}}>
                                                        <input type='number' name="tehsil" onChange={(e)=>{this.handleChange(e)}}/>
                                                    </td>
                                                </tr> */}
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="container" style={{ paddingLeft:"340px",paddingTop:"30px",paddingBottom:"30px"}}>
                        <div className="caseStatusButton">
                            <div className="container"><button type="button" className="btn btn-secondary btn-lg" style={{background:"transparent",borderColor:"blue",borderWidth:"2px",color:"blue", width:"230px"}} onClick={()=>{this.reset()}}>Reset</button>
                                <button type="button" className="btn btn-primary btn-lg" style={{width:"230px",marginLeft:"30px"}} onClick={()=>{this.onSubmit()}}>Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer/>

            </div>
        );
    }
}

export default addCourt;