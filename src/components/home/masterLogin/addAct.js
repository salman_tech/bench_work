import React, {Component} from 'react';
import logo from "../../assets/logo.jpg";
import addUser from "../../assets/addUser.png";
import updateUser from "../../assets/updateUser.png";
import setting from "../../assets/setting.png";
import MasterLoginSetting from "../../common/MasterLoginSetting";
import Footer from "../../common/footer";
import {Link} from "react-router-dom";
import AddAct from "../../assets/AddAct.png";
import AddCourt from "../../assets/AddCourt.png";
import SimpleReactValidator from 'simple-react-validator';
import './style.css'
import Service from '../../service'

class addAct extends Component {


    constructor(props) {

        super(props);
        this.state = {
            tabContent: 0,
            settingMenuVisible:false,
            isActive:false,
            advanced:false,
            section: [""],
            getDharas:[],
            forms:[]
        };
        this.validator = new SimpleReactValidator({ autoForceUpdate: this });
    }
    addMore=()=>{
        const arrayPush = this.state.section.concat('')
        this.setState({section:arrayPush})
        // this.setState((prevState, newEmployer) => ({
        //     section: [...prevState,prevState.concat('')]
        // })) 
    }
    
    handleChange=(e)=>{
        if(e.target.name == "advanced")this.setState({isActive:e.target.checked,advanced:e.target.checked})
        else if (e.target.name=="form1")this.setState({[e.target.name]:e.target.checked})
        else if (e.target.name=="form2")this.setState({[e.target.name]:e.target.checked})
        else if (e.target.name=="form3")this.setState({[e.target.name]:e.target.checked})
        else if (e.target.name=="form4")this.setState({[e.target.name]:e.target.checked})
        else if (e.target.name=="form5")this.setState({[e.target.name]:e.target.checked})
        else this.setState({[e.target.name]:e.target.value}) 
    }
    handleSectionChange=(e,i)=>{
        const data = [...this.state.section]
        data[i]=e.target.value
        this.setState({section:data})
    }

    remove=(i)=>{
        const section = [...this.state.section]
        section.splice(i,1)
        this.setState({section})
        console.log("removeeeeee",this.state.section)
    }
reset=()=>{
    this.setState({
        tehsil:"",
        year:'',
        section:[""],
        actname:'',
        isActive:false,
        advanced:false,
        form1:false,
        form2:false,
        form3:false,
        form4:false,
        form5:false,
    
    })
}
handleChangePush= async(e)=>{
    if(e.target.checked == true){
        debugger
        let name = e.target.name 
        const newArray = [...this.state.forms]
        newArray.push(name)
        // const originalArray = Array.from(new Set(newArray))
       await this.setState({forms:newArray})
        console.log("form state",this.state.forms)
    }
    else{
        const name = e.target.name
        debugger
        const newState = [...this.state.forms]
        const idx = newState.indexOf(name)
        newState.splice(idx,1)
        await this.setState({forms:newState})
    }

}
componentDidMount(){
    this.getDharas()
}
getDharas=async()=>{
    debugger
let r = await Service.location.getDharas()
if(typeof(r) == "object"){
    if(r.status && r.status == true){
        this.setState({getDharas:r.data})
    }
    else{
        this.setState({getDharas:''})
    }
}else{
    this.setState({getDharas:''})
    alert("Dharas api failed")
}
}
submit= async()=>{
    // if(this.state.year.length < 5){

        if (this.validator.allValid()) {
            const body={
                "name":this.state.actname,
                "section":this.state.section,
                "code":"Demo",
                "year":this.state.year,
                // "description":"null",
                "advance":this.state.isActive,
                "forms":this.state.forms
            }
            const res = await Service.location.postAct(body)
            if(typeof(res) == "object"){
                if(res.status && res.status == true){
                    alert("Act Registration successfully")
                }
                else{
                    alert("Act Registration failed")
                }
            }
            else{
                alert("Api_Fial_Backend_Error")
            }
            console.log(res)
        }
        else {
            this.validator.showMessages();
            this.forceUpdate();
         }
    // }
    // else{
    //     alert("Enter the Valid year in 4 digit")
    // }


}
    render() {
        return (
            <div>
                <div className="TopIconShadow">
                    <div className="logo--heading">
                        <img src={logo} style={{width:80,height:80}}/>
                        <h2>REVENUE COURT OF DEHRADUN</h2>
                    </div>
                    <div className={"setting_menu_main " + (this.state.settingMenuVisible ? "open" : "")}>
                        <span className="close_menu" onClick={()=>{
                            this.setState({
                                settingMenuVisible:false
                            })
                        }}>
                            <i className="mdi mdi-close"></i>
                        </span>
                        <MasterLoginSetting/>
                    </div>
                    <div className="Master-Login-tabs">
                        <Link to="/Add-Users"> <button className="active"><img src={addUser} style={{width:60,height:60}}/><br/> Add User</button></Link>
                        <Link to="/Add-Act"> <button className="active"><img src={AddAct} style={{width:60,height:60}}/><br/> Add Act</button></Link>
                        <Link to="/Add-Court"><button className="active"><img src={AddCourt} style={{width:60,height:60}}/><br/>  Add Court</button></Link>
                        <Link to="/master-login"><button className="active"><img src={updateUser} style={{width:60,height:60}}/><br/> Modify User</button></Link>
                        <button className="active"  onClick={()=>{this.setState({settingMenuVisible:true})}}><img src={setting} style={{width:60,height:60}}/><br/> Setting</button>
                    </div>
                </div>

                <div className="CaseRegistrationBox" style={{height:"450px"}} >

                    <div style={{paddingTop: "30px"}}>
                        <div className="container" style={{background: "#FFF", border: "2px solid lightGray"}}>
                            <div>
                                <div style={{
                                    background: "#B7DCFD",
                                    border: "1px solid black",
                                    marginTop: "-15px",
                                    width: "135px",
                                    textAlign: "center"
                                }}><b> Act Details </b></div>
                                <div style={{paddingTop: "20px", paddingBottom: "20px"}}></div>
                                
                                <div className="row">
                                    
                                    <div className='col-sm-6'>
                                        <div className="newCaseRegistrationTable">
                                            <table style={{border: "none"}}>
                                                {/* <tr>
                                                    <td style={{border: "none"}}><p>Court Type</p></td>
                                                        <td style={{border: "none"}}><input name="courtType" disbaled value="Revenue Court" type={'Text'}/></td>
                                                </tr> */}
                                                <tr>
                                                    <td style={{border: "none"}}><p>Year</p></td>
                                                        <td style={{border: "none"}}>
                                                            <input name="year" value={this.state.year} type='date' onChange={(e)=>{this.handleChange(e)}}/>
                                                     <span className='text-danger'>
                                                            {this.validator.message('year', this.state.year, 'required' )}
                                                    </span>
                                                            </td>
                                                </tr>

                                                {/* <tr>
                                                    <td style={{border: "none"}}><p>Tehsil</p></td>
                                                    <td style={{border: "none"}}>
                                                    <input name="tehsil" type={'number'}/>
                                                    <select className="DropDownForm" name="tehsil" value={this.state.tehsil} style={{border:"1px solid black]"}} onChange={(e)=>{this.handleChange(e)}}>
                                                        <option  value="default">Select Tehsil type</option>
                                                        <option  value="Chakrata">Chakrata</option>
                                                        <option  value="VikasNamger">VikasNamger</option>
                                                        <option  value="Sadar">Sadar</option>
                                                        <option  value="Tiyoni">Tiyoni</option>
                                                        <option  value="Rishikash">Rishikash</option>
                                                        <option  value="Kalsa">Kalsa</option>
                                                        <option  value="Dojwals">Dojwals</option>
                                                        </select>

                                                       <span className='text-danger'> {this.validator.message('tehsil', this.state.tehsil, 'required' )}</span>
                                                    
                                                    </td>
                                                </tr> */}
                                                <tr style={{border: "none"}}>
                                                <td style={{border: "none"}}><p>Act Name</p></td>
                                                <td style={{border: "none"}}>
                                                    <input  name="actname" value={this.state.actname} onChange={(e)=>{this.handleChange(e)}} type='text'/>
                                                  <span className='text-danger'>
                                                    {this.validator.message('ActName', this.state.actname, 'required' )}
                                           </span>
                                                    </td>
                                           
                                            </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div className='col-sm-6'>
                                        <div className="newCaseRegistrationTable">
                                            <table style={{border: "none"}}>
                                                
                                                        {this.state.section && this.state.section.map((itm,idx)=>{
                                                            return(
                                                                
                                                <tr>
                                                    <td style={{border: "none"}}><p>Section</p></td>
                                                    
                                                    <td style={{border: "none"}}>
                                                        <select value={this.state.section[idx]} onChange={(e)=>{this.handleSectionChange(e,idx)}}>
                                                            <option value="select_default">Select the Default Value</option>
                                                            {this.state.getDharas && this.state.getDharas.map((ex,idx)=>{
                                                                return(

                                                                    <option value={ex._id}>{ex.name}</option>
                                                                )
                                                            }) 
                                                            }
                                                            </select>
                                            {/* <input  value={this.state.section[idx]} type='text' onChange={(e)=>{this.handleSectionChange(e,idx)}}/> */}
                                           <span className='text-danger'>
                                            {this.validator.message('section', this.state.section[idx], 'required' )}
                                           </span>
                                           <span  onClick={()=>{this.addMore()}} style={{marginLeft:"10px",cursor:"pointer"}}>Add</span>
                                           {this.state.section.length > 1 &&
                                        <span style={{cursor:"pointer"}} onClick={()=>{this.remove(idx)}}>/Remove</span>}
                                           </td>
                                           
                                                </tr>
                                                            )
                                                        })}
                                             
                                            </table>
                                        </div>
                                    </div>
                                    <div className='col-sm-12'>
                                        <table className="newCaseRegistrationTable" style={{border: "none"}}>
                                                <div className="col-md-6">
                                            <div className="row">
                                            <div className="col-md-5">
                                                        <p style={{float:"left"}}>Advanced Option</p>
                                                        </div>
                                            <div className="col-md-2">
                                                <input type="checkbox" name="advanced" style={{marginTop:"10px",float:"left"}} value={this.state.advanced} onClick={(e)=>{this.handleChange(e)}} />
                                            </div>
                                            
                                                    
                                                    </div>
                                                </div>
                                            

                                            {/* <tr>
                                                <td style={{border:"none"}}><h5>Advanced</h5></td>
                                            </tr> */}
                                        </table>
                                    </div>
                                   
                                </div>

                            </div>
                            {this.state.isActive &&
                            <>
                                <p style={{textAlign:"center",fontWeight:"bold"}}>Please Select Form Type</p>
                            <div className="row" style={{marginTop:"20px"}}>
                                <div className="col-md-3" style={{margin:"0px"}}>
                           
                                    <input type="checkbox"  name="LAND" value={this.state.LAND}  onClick={(e)=>{this.handleChangePush(e)}}/>
                                        <label style={{marginLeft:"10px"}}> Land Description</label>
                           
                         </div>
                         <div className="col-md-3">
                         <input type="checkbox"  name="STAMP" value={this.state.STAMP} onClick={(e)=>{this.handleChangePush(e)}}/>
                                     <label style={{marginLeft:"10px"}}>  Stamp Duty Description</label>
                             </div>
                             <div className="col-md-3">
                         <input type="checkbox"  name="ARMS" value={this.state.ARMS} onClick={(e)=>{this.handleChangePush(e)}}/>
                                     <label style={{marginLeft:"10px"}}>  Arms Description</label>
                             </div>
                             <div className="col-md-3">
                         <input type="checkbox"  name="DESCRIPTION" value={this.state.DESCRIPTION} onClick={(e)=>{this.handleChangePush(e)}}/>
                                     <label style={{marginLeft:"10px"}}> Description</label>
                             </div>
                             <div className="col-md-3">
                         <input type="checkbox"  name="COMPLAINANT" value={this.state.COMPLAINANT} onClick={(e)=>{this.handleChangePush(e)}}/>
                                     <label style={{marginLeft:"10px"}}> Type Of Complainant</label>
                             </div>
                             </div>
                        </>
                            }
                        </div>
                        
                    </div>
                    

                    <div className="container" style={{ paddingLeft:"340px",paddingTop:"30px",paddingBottom:"65px"}}>
                        <div className="caseStatusButton">
                            <div className="container"><button type="button" className="btn btn-secondary btn-lg" style={{background:"transparent",borderColor:"blue",borderWidth:"2px",color:"blue", width:"230px"}} onClick={()=>{this.reset(0)}}>Reset</button>
                                <button type="button" className="btn btn-primary btn-lg" style={{width:"230px",marginLeft:"30px"}} onClick={()=>{this.submit()}}>Submit</button>
                            </div>
                        </div>
                    </div>

                </div>
                <Footer/>

            </div>
        );
    }
}

export default addAct;