import React, {Component} from 'react';
import logo from "../../assets/logo.jpg";
import addUser from "../../assets/AllOrder.png";
import UploadType from "../../assets/UploadType.png";
// import updateUser from "../../assets/AllOrder.png";
// import setting from "../../assets/setting.png";
import MasterLoginSetting from "../../common/MasterLoginSetting";
import Footer from "../../common/footer";
import {Link} from "react-router-dom";
import AddAct from "../../assets/TypeOrder.png";
import AddCourt from "../../assets/AddCourt.png";
import SimpleReactValidator from 'simple-react-validator';
// import './style.css'
import Service from '../../service'

class AllCaseOrder extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data:[],
            tab1:true,
            files:null,
            type1:localStorage.getItem("type1"),
            type2:localStorage.getItem("type2")
        };
        this.validator = new SimpleReactValidator({ autoForceUpdate: this });
    }

async componentDidMount(){
let res = await Service.location.getAllCase()
    if(typeof(res) == "object"){
        if(res.status == true && res.status !==undefined){
            this.setState({data:res.data})
        }
        else{
            alert("Backend Error")
        }
    }
    else{
        alert("Api Fail")
    }
}
handleChange=(e)=>{
    this.setState({[e.target.name]:e.target.value})
}
saveImages=async(e)=>{
    await this.setState({files:e.target.files[0]})
    console.log("imagesss",this.state.files)
    // if(e.target.files.length > 0){
    //     var t = e.target.files;
    //     var image = [];
    //     for (let i = 0; i < t.length; i++) {
    //         image.push(t[i].name)
    //     }
    //     await this.setState({files:image})
    //     console.log("imagess array",this.state.files)
    // }
}

    tab=(value)=>{
    switch(value) {
    case "1":
      this.setState({tab1:true,tab2:false,tab3:false})
      break;
    case "2":
        this.setState({tab1:false,tab2:true,tab3:false})
      break;
    case "3":
        this.setState({tab1:false,tab2:false,tab3:true})
      break;
    default:
      alert("Error")
  
}
}
onSubmit=async()=>{
    // alert("hit")
    // const formData = new FormData()
    const body={
            "order": {
                    "files": this.state.tab2 ? []:this.state.files, 
                    "date": this.state.tab2 ? this.state.tab2Date:this.state.tab3Date,
                    "details": this.state.tab2 ? this.state.typeOrder: ""
                }
            }
    const formData = new FormData();
        formData.append('tab3Images',body);
        console.log("formdtatatata",formData)
        const res = await Service.location.addCourtOrder(this.state.tab2 ? this.state.tab2CaseId:this.state.tab3CaseId,formData)
            alert(res.message)     
}
handleChnage=(e)=>{
    this.setState({[e.target.name]:e.target.value})
    }
    onSearch=async()=>{
        if(this.state.searchItem !==undefined && this.state.searchItem !==''){

            const res = await Service.location.caseSearch(this.state.searchItem)
            if(typeof(res)=='object'){
                if(res.status && res.status == true){
                    this.setState({data:res.data})
                }
                else{
                    alert("Search Failed")
                }
            }
            else{
                alert("API Failed")
            }
        }
        else{
            alert("Enter the Search Item")
        }
    }
    render() {
        return (
            <div>
                <div className="TopIconShadow">
                       {this.state.type1 == "PESHKAR" && <Link to="/peshkaar-login"> <span style={{float:"left",paddingLeft:"20px",paddingTop:"50px",fontSize:"30px",cursor:"pointer"}}><img style={{width:"30px"}} src={require('../../assets/arrow.png')} /></span>
                       </Link>}
                       {this.state.type2 == "PEETHASEEN_ADHIKARI" && <Link to="/DM-login"> <span style={{float:"left",paddingLeft:"20px",paddingTop:"50px",fontSize:"30px",cursor:"pointer"}}><img style={{width:"30px"}} src={require('../../assets/arrow.png')} /></span>
                       </Link>}
                    <div className="logo--heading">
                        <img src={logo} style={{width:80,height:80}}/>
                        <h2>REVENUE COURT OF DEHRADUN</h2>
                    </div>
                    <div className={"setting_menu_main " + (this.state.settingMenuVisible ? "open" : "")}>
                        <span className="close_menu" onClick={()=>{
                            this.setState({
                                settingMenuVisible:false
                            })
                        }}>
                            <i className="mdi mdi-close"></i>
                        </span>
                        <MasterLoginSetting/>
                    </div>
                    <div className="Master-Login-tabs">
                         <button onClick={()=>this.tab("1")} style={{border:"none",background:"white"}}><img src={addUser} style={{width:60,height:60}} /><br/>All Orders</button>
                    {this.state.type1 == "PESHKAR" &&  
                    <>    
                         <button onClick={()=>this.tab("2")} style={{border:"none",background:"white"}}><img src={AddAct} style={{width:60,height:60}} /><br/>Type Order</button>
                         <button onClick={()=>this.tab("3")} style={{border:"none",background:"white"}}><img src={UploadType} style={{width:60,height:60}} /><br/>Upload Order</button>
                      </>
                        }
                    </div>
                </div>
                {this.state.tab1 && 
                <div className="PeshkarLoginBlueBox">
                    <div className="tablecontainer">
                    <div className="row">
                    <div className="col-md-4"><h4></h4>
                        </div>
                        <div className="col-md-4" style={{textAlign:"center"}}><h4>Cause List Report</h4>
                        </div>
                        {this.state.type2 == "PEETHASEEN_ADHIKARI" && 
                        <div className="col-md-4" style={{textAlign:"right"}}>
                        <div className="row">
                        <div className="col-md-9" style={{textAlign:"right",paddingRight:"0px"}} >
                        <input placeholder="Enter the Case No" name="searchItem" onChange={(e)=>this.handleChnage(e)} style={{height:"29px",width:"100%"}}/>
                        </div>
                        <div className="col-md-3" style={{textAlign:"center",padding:"0px"}}>
                        <button className="btn btn-primary" type="button" onClick={()=>this.onSearch()} style={{height:"30px",lineHeight:"0px"}}>Search</button>
                        </div>
                        </div>
                        </div>
                    }
                        
                        </div>
                        <div className="causeListTable">
                            <table>
                                <tr>
                                    <th>S.No</th>
                                    <th>Case Number</th>
                                    <th>Court</th>
                                    <th>Upload Date</th>
                                    <th>Upload By</th>
                                    <th>View Order</th>
                                    <th>Download Order</th>
                                </tr>
                                {this.state.data.length > 0 ? this.state.data.map((itm,key)=>{
                                    
                                    return(

                                <tr>
                                    <td>{key+1}</td>
                                    <td>{itm.caseId}</td>
                                    <td>{itm.caseDirection}</td>
                                    <td>null</td>
                                    <td>null</td>
                                    <td>View</td>
                                    <td>Download</td>
                                    {/* <td><span onClick={()=>this.moveToPage(itm._id)} style={{color:"#2780d4",cursor:"pointer"}}>View</span></td> */}
                                </tr>
                                    )
                                })
                                :
                                <tr>
                                <td colspan="10"><div style={{color:"red"}}>No result found.</div></td></tr> }
                            </table>
                                <div className="row">
                                    <div className="container">
                                        <div className="row">
                                        <div className="col-md-6"></div>
                                        <div className="col-md-6">
                                            <div className="row" style={{marginTop:"20px"}}>
                                                <div className="col-md-8"><button className="btn btn-primary" style={{float:"right"}}>Selecte Multiple</button></div>
                                                <div className="col-md-4"><button className="btn btn-success" style={{float:"left"}}>Download All</button></div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                        </div>

                    </div>
                </div>
                }
                {this.state.tab2 &&
                <>
                <div className="CaseRegistrationBox">
                <div className="container" style={{background: "#FFF", border: "2px solid lightGray"}}>
                                <div >
                                    
                                    <div style={{paddingTop: "20px", paddingBottom: "20px"}}>
                                        <div className="container">
                                        <div className="row">
                                            
                                            {/* <div className="col-sm-1"><p>Court</p>
                                            </div>
                                            <div className='col-sm-2'>
                                                <div>
                                                    <input type="date" id="date-picker-example" name={'RegistrationDate'} value={this.state.RegistrationDate} className="form-control datepicker" onChange={this.handleChange}/>
                                                    <span className='text-danger'>
                                                            {this.validator.message('vaadType', this.state.RegistrationDate, 'required' )}
                                                    </span>
                                                </div>
                                            </div> */}
                                            <div className="col-sm-2"><p>Case Number</p>
                                            </div>
                                            <div className='col-sm-2'>
                                                <input className="custom-select" name={'tab2CaseId'}  onChange={(e)=>this.handleChange(e)} />
                                                {/* <option selected>Choose Section</option>
                                                <option value="1">One</option>
                                                <option value="2">Two</option>
                                                <option value="3">Three</option> */}
                                            
                                            <span className='text-danger'>
                                                            {this.validator.message('section', this.state.section, 'required' )}
                                                    </span>
                                            </div>
                                            <div className="col-sm-1"><p>Date</p></div>
                                            <div className='col-sm-3'>
                                                <div>
                                                    <input type="date" id="date-picker-example" name={'tab2Date'} value={this.state.targetDate} className="form-control datepicker" onChange={this.handleChange}/>
                                                    <span className='text-danger'>
                                                            {this.validator.message('targetDate', this.state.targetDate, 'required' )}
                                                    </span>
                                                </div>
                                            </div>
                                            <div className="col-md-12" style={{marginTop:"10px"}}>
                                            <div className="row" style={{marginTop:"10px",marginBottom:"10px"}}>
                                                <div className="col-md-2">
                                                    <label style={{display:"block",textAlign:"left"}}>Type Order</label>
                                                    </div>
                                                <div className="col-md-9" style={{position:"relative"}}><input style={{width:"100%",height:"76px"}} name="typeOrder" onChange={(e)=>this.handleChange(e)} placeholder="Enter the Arguments Here"/>
                                                {/* <span style={{width:"50px",height:"75px",position:"absolute",right:"16px",background:"#c9d7ef",textAlign:"center",fontSize:"46px",color:"#fff",cursor:"pointer"}}><img src={require('../../assets/phone.png')} /></span> */}

                                                </div>
                                            </div>
                                            </div>
                                             </div>
                            <div className="row">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-5"></div>
                                <div className="col-md-6">
                                    <div className="row">
                                        <div className="col-md-6"></div>
                                    <div className="col-md-6">
                                        <div className="container">
                                        <div className="row">
                                            <div className="col-md-6">

                                        <button className="btn btn-primary" style={{background:"white",border:"2px solid blue",color:"blue",width:"100%"}}>Reset</button>
                                            </div>
                                            <div className="col-md-6" style={{float:"left"}}>

                                        <button className="btn btn-primary" style={{width:"100%"}}>submit</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </>}
                {this.state.tab3 &&
                <>
                <div className="CaseRegistrationBox">
                <div className="container" style={{background: "#F1F6FE"}}>
                    <div className="row">
                        <div className="col-md-4"></div>
                        <div className="col-md-4">
                        <div className="col-md-12">
                            {/* <div className="row" style={{marginTop:"10px"}}>
                                <div className="col-md-3"><p style={{paddingTop:"5px",background:"#F1F6FE"}}>Court</p></div>
                                <div className="col-md-8">
                                <select name={'courtName'} className="custom-select"style={{width:"100%"}} >
                                    <option selected>Prime Case</option>
                                </select>
                                </div>
                            </div> */}
                            <div className="row" style={{marginTop:"10px"}}>
                                <div className="col-md-3"><p style={{paddingTop:"5px",background:"#F1F6FE"}}>Case ID</p></div>
                                <div className="col-md-8">
                                <input placeholder="Enter the Case Number" name="tab3CaseId" onChange={(e)=>this.handleChange(e)} style={{width:"100%"}}/>
                                </div>
                            </div>
                            <div className="row" style={{marginTop:"10px"}}>
                                <div className="col-md-3"><p style={{paddingTop:"5px",background:"#F1F6FE"}}>Date</p></div>
                                <div className="col-md-8">
                                <input type="date" name="tab3Date" onChange={(e)=>this.handleChange(e)} style={{width:"100%"}}/>
                                </div>
                            </div>
                            <div className="row" style={{marginTop:"10px"}}>
                                <div className="col-md-3"><p style={{paddingTop:"5px",background:"#F1F6FE"}}>Upload Order</p></div>
                                <div className="col-md-8">
                                <input type="file" id="files" name="files" multiple onChange={(e)=>this.saveImages(e)} style={{width:"100%"}}/>
                                </div>
                            </div>
                            <div className="row" style={{marginTop:"-10px",marginBottom:"50px"}}>
                                <div className="col-md-3"><p style={{paddingTop:"5px",background:"#F1F6FE"}}></p></div>
                                <div className="col-md-8">
                                <div className="row" style={{marginBottom:"50px"}}>
                                    <div className="col-md-6"><button className="btn btn-primary" style={{width:"100%"}}>Reset</button></div>
                                    <div className="col-md-6"><button className="btn btn-primary" type="button" onClick={()=>this.onSubmit()}>Submit</button></div>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                    </div>
                </>}

                                        
                <Footer/>

            </div>
        );
    }
}

export default AllCaseOrder;