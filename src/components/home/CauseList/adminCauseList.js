import React, {Component} from 'react';
import logo from "../../assets/logo.jpg";
// import { Link } from "react-router-dom";
import DownArrow from '../../assets/DownArrow.png';
import Dashboard from "../../assets/Dashboard.png";
import CaseRegistration from "../../assets/CaseRegistration.png";
import DailyCauseList from "../../assets/DailyCauseList.png";
import CourtOrder from "../../assets/CourtOrder.png";
import DisputedLandInfo from "../../assets/DisputedLandInfo.png";
import Setting from "../../assets/setting.png";
// import Calendar from "../common/calendar";
// import Navbar from "../../common/navbar"
import {Link} from "react-router-dom";
import MasterLoginSetting from "../../common/MasterLoginSetting";
import Footer from "../../common/footer";
import Service from '../../service'
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';


class causeList extends Component {
    constructor(props) {

        super(props);
        this.state = {
            tabContent: 0,
            menuOpenClass: false,
            data:[],
            isActive:true
        };
    }


onHandleChange=(e)=>{
    this.setState({[e.target.name]:e.target.value})
}
onSubmit=async()=>{
const res = await Service.location.getClauseCase(this.state.clauseDate)
if(typeof(res)== "object"){
    if(res.status == true && res.status !==undefined){
        this.setState({isActive:false,data:res.data})
    }
}

console.log("clauseDateeeeee",res)
}
moveToPage=(id)=>{
    const value = id
    localStorage.setItem('page_id',value)
    this.props.history.push('/view-case')
        }
    render() {
        return (
            <div>
                 <div style={{}}>
                    <div className="logo--heading">
                        <img src={logo} style={{width:80,height:80}}/>
                        <h2>REVENUE COURT OF DEHRADUN</h2>
                    </div>

                    <div className={"setting_menu_main " + (this.state.settingMenuVisible ? "open" : "")}>
                        <span className="close_menu" onClick={()=>{
                            this.setState({
                                settingMenuVisible:false
                            })
                        }}>
                            <i className="mdi mdi-close"></i>
                        </span>
                        <MasterLoginSetting/>
                    </div>

                    <div className="PeshkarLoginTabs" >
                        <div className="container">
                            <ul>
                                <li><Link to='/peshkaar-login' ><button className="active"><img src={Dashboard} style={{width:60,height:60,marginTop:"-15px"}}/><br/> Dashboard </button> </Link> </li>

                                <li> <UncontrolledDropdown>
                                    <DropdownToggle  style={{background:"white",color: "black",border:"none"}}>
                                        <img src={CaseRegistration} style={{width:60,height:60}}/><br/> Case Registration <br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem> <Link to='/new-case-registration'>New Case Registration</Link></DropdownItem>
                                        <DropdownItem divider />
                                        <DropdownItem><Link to to='/view-all-case'>View Case</Link></DropdownItem>
                                        <DropdownItem divider />
                                        {/* <DropdownItem>Delete Case</DropdownItem> */}
                                        <DropdownItem divider />
                                    
                                        <DropdownItem ><Link to='/view-case-status'>Update Case status</Link>
                                        <DropdownItem divider />
                                        </ DropdownItem>
                                        <DropdownItem ><Link >Upload Order Sheet</Link>
                                        <DropdownItem divider />
                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown> </li>

                                <li>
                                    <UncontrolledDropdown>
                                        <DropdownToggle style={{background:"white",color: "black",border:"none"}} >
                                            <img src={DailyCauseList} style={{width:60,height:60}}/><br/>  Daily Cause List <br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem><Link to="/admin-cause-list"> View Daily Cause List </Link></DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Change Prioritiy</DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Next Hearing Date</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                </li>



                                <li>
                                    <UncontrolledDropdown>
                                        <DropdownToggle style={{background:"white",color: "black",border:"none"}} >
                                            <Link to="/all-case-order"><img src={CourtOrder} style={{width:60,height:60}}/><br/>  Court Orders</Link> <br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                         {/* <DropdownMenu>
                                            <DropdownItem>  Type Order </DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Upload Order</DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Upload Order: Disposed </DropdownItem>
                                           <DropdownItem divider />
                                           <DropdownItem>Application Letter: Disposed</DropdownItem> 
                                     </DropdownMenu> */}
                                    </UncontrolledDropdown>
                                </li>


                                <li>
                                    <UncontrolledDropdown>
                                        <DropdownToggle  style={{background:"white",color: "black",border:"none"}}>
                                            <img src={DisputedLandInfo} style={{width:60,height:60}}/><br/>Disputed Land Info<br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem> <Link to={{pathname:'/disputed-land-information',state:{type:"GOLDEN_FOREST_LAND"}}}>Golden Forest Land</Link></DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem> <Link to={{pathname:'/disputed-land-information',state:{type:"GRAM_SABHA_LAND"}}}>Gram Sabha Land</Link></DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem> <Link to={{pathname:'/disputed-land-information',state:{type:"LAND_PROHIBITED_FROM_SALE"}}}>Land Prohibited From Sale</Link></DropdownItem>

                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                </li>
                                <li><button className="active" onClick={()=>{this.setState({settingMenuVisible:true})}}><img src={Setting} style={{width:60,height:60,marginTop:"-15px"}}/><br/> Setting</button></li>
                            </ul>
                        </div>
                    </div>
                </div>
                        {this.state.isActive ? 
                        <>
                <div className="blueBox1">

                        <form>
                            <div className="causeListTitle">
                                <div className="container">
                                    <h4>Cause List Report</h4>
                                </div>
                            </div>

                            <div className="box_wrapper">
                                <div className="form-group row">
                                <label htmlFor="inputEmail3" className="col-sm-4 col-form-label">Cause List</label>
                                <div className="col-sm-8">
                                    <div className="md-form">
                                        <input type="date" name="clauseDate" onChange={(e)=>{this.onHandleChange(e)}} id="date-picker-example"
                                               className="form-control datepicker"/>
                                    </div>
                                    <div className="caseStatusButton">
                                        {/* <Link to="/cause-list-submit"> */}
                                            <button type="button" onClick={()=>this.onSubmit()} className="btn btn-primary btn-lg">Submit</button>
                                            {/* </Link> */}
                                    </div>
                                </div>
                            </div>
                            </div>


                        </form>
                </div>
                        </>
                        :
                        <>
                        <div className="blueBox1">
                    <div className="tablecontainer">
                        <center> <div><h4> Cause List Report</h4> </div></center>

                        <div className="causeListTable">
                            <table>
                                <tr>
                                    <th>Serial No.</th>
                                    <th>Bench</th>
                                    <th>Registration Date </th>
                                    <th>View Cause List</th>
                                </tr>
                                {this.state.data.map((itm,key)=>{
                                    return(

                                <tr>
                                    <td>{key+1}</td>
                                    <td>{itm.complainant.length > 0 ? itm.complainant[0].name:"No Complainant Name" }</td>
                                    <td>{itm.registrationDate}</td>
                                    <td><span onClick={()=>this.moveToPage(itm._id)}>View</span></td>
                                </tr>
                                    )
                                })}
                            </table>
                        </div>

                    </div>
                </div>
                        </>
                            
                        }
                <Footer/>


            </div>
        );
    }
}

export default causeList;