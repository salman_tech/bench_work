import React, {Component} from 'react';
import Navbar from "../../common/navbar"
import {Link} from "react-router-dom";
import Footer from "../../common/footer";
import Service from '../../service'


class causeList extends Component {


    constructor(props) {

        super(props);
        this.state = {
            tabContent: 0,
            menuOpenClass: false,
            isActive:true,
            data:[],
            select:"daily"

        };
    }
    selectRadio=(e)=>{
        this.setState({[e.target.name]:e.target.value})
    }
    onSubmit=async()=>{
        if(this.state.select === "daily"){

            const res = await Service.location.getClauseCase(this.state.clauseDate1)
            if(typeof(res)== "object"){
                if(res.status == true && res.status !==undefined){
                    this.setState({isActive:false,data:res.data})
                }
                else{
                    alert(res.message)
                }
            }
            else{
             alert("API Fails")   
            }
        }
        else if(this.state.select === "weekly"){
            const body ={
                "date1": this.state.clauseDate1,
                "date2": this.state.clauseDate2
            }
            const res = await Service.location.getClauseCaseWeekly(body)
            if(typeof(res)== "object"){
                if(res.status == true && res.status !==undefined){
                    this.setState({isActive:false,data:res.data})
                }
                else{
                    alert(res.message)
                }
            }
            else{
             alert("API Fails")   
            }
        }
    }
        
    render() {
        console.log("staeeeeee",this.state)
        return (
            <div>
                <Navbar />
                <div className="blueBox1">
                    {this.state.isActive ?
                        <form>
                            <div className="causeListTitle">
                                <div className="container">
                                    <h4>Cause List Report</h4>
                                </div>
                            </div>

                            <div className="box_wrapper">
                                <div className="form-group row">
                                    <div className="col-md-3">
                                <label htmlFor="inputEmail3" style={{marginTop:"35px",paddingLeft:"65px"}} className="col-sm-4 col-form-label">To</label>
                                {this.state.select === "weekly" &&
                            
                                <label htmlFor="inputEmail3" style={{marginTop:"10px",paddingLeft:"65px"}} className="col-sm-4 col-form-label">From</label>
                            }
                                </div>
                                <div className="col-sm-8">
                                    <div className="col-md-12" style={{marginBottom:"10px"}}>
                                    <div className="row">
                                        <div className="col-md-6">
                                        {/* <label for="male">Daily</label><br/> */}
                                    <input type="radio" id="daily" name="select" value="daily"
                                    checked={this.state.select === "daily"}
                                    onChange={(e)=>this.selectRadio(e)}
                                    />Daily
                                        </div>
                                        <div className="col-md-6">
                                        {/* <label for="male">Weekly</label><br/> */}
                                    <input type="radio" id="weekly" name="select" value="weekly"
                                    checked={this.state.select === "weekly"}
                                    onChange={(e)=>this.selectRadio(e)}
                                    />Weekly
                                    </div>
                                    </div>
                                    </div>
                                    <div className="md-form">
                                        <div className="col-md-12">
                                        <input type="date" id="date-picker-example"
                                        name="clauseDate1"
                                        onChange={(e)=>this.selectRadio(e)}
                                               className="form-control datepicker"/>
                                    </div>
                                    </div>
                                    {this.state.select === "weekly" &&
                                    <>
                                    <div className="md-form">
                                        <div className="col-md-12" style={{marginTop:"10px"}}>
                                        <input type="date" id="date-picker-example1"
                                        name="clauseDate2"
                                        onChange={(e)=>this.selectRadio(e)}
                                               className="form-control datepicker"/>
                                    </div>
                                    </div>
                                    </>
                                }
                                    
                                    <div className="caseStatusButton">
                                        <div className="col-md-12" style={{marginBottom:"10px",marginTop:"10px"}}>
                                        {/* <Link to="/cause-list-submit"> */}
                                            <button type="button" onClick={()=>this.onSubmit()} className="btn btn-primary btn-lg">Submit</button>
                                            {/* </Link> */}
                                    </div>
                                    </div>
                                </div>
                            </div>
                            </div>


                        </form>:
                         <>
                         <div className="blueBox1">
                     <div className="tablecontainer">
                    <center> <div><h4> Cause List Report</h4> {this.state.isActive == false && <span onClick={()=>this.setState({isActive:true})}>Back</span>} </div></center>
 
                         <div className="causeListTable">
                             <table>
                                 <tr>
                                     <th>Serial No.</th>
                                     <th>Bench</th>
                                     <th>Registration Date </th>
                                     <th>View Cause List</th>
                                 </tr>
                                 {this.state.data && this.state.data.length > 0 ? this.state.data.map((itm,key)=>{
                                     return(
 
                                 <tr>
                                     <td>{key+1}</td>
                                     <td>{itm.complainant.length > 0 ? itm.complainant[0].name:"No Complainant Name" }</td>
                                     <td>{itm.registrationDate}</td>
                                     <td>
                                    <span >View</span>
                                    </td>
                                 </tr>
                                     )
                                 }):
                                 <tr><td colspan="4"><div style={{color:"red"}}>No result found.</div></td></tr> }
                                
                             </table>
                         </div>
 
                     </div>
                 </div>
                         </>
                        }
                </div>
                <Footer/>


            </div>
        );
    }
}

export default causeList;