import React, {Component} from 'react';
import Navbar from "../../common/navbar"
import Footer from "../../common/footer";

class causeListSubmit extends Component {


    constructor(props) {

        super(props);
        this.state = {
            tabContent: 0,
            menuOpenClass: false
        };
    }

    render() {
        return (
            <div>
                <Navbar />
                <div className="blueBox1">
                    <div className="tablecontainer">
                        <center> <div> <h4> Cause List Report</h4> </div></center>

                        <div className="causeListTable">
                            <table>
                                <tr>
                                    <th>Serial No.</th>
                                    <th>Bench</th>
                                    <th>Cause List Type </th>
                                    <th>View Cause List</th>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Mr.Sudhanshu Sing Kaintura</td>
                                    <td>WEEKLY</td>
                                    <td><a href={""}>View</a></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Mr.Gajendra Das</td>
                                    <td>WEEKLY</td>
                                    <td><a href={""}>View</a></td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Mr.Rehman Hafeez</td>
                                    <td>WEEKLY</td>
                                    <td><a href={""}>View</a></td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Mr Satpal Singh Bagga</td>
                                    <td>WEEKLY</td>
                                    <td><a href={""}>View</a></td>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>
                <Footer/>

            </div>
        );
    }
}

export default causeListSubmit;