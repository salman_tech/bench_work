import React, {Component} from 'react';
import logo from "../../components/assets/logo.jpg";
import footer from "../../components/assets/footer.png";
import Navbar from "../common/navbar"
import {Link} from "react-router-dom";


class Index extends Component {


    constructor(props) {

        super(props);
        this.state = {
            tabContent: 0,
            menuOpenClass: false
        };
    }

    handleDropdown = () => {
        this.setState({menuOpenClass: !this.state.menuOpenClass});
    }

    render() {
        return (
            <div>
                <Navbar />
                <div className="blueBox">
                    {this.state.tabContent == 1 && <div>

                        {/*<form>*/}

                        {/*    <div className="form-group row">*/}
                        {/*        <label htmlFor="colFormLabel" className="col-sm-2 col-form-label">Email</label>*/}
                        {/*        <div className="col-sm-10">*/}
                        {/*            <input type="email" className="form-control" id="colFormLabel"*/}
                        {/*                   placeholder="col-form-label"/>*/}
                        {/*        </div>*/}
                        {/*    </div> <div className="form-group row">*/}
                        {/*    <label htmlFor="colFormLabel" className="col-sm-2 col-form-label">Email</label>*/}
                        {/*    <div className="col-sm-10">*/}
                        {/*        <input type="email" className="form-control" id="colFormLabel"*/}
                        {/*               placeholder="col-form-label"/>*/}
                        {/*    </div>*/}
                        {/*</div> <div className="form-group row">*/}
                        {/*    <label htmlFor="colFormLabel" className="col-sm-2 col-form-label">Email</label>*/}
                        {/*    <div className="col-sm-10">*/}
                        {/*        <input type="email" className="form-control" id="colFormLabel"*/}
                        {/*               placeholder="col-form-label"/>*/}
                        {/*    </div>*/}
                        {/*</div>*/}

                        {/*</form>*/}
                    </div>}


                    {this.state.tabContent == 2 && <div>content 2</div>}
                    {this.state.tabContent == 3 && <div>content 3</div>}
                </div>

                <div className='footer-A'>
                    <footer> <a href={'https://wefoundenterprises.com/'}><img src={footer} style={{width:150,paddingTop:"10px"}}/></a>  </footer>
                </div>
            </div>
        );
    }
}

export default Index;