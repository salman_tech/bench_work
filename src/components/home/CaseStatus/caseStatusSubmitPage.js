import React, { Component } from "react";
import logo from "../../assets/logo.jpg";
import footer from "../../assets/footer.png";
import Footer from "../../common/footer";
import Service from '../../service'
import Moment from 'react-moment'
import {Link} from 'react-router-dom'

class caseStatusSubmitPage extends Component {
    constructor(){
        super()
        this.state={
        id:localStorage.getItem('disputed_id'),
        data:[],
        type1:localStorage.getItem("type1"),
        type4:localStorage.getItem("type4"),
        }

    }
    
    componentDidMount(){
        // this.getCase()
        this.viewCasePage(this.state.id)
    }
    viewCasePage=async(id)=>{
        let res = await Service.location.getCasePerId(id)
        if(typeof(res) == "object"){
            if(res.status == true && res.status !==undefined){
                this.setState({data:res.data})
            }
            else{
                alert(res.messsage)
            }
        }
        else{
            this.setState({data:[]})
        }
    }
                

    render() {
        console.log("staeeeeeeee",this.state.data)
        return (
            <div>

{this.state.type1 == "PESHKAR" && <Link to="/peshkaar-login"> <span style={{float:"left",paddingLeft:"20px",paddingTop:"50px",fontSize:"30px",cursor:"pointer"}}><img style={{width:"30px"}} src={require('../../assets/arrow.png')} /></span>
                       </Link>}
                       {this.state.type4 == "CLIENT" && <Link to="/"> <span style={{float:"left",paddingLeft:"20px",paddingTop:"50px",fontSize:"30px",cursor:"pointer"}}><img style={{width:"30px"}} src={require('../../assets/arrow.png')} /></span>
                       </Link>}
                    <div className="logo--heading" style={{paddingBottom:'30px'}}>
                        <img src={logo} style={{width:80,height:80}}/>
                        <h2>REVENUE COURT OF DEHRADUN</h2>
                    </div>


                    <div >

                        <div style={{padding:'30px',background:"#F1F6FE",overflow:"hidden"}}>
                            <div className="row">
                                <div className="col-md-4">
                                    <center><strong>Case Detail</strong></center>

                                    <table className="CaseStatusSubmitPageTable">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <b>Case Type:</b>{this.state.data && this.state.data.vaadType}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Registration Date:</b>{this.state.data && <Moment format="DD/MM/YYYY">{this.state.registrationDate}</Moment>}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Target Date:</b>{this.state.data && <Moment format="DD/MM/YYYY">{this.state.data.targetDate}</Moment>}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Case ID:</b>{this.state.data && this.state.data.courtId}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Court:</b>Has To be come from Backend
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Case Direction:</b>{this.state.data && this.state.data.caseDirection}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div style={{marginTop:"30px",textAlign:"center",fontWeight:"bold", }}><p style={{background:"#F1F6FE"}}>Case status</p></div>

                                    <table className="CaseStatusSubmitPageTable">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <b>First HearingDate:</b>Has To be come from Backend
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Decision Date:</b>Has to be come from Backend
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Case staus:</b>Has to be come backend
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>State:</b>DEHRADUN
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>District:</b>{this.state.data && this.state.data.complainant && this.state.data.complainant[0] ? this.state.data.complainant[0].districtId :"" }
                                            </td>
                                        </tr>


                                        </tbody>
                                    </table>
                                </div>
                                <div className="col-md-8">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <center><strong>Complainant and Advocate</strong></center>
                                            <div style={{border: "1px solid"}} className="CaseStatusSubmitPageDiv">
                                                {this.state.data && this.state.data.complainant && this.state.data.complainant.map((itm,key)=>{
                                                    return(
                                                        <>
                                                <p>{key+1}.){itm.name}</p>
                                                <p>advocate:{itm.advocateName}</p>
                                                        </>
                                                    )
                                                })}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <center><strong>Defendant and Advocate</strong></center>
                                            <div style={{border: "1px solid"}} className="CaseStatusSubmitPageDiv">
                                            {this.state.data && this.state.data.defendant && this.state.data.defendant.map((itm,key)=>{
                                                    return(
                                                        <>
                                                <p>{key+1}.){itm.name}</p>
                                                <p>advocate:{itm.advocateName}</p>
                                                        </>
                                                    )
                                                })}
                                            </div>
                                        </div>
                                    </div>
                                    <center><strong>Act</strong></center>
                                    <table className="CaseStatusSubmitPageTable">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <b>Under Act(s):</b>{this.state.data && this.state.data.dharaId}
                                            </td>
                                            <td>
                                                <b>Section(s):</b>{this.state.data && this.state.data.adhiniumId}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <center><strong>Category Details</strong></center>
                                    <table className="CaseStatusSubmitPageTable">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <b>Court:</b>Discuss
                                            </td>
                                            <td>
                                                <b>Court:</b>Discuss
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <center><strong>Court Fee Details</strong></center>
                                    <table className="CaseStatusSubmitPageTable">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <b>Valuation of Property:</b>{this.state.data && this.state.data.courtFeeValutionProperty}
                                            </td>
                                            <td>
                                                <b>Court Fee Valuation Land Revenue:</b>{this.state.data && this.state.data.courtFeeValuationLandRevenue}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <center><strong>Fixed Proceeding Details</strong></center>
                                    <table className="CaseStatusSubmitPageTable">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <b>Fixed Date:</b>{this.state.data && <Moment format="DD/MM/YYYY">{this.state.data.fixedDate}</Moment>}
                                            </td>
                                            <td>
                                                <b>Fixed Proceeding:</b>{this.state.data && this.state.data.fixedProceeding}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <center><div style={{background:"#c9d7ef", height:"61px"}}> <img src={footer} style={{width:150,paddingTop:"20px"}}/></div></center>
                    </div>
                {/* <Footer/> */}

            </div>
        );
    }
}

export default caseStatusSubmitPage;