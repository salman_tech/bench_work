import React, {Component} from 'react';
import Navbar from "../../common/navbar"
import {Link} from "react-router-dom";
import setting from "../../assets/setting.png";
import Footer from "../../common/footer";

class caseStatusSubmit extends Component {


    constructor(props) {

        super(props);
        this.state = {
            tabContent: 0,
            menuOpenClass: false
        };
    }

    render() {
        return (
            <div>
                <Navbar />
                <div className="blueBox1">
                    <div className="tablecontainer">
                       <center> <div> <h4> Total Number of Cases: 1</h4> </div></center>

                            <table>
                              <tr>
                                <th>Serial No.</th>
                                <th>Court/Case Number/<br/>Case Year</th>
                                <th>Petitioner Name<br/>Versus<br/>Respondent Name </th>
                                <th>View Status</th>
                              </tr>
                              <tr>
                                <td>1</td>
                                <td>DM/1131/2019</td>
                                <td>Mukesh Kumar <br/>Versus <br/> Pushkar Sharma </td>
                                  <Link to="/case-status-submit-page"> <p style={{color:"blue",  textAlign: "center"}}>View all</p></Link>

                              </tr>
                            </table>
                        </div>
                </div>
                <Footer/>

            </div>
        );
    }
}

export default caseStatusSubmit;