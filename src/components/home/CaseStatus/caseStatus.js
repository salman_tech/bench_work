import React, {Component} from 'react';
import Navbar from "../../common/navbar"
import {Link} from "react-router-dom";
import Footer from "../../common/footer";
import Service from '../../service'
import SimpleReactValidator from 'simple-react-validator';

class caseStatus extends Component {


    constructor(props) {

        super(props);
        this.state = {
            tabContent: 0,
            settingMenuVisible:false,
            courtName:[]

        };
        this.validator = new SimpleReactValidator({ autoForceUpdate: this });
    }
    componentDidMount(){
        this.getCourt()
        localStorage.clear()
    }
    handleChange=(e)=>{
        this.setState({[e.target.name]:e.target.value})
    }
    getCourt=async()=>{
        let res  = await Service.location.getCourt()
        if(typeof(res) == "object"){
            if(res && res.status && res.status == true){
                this.setState({courtName:res.data})
            }
            else{
                alert(res.message)
            }
        }
        else{
            this.setState({courtName:[]})
            // alert("API")
        }

    }
    moveToPage=(id)=>{
        localStorage.setItem("type4","CLIENT")
        const value = id
        debugger
        localStorage.setItem('disputed_id',value)
        this.props.history.push('/Case-Status-submit-page')
    }

    submit=async()=>{
        if (this.validator.allValid()) {
        const id = this.state.caseId
        debugger
        let res = await Service.location.getCaseHearing(id)
        debugger
        if(typeof(res) == "object"){
            if(res.status == true && res.status !==undefined){

                debugger
                // this.setState({data:res.data})
                const caseMoveTo = res.data[0] && res.data[0]._id && res.data[0]._id ? res.data[0]._id : "null" 
                // alert(caseMoveTo)
                this.moveToPage(caseMoveTo)
                // alert(res.message)

            }
            else{
                alert(res.message)
            }
        }
        else{
           alert("API Faild")
        }
    }
    else {
        this.validator.showMessages();
        this.forceUpdate();
        // alert("you miss the required filed")
     }
    }


    render() {
        return (
            <div>
                <Navbar />
                <div className="blueBox1">
                    <div className="container">
                        <div className="box_wrapper">
                            <form>
                                <div className="form-group row">
                                    <label htmlFor="inputEmail3" className="col-sm-4 col-form-label">Court </label>
                                    <div className="col-sm-8">
                                        <select className="custom-select" name="courtSelect" onChange={(e)=>this.handleChange(e)}>
                                            <option selected>Select Court</option>
                                            {this.state.courtName && this.state.courtName.map(itm=>{
                                                            return(
                                                                
                                                                <option  value={itm.name}>{itm.name}</option>
                                                            )
                                                        })}
                                            {/* <option value="1">One</option> */}
                                            {/* <option value="2">Two</option>
                                            <option value="3">Three</option> */}
                                        </select>
                                        <span className='text-danger'>
                                                {this.validator.message('courtSelect', this.state.courtSelect, 'required' )}
                                        </span>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label htmlFor="inputEmail3" className="col-sm-4 col-form-label">Case Number</label>
                                    <div className="col-sm-8">
                                        <input type="text" className="form-control" id="inputEmail3" name="caseId" onChange={(e)=>this.handleChange(e)}/>
                                        <span className='text-danger'>
                                                {this.validator.message('caseId', this.state.caseId, 'required' )}
                                        </span>
                                    </div>
                                </div>
                                {/* <div className="form-group row">
                                    <label htmlFor="inputEmail3" className="col-sm-4 col-form-label">Year</label>
                                    <div className="col-sm-8">
                                        <input type="email" className="form-control" id="inputEmail3"/>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label htmlFor="inputEmail3" className="col-sm-4 col-form-label">Act Number</label>
                                    <div className="col-sm-8">
                                        <input type="email" className="form-control" id="inputEmail3"/>
                                    </div>
                                </div> */}

                            </form>
                            <div className="form-group row">
                                    <label htmlFor="inputEmail3" className="col-sm-4 col-form-label"></label>
                                    <div className="col-sm-8">
                                        <div className="row">
                                        <div className="col-md-2">

                                {/* <button type="button" className="btn btn-secondary btn-lg" style={{background:"white",borderColor:"blue",borderWidth:   "thick",color:"blue"}}>Reset</button> */}
                                
                                        </div>
                                        <div className="col-md-4">

                                        <button type="button" className="btn btn-primary btn-lg" onClick={()=>this.submit()} style={{width:"250%"}}>Submit</button>
                                
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            {/* <div className="caseStatusButton">
                                <div className="container"></div> 
                                 &#160;
                                <Link to="/case-status-submit"><button type="button" className="btn btn-primary btn-lg">Submit</button></Link>
                            </div> */}
                        </div>
                    </div>
                </div>
                <Footer/>

            </div>
        );
    }
}

export default caseStatus;