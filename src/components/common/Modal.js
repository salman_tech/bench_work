import React from "react";
import ReactModal from "react-modal";
import { Modal, Button } from "react-bootstrap";
import "./modal.css";
import {Link} from "react-router-dom";
import remove from "../assets/remove.png";



class modalOffer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false
        }

    }



    handleOpenModal = () => {

        this.setState({ showModal: true });
    };

    handleCloseModal = () => {
        this.setState({ showModal: false });
    };

    render() {
        const overlayClassName = this.state.showModal
            ? "modal fade show"
            : "modal fade";

        return (
            <div>



                <img onClick={this.handleOpenModal} src={remove} style={{width:30}}/>

                <ReactModal
                    className="modal-dialog modal-content"
                    bodyOpenClassName="modal-open"
                    overlayClassName={overlayClassName}
                    ariaHideApp={false}
                    isOpen={this.state.showModal}>

                    <Modal.Body>
                       <div>
                           <h5> Do you want to Remove this user</h5>

                           <div className="form-group row">
                               <label htmlFor="inputEmail3" className="col-sm-6 col-form-label">Enter Your Password</label>
                               <div className="col-sm-4">
                                   <input type="password" className="form-control" id="inputEmail3" placeholder='Password'/>
                               </div>
                           </div>

                           <div className="caseStatusButton">
                               <button type="button"  onClick={this.handleCloseModal} className="btn btn-secondary btn-lg" style={{width:"150px"}}>No</button> &#160; &#160;&#160;&#160;
                               <button type="button" onClick={this.handleCloseModal} className="btn btn-primary btn-lg" style={{width:"150px"}}>Yes</button>
                           </div>
                       </div>
                    </Modal.Body>
                </ReactModal>
                <div
                    className={
                        this.state.showModal ? "modal-backdrop fade show" : "display: none;"
                    }
                />

            </div>
        );
    }
}


export default modalOffer;