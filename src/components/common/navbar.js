import React, {Component} from 'react';
import {Link,NavLink} from  "react-router-dom";
import logo from "../../components/assets/logo.jpg";
import pic1 from "../../components/assets/1.png";
import pic2 from "../../components/assets/2.png";
import pic3 from "../../components/assets/3.png";
import pic4 from "../assets/DisputedLandInfo.png";


class navbar extends Component {


    constructor(props) {

        super(props);
        this.state = {
            tabContent: 0,
            menuOpenClass: false
        };
    }

    handleDropdown = () => {
        this.setState({menuOpenClass: !this.state.menuOpenClass});
    }

    Close=()=>{
    // alert("close")
    window.close()
    }



    render() {
        return (
            <div>
                <div className="logo--heading">
                    <img src={logo} style={{width: 80, height: 80}}/>
                    <h2>REVENUE COURT OF DEHRADUN</h2>
                    <button className={this.state.menuOpenClass ? 'open' : ''} onClick={this.handleDropdown}><span
                        className="bar"></span></button>
                </div>
                <div className="hamburgerMenu">
                    <ul className={this.state.menuOpenClass ? 'show' : ''}>
                        {/* <li>Font Size: Normal</li> */}
                        <Link to="/login">
                            <button type="button" style={{
                                background: "white",
                                textDecoration: "none",
                                color: "black",
                                border: "none",
                                fontWeight: "bold"
                            }}>Login
                            </button>
                        </Link>
                        {/* <li onClick={()=>this.Close()}>Close Application</li> */}
                    </ul>
                </div>
                <div className="All top_menu_tab">
                    <div className="tabs" >

                        <NavLink activeClassName="" to="/case-status">
                            <img src={pic1}/><br/>
                          <center> <p style={{Color:"black"}}>Case Status</p> </center>

                        </NavLink>
                        <NavLink activeClassName="" to="/case-order-info">
                          <img src={pic2} /><br/> <center> <p style={{Color:"black"}}>Court Order</p> </center>
                        </NavLink>
                        <NavLink activeClassName="" to="/cause-list">
                           <img src={pic3} /><br/>  <center><p style={{Color:"black"}}>Cause List</p> </center>
                        </NavLink>
                        <NavLink activeClassName="" to="/disputed-land">
                           <img src={pic4} style={{marginLeft:"15px"}} /><br/>  <center><p style={{Color:"black"}}>Disputed Land</p> </center>
                        </NavLink>
                    </div>
                </div>
            </div>
        );
    }
}

export default navbar;