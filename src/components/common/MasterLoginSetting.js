import React, {Component} from 'react';
import logo from "../assets/logo.jpg";
import ProfleSetting from "../assets/ProfileSetting.png";
import ChangePassword from "../assets/ChangePassword.png";
import logout from "../assets/logout.png";
import {Link} from "react-router-dom";




class MasterLoginSetting extends Component {


    constructor(props) {

        super(props);
        this.state = {
            tabContent: 0,
            menuOpenClass: false
        };
    }

    render() {
        return (
            <React.Fragment>
                <div className="setting_menu">
                    <div className="logo--heading">
                        <img src={logo} style={{width:80,height:80}}/>
                        <h2>REVENUE COURT OF DEHRADUN</h2>
                    </div>

                    <div>
                        <div className="tabs">
                            {/* <button className="active"><img src={ProfleSetting} style={{width:60,height:60}}/><br/> Profile Setting</button> */}
                            <button className="active"><Link to="/change-password"><img src={ChangePassword} style={{width:60,height:60}}/></Link><br/> Change Password</button>
                           <Link to='/login'><button className="active" style={{marginTop:"20px"}}><img src={logout} style={{width:60,height:60,marginTop:"-15px"}}/><br/> Log Out</button></Link>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
export default MasterLoginSetting;