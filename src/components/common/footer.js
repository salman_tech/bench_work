import React, {Component} from 'react';
import footer from "../assets/footer.png";

class Footer extends Component {
    constructor(){
        super()
        this.state={
            time: new Date(),
          }
    }
    componentDidMount(){
        this.getTime()
    }
    componentWillMount(){
    setInterval(()=>this.getTime(),1000)      
    }
    getTime=()=>{this.setState({time:new Date()})}
    render() {
        return (
            <>
            <div className='footer'>
                 <div className="col-md-6">
                 <footer> 
                <a href={'https://wefoundenterprises.com/'} style={{float:"right"}}>
                <img src={footer} style={{width:150,paddingTop:"10px"}}/></a>
                 </footer>
                </div>
                <div className="col-md-6" style={{float:"right"}} >
                <p style={{marginTop:"20px",float:"right",background:"transparent"}}>{this.state.time.toLocaleTimeString()}</p> 
                </div>
            </div>
            </>
        );
    }
}

export default Footer;