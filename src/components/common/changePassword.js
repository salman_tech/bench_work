import React, {Component} from 'react';
import {Redirect,Link} from "react-router-dom";
import logo from "../assets/logo.jpg";
import axios from "axios";
import Footer from "../common/footer";
import config from "../../config";
import Service from '../service'
import {ToastsContainer, ToastsStore} from 'react-toasts';


class ChangePassowrd extends Component {


    constructor(props) {
        super(props);
        this.state = {};
    }

    handleChange=(e)=> {
        this.setState({
            [e.target.name] : e.target.value
        });
        console.log(this.state,"Updated")

    }
    checkPassword=(str)=>{
		var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])");
		return strongRegex.test(str)
}
    onSubmit=async()=>{
        debugger
        if(this.checkPassword(this.state.confirm_passowrd)){

            const body={
                "old_password":this.state.old_password,
                "new_password":this.state.new_password,
            }
            const res = await Service.location.ChangePassowrd(body)
            console.log("resssschangepasssworddddd",res)
        }
        else{
            alert("Passowrd Should Contain Uppercase Lowercase letter number and Special Character")
        }
    }
    render() {
        console.log("loginstateeeeeee",this.state)
        return (

            <div>

                <div className="logo--heading">
                    <img src={logo} style={{width:80,height:80}}/>
                    <h2>REVENUE COURT OF DEHRADUN</h2>
                </div>

                   <div style={{background:"#fff",height:"482px",overflow: "hidden",paddingTop:"30px" }} >

                       <div style={{paddingTop:'30px',background:"#F0F5FF",overflow: "hidden",height:"auto", width:"550px", margin:"auto",borderRadius:"20px",boxShadow:"0px 7px 5px rgba(0, 0, 0, .1)"}}>
                          {/* <center><div style={{marginTop:"-5px",height:"40px",width:"100%"}}><b><p>Change Passowrd</p></b></div></center> */}


                           <div style={{width:"350px",margin:"auto"}}>
                               <span><Link to="/"><span style={{fontSize:"28px",border:"none"}}>&larr;</span></Link></span>
                               {/* <span>Change Passowrd </span> */}
                               <form onSubmit={this.handleSubmit}>

                                   <div className="form-group row">
                                       <label htmlFor="inputEmail3" className="col-sm-4 col-form-label">old Password</label>

                                       <div className="col-sm-8">
                                           <input type="password" id="date-picker-example" name={'old_password'} className="form-control" placeholder="Enter your Old password" onChange={(e)=>this.handleChange(e)} />
                                       </div>
                                   </div>
                                   <div className="form-group row">
                                       <label htmlFor="inputEmail3" name={'new_password'} className="col-sm-4 col-form-label">New Password</label>

                                       <div className="col-sm-8">
                                           <input type="password" id="date-picker-example" name={'new_password'} className="form-control" placeholder="Enter your New password" onChange={(e)=>this.handleChange(e)} />
                                       </div>
                                   </div>
                                   <div className="form-group row">
                                       <label htmlFor="inputEmail3" name={'confirm_passowrd'} className="col-sm-4 col-form-label">Confirm Password</label>

                                       <div className="col-sm-8">
                                           <input type="password" id="date-picker-example" name={'confirm_passowrd'} className="form-control" placeholder="Enter your Confirm Password" onChange={(e)=>this.handleChange(e)} />
                                       </div>
                                   </div>

                                   <div className='column' style={{marginLeft:"110px"}}>
                                       <div className="row">
                                       <div className='col-sm-5' style={{paddingBottom:"10px",marginLeft:"25px"}}><button type="button" onClick={()=>this.onSubmit()} className="btn btn-primary btn-lg" style={{width:'auto',height:"50px"}}>Submit</button></div>
                                       <div className='col-sm-5'><button type="submit" className="btn btn-secondary btn-lg" style={{background:"transparent",borderColor:"blue",borderWidth:"3px",color:"blue",width:'auto',height:"50px" }}> <b>Reset</b> </button>
                                    </div>
                                    </div>
                                       <ToastsContainer store={ToastsStore}/>
                                   </div>
                               </form>
                           </div>
                       </div>
                   </div>
                <Footer/>

            </div>

        );
    }
}

export default ChangePassowrd;