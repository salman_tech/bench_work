import React, { Component } from 'react';
import Chart from "chart.js";
import Service from '../../service'
import { async } from 'q';

class MyChartD1 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: ["40","40","20"],
            backgroundData: new Array(12)
        }
    }

    componentDidMount() {
        var ctx = document.getElementById('myChartD1').getContext('2d');
        
        this.myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: ['Total No Of cases Registered', 'Total No Of Cases Ongoing', 'Total No Of Cases Disposed'],
                datasets: [{
                    // type: 'doughnut',
                    // label: 'Total Booking ',
                    // id: "y-axis-0",
                    // borderWidth: 1,
                    data : this.state.data,
                    backgroundColor: ["#ff6384","#36a2eb","#ffcd56"],
                    // tooltip: false
                }, 
            ]
            },
            options: {
                responsive: true,
                // maintainAspectRatio: false,
            }
        });
    }

    render() {
        return (
            <div>
                <canvas style={{ height:250+"px", width: 350+"px" }} id="myChartD1" ref={this.chartRef} />
            </div>
        );
    }
}

export default MyChartD1