import React, { Component } from 'react';
import Calendar from 'react-calendar';

class Calendar1 extends Component {
    state = {
        date: new Date(),
    }

    onChange = date => this.setState({ date })

    render() {
        return (
            <div>
                <div className='container'>
                    <Calendar
                        onChange={this.onChange}
                        value={this.state.date}
                    />
                </div>

            </div>
        );
    }
}

export default Calendar1;