import axios from 'axios';
import ENV from './config';
import { async } from 'q';

async function getData(url) {
    try {
        console.log(url)
        let _r = await axios.get(url,{ headers: { Authorization: 'jwt '.concat(localStorage.getItem('token')) } })
       debugger
        console.log( _r)
        return _r.data
    }
    catch (e) {
        console.log(e)
    }
}

async function postData(url, body) {
    try {
        let _r = await axios.post(url, body,{ headers: { Authorization: 'jwt '.concat(localStorage.getItem('token')) } })
        return _r.data
    } catch (e) {
}
}
async function deleteData(url) {
    try {
        let _r = await axios.delete(url)
        return _r.data
    } catch (e) {

    }
}
async function putData(url) {
    try {
        let _r = await axios.put(url)
        return _r.data
    } catch (e) {

    }
}


const location = {
    getCase:async()=>{
        const url = ENV.host0 + `/api/v1/case/type`;
        console.log(url)
        let r = await getData(url)
        console.log(r)
        return r;
    },
    getAllCase:async()=>{
        const url = ENV.host0 + `/api/v1/case`;
        console.log(url)
        let r = await getData(url)
        console.log(r)
        return r;
    },
    getCasePerId:async(id)=>{
        const url = ENV.host0 + `/api/v1/case/${id}`;
        console.log(url)
        let r = await getData(url)
        console.log(r)
        return r;
    },
    getDistict:async(id)=>{
        const url = ENV.host0 + `/api/v1/admin/dist`;
        console.log(url)
        let r = await getData(url)
        console.log(r)
        return r;
    },
    getPargana:async(id)=>{
        const url = ENV.host0 + `/api/v1/admin/pargana`;
        console.log(url)
        let r = await getData(url)
        console.log(r)
        return r;
    },
    getVillage:async(id)=>{
        const url = ENV.host0 + `/api/v1/admin/gram`;
        console.log(url)
        let r = await getData(url)
        console.log(r)
        return r;
    },
    
    getTehsil: async()=>{
        const url = ENV.host0 + `/api/v1/admin/tehsil`;
        console.log(url)
        let r = await getData(url)
        console.log(r)
        return r;
    },
    getCourt: async()=>{
        const url = ENV.host0 + `/api/v1/admin/court`;
        console.log(url)
        let r = await getData(url)
        console.log(r)
        return r;
    },
    DisputedLandInfo:async(type)=>{
        const url = ENV.host0 + `/api/v1/case/disputedLand/${type}`;
        console.log(url)
        let r = await getData(url)
        console.log(r)
        return r;
    },
    getClauseCase:async(id)=>{
        debugger
        const url = ENV.host0 + `/api/v1/causeList?date=${id}`;
        console.log(url)
        let r = await getData(url)
        console.log(r)
        return r;
    },
    getCaseHearing:async(id)=>{
        const url = ENV.host0 + `/api/v1/case/search?caseId=${id}`;
        console.log(url)
        let r = await getData(url)
        console.log(r)
        return r;
    },
    getAct:async()=>{
        const url = ENV.host0 + `/api/v1/admin/adhinium`;
        console.log(url)
        let r = await getData(url)
        console.log(r)
        return r;
    },
    getActAdhinium:async()=>{
        const url = ENV.host0 + `/api/v1/admin/adhinium/dharas`;
        console.log(url)
        let r = await getData(url)
        console.log(r)
        return r;
    },
    getLoginType:async()=>{
        const url = ENV.host0 + `/api/v1/login`;
        console.log(url)
        let r = await getData(url)
        console.log(r)
        return r;
    },
    caseSearch:async(id)=>{
        const url = ENV.host0 + `/api/v1/case/search?caseId=${id}`;
        console.log(url)
        let r = await getData(url)
        console.log(r)
        return r;
    },
    userSearch:async(id)=>{
        debugger
        const url = ENV.host0 + `/api/v1/admin/user/search?id=${id}`;
        console.log(url)
        let r = await getData(url)
        console.log(r)
        return r;
    },
    getDharas:async()=>{
        const url = ENV.host0 + `/api/v1/admin/dhara`;
        console.log(url)
        let r = await getData(url)
        console.log(r)
        return r;
    },
    getClauseCaseWeekly:async(body)=>{
        const url = ENV.host0 + `/api/v1/causeList/custom`;
        console.log(url)
        let r = await postData(url,body)
        console.log(r)
        return r;
    },

    editCase:async()=>{
        const url = ENV.host0 + `/api/v1/case`;
        console.log(url)
        let r = await putData(url)
        console.log(r)
        return r;
    },
    
    addCaseHearing:async(id,body)=>{
        debugger
        const url = ENV.host0 + `/api/v1/case/${id}/hearings`;
        console.log(url)
        let r = await postData(url,body)
        console.log(r)
        return r;
    },
    addCourtOrder: async (id,body) => {
        const url = ENV.host0 + `/api/v1/case/${id}/courtOrder`;
        let r = await postData(url,body);
        return r;
    },
    addCourt: async (body) => {
        const url = ENV.host0 + `/api/v1/admin/court`;
        let r = await postData(url, body);
        return r;
    },
    postAct: async (body)=>{
        const url = ENV.host0 + `/api/v1/admin/adhinium`;
        let r = await postData(url, body);
        return r;
    },
    ChangePassowrd: async (body)=>{
        const url = ENV.host0 + `/api/v1/change-password`;
        let r = await postData(url, body);
        return r;
    },
    deleletCase:async(id)=>{
        const url = ENV.host0 + `/api/v1/case/${id}/delete`;
        console.log(url)
        let r = await deleteData(url)
        console.log(r)
        return r;
    },
    deleletUser:async(user)=>{
        debugger
        const url = ENV.host0 + `/api/v1/admin/user/${user.id}/delete`;
        console.log(url)
        let r = await postData(url,user)
        debugger
        console.log(r)
        return r;
    },

}

export default {
    location
}