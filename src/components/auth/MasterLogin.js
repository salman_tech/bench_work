import React, {Component} from 'react';
import logo from "../assets/logo.jpg";
import addUser from "../assets/addUser.png";
import updateUser from "../assets/updateUser.png";
import setting from "../assets/setting.png";
import MasterLoginSetting from "../common/MasterLoginSetting";
import Modal from "../common/Modal"
import Footer from "../common/footer";
import AddCourt from "../assets/AddCourt.png";
import AddAct from "../assets/AddAct.png";
import {Link,NavLink} from "react-router-dom";
import axios from "axios";
import Service from '../service';
import { typeAlias } from '@babel/types';


class MasterLogin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: []
        };
    }





    componentDidMount() {
        this.getUser()
    }

    getUser=()=>{
        axios.get('http://139.59.47.2/api/v1/admin/user/',{ headers: { Authorization: 'jwt '.concat(localStorage.getItem('token')) } })
            .then(res => {
                console.log(res)
                this.setState({users: res.data.data})
                console.log(this.state.users)

            })
    }
    prompt= async (id)=>{
        const userId = id 
        var person = prompt("Please Enter Password");
        if(person !==''){
            const body={
                "id":userId,
                "password":person
            }
            debugger
            const res = await  Service.location.deleletUser(body)
            if(typeof(res) == 'object'){
                if(res.status && res.status == true ){
                    debugger
                    alert("User Deleted Successfully")
                    this.getUser()
                }else{
                    alert("Enter the correct password")
                }

            }
        }else{
            alert("Enter the Password")
        }
    }
    // delete=(id)=>{
    //     const body = {id}
    //     alert(body.id)
    // }
    handleChange=(e)=>{
        this.setState({[e.target.name]:e.target.value})
    }
    userSearch=async()=>{
        debugger
        if(this.state.userSearch && this.state.userSearch !==undefined){
            let res = await Service.location.userSearch(this.state.userSearch)
            if(typeof(res) == 'object'){
                if(res.status && res.status == true){
                    this.setState({users:res.data})
                }
                else{
                    alert("no Search found")
                }
            }
            else{
                alert("API Failed")
            }
        }
    }

    render() {
        return (
            <div>
                <div className="TopIconShadow">
                    <div className="logo--heading">
                        <img src={logo} style={{width:80,height:80}}/>
                        <h2>REVENUE COURT OF DEHRADUN</h2>
                    </div>
                    <div className={"setting_menu_main " + (this.state.settingMenuVisible ? "open" : "")}>
                        <span className="close_menu" onClick={()=>{
                            this.setState({
                                settingMenuVisible:false
                            })
                        }}>
                            <i className="mdi mdi-close"></i>
                        </span>
                        <MasterLoginSetting/>
                    </div>
                        <div className="Master-Login-tabs">
                            <Link to="/Add-Users"> <button className="active"><img src={addUser} style={{width:60,height:60}}/><br/> Add User</button></Link>
                            <Link to="/Add-Act"> <button className="active"><img src={AddAct} style={{width:60,height:60}}/><br/> Add Act</button></Link>
                            <Link to="/Add-Court"><button className="active"><img src={AddCourt} style={{width:60,height:60}}/><br/>  Add Court</button></Link>
                            <Link to="/master-login"><button className="active"><img src={updateUser} style={{width:60,height:60}}/><br/> Modify User</button></Link>
                            <button className="active"  onClick={()=>{this.setState({settingMenuVisible:true})}}><img src={setting} style={{width:60,height:60}}/><br/> Setting</button>
                        </div>
                    </div>

                <div>
                    <div style={{paddingTop:'30px',background:"#F1F6FE", height:"calc(100vh - 250px)",width:"100%"}}>

                        <div className="container" style={{paddingBottom:"60px"}}>
                                <div className='row'><div className="col-sm-6"><h4> Totel Number of User : {this.state.users ? this.state.users.length : 0 }</h4></div>
                                    <div className="col-sm-6" > 
                                    <input name="userSearch" onChange={(e)=>this.handleChange(e)} type="text" placeholder="Search By Case No"/>&nbsp;&nbsp;
                                    <button onClick={()=>this.userSearch()} type="submit" style={{background:"#1F51AF" ,paddingTop:"5px",paddingBottom:"5px", textAlign:"center",color:"white",border: "none",borderRadius: "5px"}}> Search</button>
                                    </div>
                                </div>

                            <div style={{overflowY:"auto", height:"300px"}}>
                                <table className="CaseStatusSubmitPageTable">

                                    <tr>
                                        <th>S.No</th>
                                        <th>Name</th>
                                        <th>Designation</th>
                                        <th>Case ID</th>
                                        <th>Password</th>
                                        <th>Account Type</th>
                                        <th>Delete Profile</th>
                                    </tr>

                                    {
                                        this.state.users.map((item,key) =>
                                            <tr>
                                                <td>{key+1}</td>
                                                <td>{item.username}</td>
                                                <td>{item.designation}</td>
                                                <td>{item.registrationNo}</td>
                                                <td>{item.password.slice(0,8)}</td>
                                                <td>
                                                <select name={'LoginType'} onChange={()=>{this.prompt()}} className="DropDownForm" style={{width:"110px"}} >
                                                    <option value="Peethaseen">Peethaseen </option>
                                                    <option value="Peshkaar">Peshkaar</option>
                                                </select>
                                                </td>
                                                <td><span style={{width:"100px",borderRadius:"50%",background:"#b7dcfd"}}onClick={()=>{this.prompt(item._id)}} >X</span></td>
                                            </tr>
                                        )
                                    }

                                </table>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default MasterLogin;