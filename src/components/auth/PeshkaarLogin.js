import React, {Component} from 'react';
import logo from "../assets/logo.jpg";
import {Link} from "react-router-dom";
import Dashboard from "../assets/Dashboard.png";
import CaseRegistration from "../assets/CaseRegistration.png";
import DailyCauseList from "../assets/DailyCauseList.png";
import CourtOrder from "../assets/CourtOrder.png";
import DisputedLandInfo from "../assets/DisputedLandInfo.png";
import Setting from "../assets/setting.png";
// import Calendar from "../common/calendar";
import ChartD1 from "../common/peshkarChart/chartD1";
import ChartD2 from "../common/peshkarChart/chartD2";
import ChartD3 from "../common/peshkarChart/chartD3";
import DownArrow from '../assets/DownArrow.png'
import MasterLoginSetting from "../common/MasterLoginSetting";
import Footer from "../common/footer";
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';







class PeshkaarLogin extends Component {


    constructor(props) {

        super(props);
        this.state = {
            tabContent: 0,
            menuOpenClass: false
        };
    }

    render() {
        return (
            <div>
                
                <div style={{}}>
                    <div className="logo--heading">
                        <img src={logo} style={{width:80,height:80}}/>
                        <h2>REVENUE COURT OF DEHRADUN</h2>
                    </div>

                    <div className={"setting_menu_main " + (this.state.settingMenuVisible ? "open" : "")}>
                        <span className="close_menu" onClick={()=>{
                            this.setState({
                                settingMenuVisible:false
                            })
                        }}>
                            <i className="mdi mdi-close"></i>
                        </span>
                        <MasterLoginSetting/>
                    </div>

                    <div className="PeshkarLoginTabs" >
                        <div className="container">
                            <ul>
                                <li><Link to='/peshkaar-login' >
                                <button className="active"><img src={Dashboard} style={{width:60,height:60,marginTop:"-15px"}}/><br/> Dashboard </button> </Link> </li>

                                <li> <UncontrolledDropdown>
                                    <DropdownToggle  style={{background:"white",color: "black",border:"none"}}>
                                        <img src={CaseRegistration} style={{width:60,height:60}}/><br/> Case Registration <br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem> <Link to='/new-case-registration'>New Case Registration</Link></DropdownItem>
                                        <DropdownItem divider />
                                        <DropdownItem><Link to to='/view-all-case'>View Case</Link></DropdownItem>
                                        <DropdownItem divider />
                                        {/* <DropdownItem>Delete Case</DropdownItem> */}
                                        <DropdownItem  />
                                        <DropdownItem ><Link to='/view-case-status'>Update Case status</Link>
                                        <DropdownItem divider />
                                        </ DropdownItem>
                                        <DropdownItem ><Link >Upload Order Sheet</Link>
                                        <DropdownItem divider />
                                        </DropdownItem>
                                    </DropdownMenu>
                                    
                                </UncontrolledDropdown> </li>

                                <li>
                                    <UncontrolledDropdown>
                                        <DropdownToggle style={{background:"white",color: "black",border:"none"}} >
                                            <img src={DailyCauseList} style={{width:60,height:60}}/><br/>  Daily Cause List <br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem><Link to="/admin-cause-list"> View Daily Cause List </Link></DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Change Prioritiy</DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Next Hearing Date</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                </li>



                                <li>
                                    <UncontrolledDropdown>
                                        <DropdownToggle style={{background:"white",color: "black",border:"none"}} >
                                            <Link to="/all-case-order"><img src={CourtOrder} style={{width:60,height:60}}/><br/>  Court Orders</Link> <br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                         {/* <DropdownMenu>
                                            <DropdownItem>  Type Order </DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Upload Order</DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Upload Order: Disposed </DropdownItem>
                                           <DropdownItem divider />
                                           <DropdownItem>Application Letter: Disposed</DropdownItem> 
                                     </DropdownMenu> */}
                                    </UncontrolledDropdown>
                                </li>


                                <li>
                                    <UncontrolledDropdown>
                                        <DropdownToggle  style={{background:"white",color: "black",border:"none"}}>
                                            <img src={DisputedLandInfo} style={{width:60,height:60}}/><br/>Disputed Land Info<br/> <img src={DownArrow} style={{width:20,height:20}}/>                                  </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem> <Link to={{pathname:'/disputed-land-information',state:{type:"GOLDEN_FOREST_LAND"}}}>Golden Forest Land</Link></DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem> <Link to={{pathname:'/disputed-land-information',state:{type:"GRAM_SABHA_LAND"}}}>Gram Sabha Land</Link></DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem> <Link to={{pathname:'/disputed-land-information',state:{type:"LAND_PROHIBITED_FROM_SALE"}}}>Land Prohibited From Sale</Link></DropdownItem>

                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                </li>
                                <li><button className="active" onClick={()=>{this.setState({settingMenuVisible:true})}}><img src={Setting} style={{width:60,height:60,marginTop:"-15px"}}/><br/> Setting</button></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div className="PeshkarLoginBlueBox" style={{height:"110vh"}}>
                        <div style={{paddingTop:"30px"}}>
                        <div className="container">
                        <div className="row">
                        <div className="col-md-8"><h4>Graphs and Statistics</h4>
                        </div>
                        <div className="col-md-4" style={{textAlign:"right"}}>
                        <div className="row">
                        {/* <div className="col-md-9" style={{textAlign:"right",paddingRight:"0px"}} >
                        <input placeholder="Enter the Case No" style={{height:"29px",width:"100%"}}/>
                        </div> */}
                        {/* <div className="col-md-3" style={{textAlign:"center",padding:"0px"}}>
                        <button className="btn btn-primary" style={{height:"30px",lineHeight:"0px"}}>Search</button>
                        </div> */}
                        </div>
                        </div>
                        
                        </div>
                            <div className="row">
                            <div className="col-sm-4">
                            <div className="col-md-12" style={{background: "white",border:"1px solid lightgray"}}>
                            <ChartD1/>
                            </div>
                            </div>
                            <div className="col-sm-4">
                            <div className="col-md-12" style={{background: "white",border:"1px solid lightgray"}}>
                            <ChartD2/>
                            </div>
                            </div>
                            <div className="col-sm-4">
                            <div className="col-md-12" style={{background: "white",border:"1px solid lightgray"}}>
                            <ChartD3/>
                            </div>
                            </div>
                            </div>
                            </div>
                    </div>

                    <div style={{paddingTop:"30px"}}>
                        <div className="container">
                        
                            <div className="row">
                            <div className="col-sm-4">
                            <div className="col-md-12" style={{background: "white",border:"1px solid lightgray"}}>
                            
                            </div>
                            </div>
                            <div className="col-sm-4">
                            <div className="col-md-12" style={{background:"white",border:"1px solid lightgray",paddingBottom:"20px"}}>
                            <div className="row">
                            <div className="col-md-6"><p style={{fontWeight:"bold",color:"black",marginBottom:"10px",marginTop:"10px"}}>Priority List</p></div>
                            <div className="col-md-6"><p style={{float:"right",color:"blue",fontWeight:"bold",marginBottom:"10px",marginTop:"10px"}}>
                            <Link to to='/view-all-case'>List All Cases</Link>
                                </p></div>
                            </div>
                            <div className="col-md-12" style={{border:"2px solid lightgray"}}>
                            <div className="col-md-12" style={{marginBottom:"20px"}}>
                            <div className="row">
                                <div className="col-md-4" style={{borderBottom:"2px solid lightgray",paddingBottom:"10px",paddingTop:"10px",fontWeight:"bold"}}><span>09 Feb</span></div>
                                <div className="col-md-8" style={{borderBottom:"2px solid lightgray",paddingBottom:"10px",padding:"0px",paddingTop:"10px",paddingRight:"0px"}}>
                                    <div class="col-md-12" style={{padding:"0px"}}><span >Hearing CaseNo. 123456</span></div>
                                
                            </div>
                            </div>
                            <div className="row">
                                <div className="col-md-4" style={{borderBottom:"2px solid lightgray",paddingBottom:"10px",paddingTop:"10px",fontWeight:"bold"}}><span>09 Feb</span></div>
                                <div className="col-md-8" style={{borderBottom:"2px solid lightgray",paddingBottom:"10px",padding:"0px",paddingTop:"10px",paddingRight:"0px"}}>
                                    <div class="col-md-12" style={{padding:"0px"}}><span >Hearing CaseNo. 123456</span></div>
                                
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div className="col-sm-4">
                            <div className="col-md-12" style={{background:"white",border:"1px solid lightgray",paddingBottom:"20px"}}>
                            {/* <ChartD3/> */}
                            <div className="row">
                            <div className="col-md-6"><p style={{fontWeight:"bold",color:"black",marginBottom:"10px",marginTop:"10px"}}>Upcoming Holidays</p></div>
                            <div className="col-md-6"><p style={{float:"right",color:"blue",fontWeight:"bold",marginBottom:"10px",marginTop:"10px"}}>List All Holidays</p></div>
                            </div>
                            <div className="col-md-12" style={{border:"2px solid lightgray"}}>
                            <div className="col-md-12">
                            <div className="row">
                                <div className="col-md-4" style={{borderBottom:"2px solid lightgray",paddingBottom:"10px",paddingTop:"10px",fontWeight:"bold"}}><span>09 Feb</span></div>
                                <div className="col-md-8" style={{borderBottom:"2px solid lightgray",paddingBottom:"10px",paddingTop:"10px"}}><span >Guru Ravidas Jayanti</span></div>
                            </div>
                            <div className="row" style={{marginBottom:"20px"}}>
                                <div className="col-md-4" style={{borderBottom:"2px solid lightgray",paddingBottom:"10px",paddingTop:"10px",fontWeight:"bold"}}><span>09 Feb</span></div>
                                <div className="col-md-8" style={{borderBottom:"2px solid lightgray",paddingBottom:"10px",paddingTop:"10px"}}><span >Guru Ravidas Jayanti</span></div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                    </div>
                    <div className="container">
                    <div style={{paddingTop:"10px",paddingBottom:"100px"}}>
                    <div className="col-md-8" style={{paddingLeft:"0px"}}><h4>Case type-wise Statistics</h4>
                        </div>
                        <div className='PeshkarTable'>
                        <table>
                            <tr>
                                <th>Prime Cases</th>
                                <th>Appeal</th>
                                <th>Revison</th>
                                <th>Review</th>
                                <th>Transfer</th>
                                <th>Miscellaneous</th>
                                <th>Restored</th>
                                <th>Context</th>
                                <th>Total Case</th>
                            </tr>
                            <tr>
                                <td>123</td>
                                <td>75</td>
                                <td>25</td>
                                <td>12</td>
                                <td>0</td>
                                <td>2</td>
                                <td>0</td>
                                <td>1</td>
                                <td>238</td>
                            </tr>
                        </table>
                        </div>
                    </div>
                    </div>

                </div>
                <Footer/>

            </div>
        );
    }
}
export default PeshkaarLogin;