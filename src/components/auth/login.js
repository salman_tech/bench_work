import React, {Component} from 'react';
import {Redirect,Link} from "react-router-dom";
import logo from "../assets/logo.jpg";
import axios from "axios";
import Footer from "../common/footer";
import config from "../../config";
import Service from '../service'
import {ToastsContainer, ToastsStore} from 'react-toasts';


class Login extends Component {


    constructor(props) {
        super(props);
        this.state = {
            LoginTypeList : [],
            LoginAsList:[],
            Username : null,
            Password : null,
            loginType : null,
            loginAs : null
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
        



    handleChange(e) {
        this.setState({
            [e.target.name] : e.target.value
        });
        console.log(this.state,"Updated")

    }
    componentDidMount() {
        this.getLoginType()
    }
    

    getLoginType= async()=>{
        const res = await Service.location.getLoginType()
        console.log("loginnnntype",res)
        if(typeof(res) =="object"){
            if(res && res.status ==true){
                  await  this.setState({LoginTypeList : res.data.loginType, LoginAsList : res.data.loginAs})
            debugger
                //   console.log("stateeee",this.state.LoginTypeList)
                const filterLoginType = this.state.LoginTypeList.filter(itm=>{
                    return itm != "MASTER"
                })
                this.setState({LoginTypeList:filterLoginType})
                }
            else{
                alert(res.message)
            }
        }
        else{
            alert("API Fails")
        }
        // axios.get('http://139.59.47.2/api/v1/login')
        //     .then(res => {
        //     })
            localStorage.clear()
    }



    handleSubmit(event) {
        event.preventDefault();
        const data = {
            email : this.state.Username,
            password : this.state.Password


        }
        const path = this.props
        axios.post('http://139.59.47.2/api/v1/admin/login', data)
            .then(response =>
                {
                    debugger
                    if(response.data.status && response.data.status == true) {
                        localStorage.setItem("token", response.data.data.token);
                        ToastsStore.success('Login Successful');
                        console.log(this.state.loginType)
                        if(this.state.loginType == "PESHKAR"){
                            console.log("Before")
                            localStorage.setItem("type1","PESHKAR")
                            localStorage.removeItem("type2")
                            localStorage.removeItem("type3")
                            path.history.push('/peshkaar-login')
                            console.log("After")
                        }
                        else if(this.state.loginType == "PEETHASEEN_ADHIKARI"){
                            console.log("Before")
                            localStorage.removeItem("type1")
                            localStorage.setItem("type2","PEETHASEEN_ADHIKARI")
                            localStorage.removeItem("type3")
                            
                            path.history.push('/DM-login')
                            console.log("After")
                        }
                        else if(this.state.loginType !== "MASTER"){
                            console.log("Before")
                            localStorage.removeItem("type1")
                            localStorage.removeItem("type2")
                            localStorage.setItem("type3","MASTER")
                            path.history.push('/master-login')
                            console.log("After")
                        }

                    }
                    else{
                        alert("Enter the Correct UserName and Paswword")
                        // ToastsStore.error('Login Failed');

                    }
                }
            )

    }


    render() {
        console.log("loginstateeeeeee",this.state)
        return (

            <div>

                <div className="logo--heading">
                    <img src={logo} style={{width:80,height:80}}/>
                    <h2>REVENUE COURT OF DEHRADUN</h2>
                </div>

                   <div style={{background:"#fff",height:"482px",overflow: "hidden",paddingTop:"30px" }} >
                       <div style={{paddingTop:'30px',background:"#F0F5FF",overflow: "hidden",height:"450px", width:"550px", margin:"auto",borderRadius:"20px",boxShadow:"0px 7px 5px rgba(0, 0, 0, .1)"}}>
                    <div className="col-md-2">
                        <Link to="/">
                        <img src={require('../assets/home.png')} style={{width:"30px",background:"transparent",border:"none",cursor:"pointer"}} className="img-thumbnail"/>
                        </Link>
                        </div>
                          <center>
                              <div style={{marginTop:"-5px",height:"40px",width:"65px"}}><b><p>Login</p></b></div>
                              </center>


                           <div style={{width:"350px",margin:"auto"}}>
                               <form >

                                   <div className='row'>

                                       <label htmlFor="inputEmail3" className="col-sm-4 col-form-label">Login Type</label>
                                       <div className="col-sm-8">

                                           <div className="md-form" >
                                               <select name={'loginAs'}  onChange={this.handleChange} className="DropDownForm" >
                                                   <option >Select your User type</option>
                                                   {
                                                       this.state.LoginAsList.map((type) =>
                                                           <option value={type} name={'loginAs'}>{type}</option>
                                                       )
                                                   }
                                               </select>
                                           </div>
                                       </div>
                                   </div>

                                {this.state.loginAs !=="MASTER" && 
                                   <div className='row' style={{marginTop:"10px"}}>

                                       <label htmlFor="inputEmail3" className="col-sm-4 col-form-label">User Type</label>
                                       <div className="col-sm-8">

                                           <div className="md-form" >
                                               <select name={'loginType'} onChange={this.handleChange} className="DropDownForm" >
                                                   <option >Select your User type</option>
                                                   {
                                                       this.state.LoginTypeList.map((type) =>
                                                           <option value={type} name={'loginType'}>{type}</option>
                                                       )
                                                   }
                                               </select>
                                           </div>
                                       </div>
                                   </div>}

                                   <div className="form-group row" style={{marginTop:"10px"}}>
                                       <label htmlFor="inputEmail3" className="col-sm-4 col-form-label"> User ID </label>
                                       <div className="col-sm-8">
                                           <input type="text" id="date-picker-example" name={'Username'} className="form-control" placeholder={'Enter you user ID'} onChange={this.handleChange} />
                                       </div>
                                   </div>


                                   <div className="form-group row">
                                       <label htmlFor="inputEmail3" className="col-sm-4 col-form-label">Password</label>

                                       <div className="col-sm-8">
                                           <input type="password" id="date-picker-example" name={'Password'} className="form-control" placeholder="Enter your password" onChange={this.handleChange} />
                                       </div>
                                   </div>

                                   <div className='column' style={{marginLeft:"110px"}}>
                                       <div className='col-sm-6' style={{paddingBottom:"10px"}}><button type="submit" className="btn btn-primary btn-lg" style={{width:'200px',height:"50px",background:"blue"}} onClick={this.handleSubmit}>Submit</button></div>
                                       <div className='col-sm-6'><button type="submit" className="btn btn-secondary btn-lg" style={{background:"transparent",borderColor:"blue",borderWidth:"3px",color:"blue",width:'200px',height:"50px" }}> <b>Reset</b> </button>
                                       </div>
                                       <ToastsContainer store={ToastsStore}/>
                                   </div>
                               </form>
                           </div>
                       </div>
                   </div>
                <Footer/>

            </div>

        );
    }
}

export default Login;