import React, {Component} from 'react';
import logo from "../assets/logo.jpg";
import Dashboard from"../assets/Dashboard.png"
import DailyCauseList from "../assets/DailyCauseList.png"
import CourtOrder from "../assets/CourtOrder.png";
import setting from "../assets/setting.png";
import Archives from "../assets/Archives.png"
import MasterLoginSetting from "../common/MasterLoginSetting";
import Chart from "../common/chart";
import Calendar from "../common/calendar";
import DownArrow from "../assets/DownArrow.png";
import ChartD1 from "../common/peshkarChart/chartD1";
import ChartD2 from "../common/peshkarChart/chartD2";
import ChartD3 from "../common/peshkarChart/chartD3";
import Footer from "../common/footer";
import {Link} from 'react-router-dom'

class DM_Login extends Component {


    constructor(props) {

        super(props);
        this.state = {
            tabContent: 0,
            settingMenuVisible:false
        };
    }

    render() {
        return (
            <div>
                <div>
                    <div className="logo--heading">
                        <img src={logo} style={{width:80,height:80}}/>
                        <h2>REVENUE COURT OF DEHRADUN</h2>
                    </div>
                    <div className={"setting_menu_main " + (this.state.settingMenuVisible ? "open" : "")}>
                        <span className="close_menu" onClick={()=>{
                            this.setState({
                                settingMenuVisible:false
                            })
                        }}>
                            <i className="mdi mdi-close"></i>
                        </span>
                        <MasterLoginSetting/>
                    </div>
                    <div className="tabs">
                        <button className="active"><img src={Dashboard} style={{width:60,height:60}}/><br/> Dashboard</button>
                        <button className="active"> <Link to='/daily-case'><img src={DailyCauseList} style={{width:60,height:60}}/><br/>Daily Cause List</Link> <br/>
                         {/* <img src={DownArrow} style={{width:20,height:20}}/> */}
                         </button>
                        <button className="active"><Link to='/all-case-order'><img src={CourtOrder} style={{width:60,height:60}}/><br/> Court Orders </Link><br/>
                        {/* <img src={DownArrow} style={{width:20,height:20}}/> */}
                        </button>
                        <button className="active"><img src={Archives} style={{width:60,height:60}}/><br/> Archives </button>
                        <button className="active"  onClick={()=>{this.setState({settingMenuVisible:true})}}><img src={setting} style={{width:60,height:60}}/><br/> Setting</button>
                    </div>
                </div>

                <div className="PeshkarLoginBlueBox" style={{height:"110vh"}}>
                        <div style={{paddingTop:"30px"}}>
                        <div className="container">
                        <div className="row">
                        <div className="col-md-8"><h4>Graphs and Statistics</h4>
                        </div>
                        <div className="col-md-4" style={{textAlign:"right"}}>
                        <div className="row">
                        <div className="col-md-9" style={{textAlign:"right",paddingRight:"0px"}} >
                        <input placeholder="Enter the Case No" style={{height:"29px",width:"100%"}}/>
                        </div>
                        <div className="col-md-3" style={{textAlign:"center",padding:"0px"}}>
                        <button className="btn btn-primary" style={{height:"30px",lineHeight:"0px"}}>Search</button>
                        </div>
                        </div>
                        </div>
                        
                        </div>
                            <div className="row">
                            <div className="col-sm-4">
                            <div className="col-md-12" style={{background: "white",border:"1px solid lightgray"}}>
                            <ChartD1/>
                            </div>
                            </div>
                            <div className="col-sm-4">
                            <div className="col-md-12" style={{background: "white",border:"1px solid lightgray"}}>
                            <ChartD2/>
                            </div>
                            </div>
                            <div className="col-sm-4">
                            <div className="col-md-12" style={{background: "white",border:"1px solid lightgray"}}>
                            <ChartD3/>
                            </div>
                            </div>
                            </div>
                            </div>
                    </div>

                    <div style={{paddingTop:"30px"}}>
                        <div className="container">
                        
                            <div className="row">
                            <div className="col-sm-4">
                            <div className="col-md-12" style={{background: "white",border:"1px solid lightgray"}}>
                            
                            </div>
                            </div>
                            <div className="col-sm-4">
                            <div className="col-md-12" style={{background:"white",border:"1px solid lightgray",paddingBottom:"20px"}}>
                            {/* <ChartD3/> */}
                            <div className="row">
                            <div className="col-md-6"><p style={{fontWeight:"bold",color:"black",marginBottom:"10px",marginTop:"10px"}}>Priority List</p></div>
                            <div className="col-md-6"><p style={{float:"right",color:"blue",fontWeight:"bold",marginBottom:"10px",marginTop:"10px"}}>List All Cases</p></div>
                            </div>
                            <div className="col-md-12" style={{border:"2px solid lightgray"}}>
                            <div className="col-md-12" style={{marginBottom:"20px"}}>
                            <div className="row">
                                <div className="col-md-4" style={{borderBottom:"2px solid lightgray",paddingBottom:"10px",paddingTop:"10px",fontWeight:"bold"}}><span>09 Feb</span></div>
                                <div className="col-md-8" style={{borderBottom:"2px solid lightgray",paddingBottom:"10px",padding:"0px",paddingTop:"10px",paddingRight:"0px"}}>
                                    <div class="col-md-12" style={{padding:"0px"}}><span >Hearing CaseNo. 123456</span></div>
                                
                            </div>
                            </div>
                            <div className="row">
                                <div className="col-md-4" style={{borderBottom:"2px solid lightgray",paddingBottom:"10px",paddingTop:"10px",fontWeight:"bold"}}><span>09 Feb</span></div>
                                <div className="col-md-8" style={{borderBottom:"2px solid lightgray",paddingBottom:"10px",padding:"0px",paddingTop:"10px",paddingRight:"0px"}}>
                                    <div class="col-md-12" style={{padding:"0px"}}><span >Hearing CaseNo. 123456</span></div>
                                
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div className="col-sm-4">
                            <div className="col-md-12" style={{background:"white",border:"1px solid lightgray",paddingBottom:"20px"}}>
                            {/* <ChartD3/> */}
                            <div className="row">
                            <div className="col-md-6"><p style={{fontWeight:"bold",color:"black",marginBottom:"10px",marginTop:"10px"}}>Upcoming Holidays</p></div>
                            <div className="col-md-6"><p style={{float:"right",color:"blue",fontWeight:"bold",marginBottom:"10px",marginTop:"10px"}}>List All Holidays</p></div>
                            </div>
                            <div className="col-md-12" style={{border:"2px solid lightgray"}}>
                            <div className="col-md-12">
                            <div className="row">
                                <div className="col-md-4" style={{borderBottom:"2px solid lightgray",paddingBottom:"10px",paddingTop:"10px",fontWeight:"bold"}}><span>09 Feb</span></div>
                                <div className="col-md-8" style={{borderBottom:"2px solid lightgray",paddingBottom:"10px",paddingTop:"10px"}}><span >Guru Ravidas Jayanti</span></div>
                            </div>
                            <div className="row" style={{marginBottom:"20px"}}>
                                <div className="col-md-4" style={{borderBottom:"2px solid lightgray",paddingBottom:"10px",paddingTop:"10px",fontWeight:"bold"}}><span>09 Feb</span></div>
                                <div className="col-md-8" style={{borderBottom:"2px solid lightgray",paddingBottom:"10px",paddingTop:"10px"}}><span >Guru Ravidas Jayanti</span></div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                    </div>
                    <div className="container">
                    <div style={{paddingTop:"10px",paddingBottom:"100px"}}>
                    <div className="col-md-8" style={{paddingLeft:"0px"}}><h4>Case type-wise Statistics</h4>
                        </div>
                        <div className='PeshkarTable'>
                        <table>
                            <tr>
                                <th>Prime Cases</th>
                                <th>Appeal</th>
                                <th>Revison</th>
                                <th>Review</th>
                                <th>Transfer</th>
                                <th>Miscellaneous</th>
                                <th>Restored</th>
                                <th>Context</th>
                                <th>Total Case</th>
                            </tr>
                            <tr>
                                <td>123</td>
                                <td>75</td>
                                <td>25</td>
                                <td>12</td>
                                <td>0</td>
                                <td>2</td>
                                <td>0</td>
                                <td>1</td>
                                <td>238</td>
                            </tr>
                        </table>
                        </div>
                    </div>
                    </div>

                </div>
                <Footer/>
            </div>
        );
    }
}

export default DM_Login;